#!/usr/bin/env bash
echo "Starting runHyJaccard."

cd python


START_TIME=$SECONDS
S=$SECONDS
TAG=xaa
echo "Running Linking on "$TAG
#python filterLshResults.py \
#--inputClusters "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_v_"$TAG".json" \
#--methodName hybridJaccard \
#--threshold 0.49 \
#--outputFile "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_Filtered_hy_"$TAG".json"

ELAPSED_TIME=$(($SECONDS - $S))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' Hybrid jaccard duration = '$timestamp' .'


S=$SECONDS
TAG=xab
echo "Running Linking on "$TAG
#python filterLshResults.py \
#--inputClusters "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_v_"$TAG".json" \
#--methodName hybridJaccard \
#--threshold 0.49 \
#--outputFile "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_Filtered_hy_"$TAG".json"

ELAPSED_TIME=$(($SECONDS - $S))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' Hybrid jaccard duration = '$timestamp' .'

TAG=xac
echo "Running Linking on "$TAG
#python filterLshResults.py \
#--inputClusters "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_v_"$TAG".json" \
#--methodName hybridJaccard \
#--threshold 0.49 \
#--outputFile "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_Filtered_hy_"$TAG".json"


TAG=xad
echo "Running Linking on "$TAG
#python filterLshResults.py \
#--inputClusters "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_v_"$TAG".json" \
#--methodName hybridJaccard \
#--threshold 0.49 \
#--outputFile "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_Filtered_hy_"$TAG".json"



TAG=xae
echo "Running Linking on "$TAG
#python filterLshResults.py \
#--inputClusters "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_v_"$TAG".json" \
#--methodName hybridJaccard \
#--threshold 0.49 \
#--outputFile "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_Filtered_hy_"$TAG".json"




TAG=xaf
echo "Running Linking on "$TAG
#python filterLshResults.py \
#--inputClusters "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_v_"$TAG".json" \
#--methodName hybridJaccard \
#--threshold 0.49 \
#--outputFile "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_Filtered_hy_"$TAG".json"



TAG=xag
echo "Running Linking on "$TAG
#python filterLshResults.py \
#--inputClusters "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_v_"$TAG".json" \
#--methodName hybridJaccard \
#--threshold 0.49 \
#--outputFile "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_Filtered_hy_"$TAG".json"



TAG=xah
echo "Running Linking on "$TAG
python filterLshResults.py \
--inputClusters "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_v_"$TAG".json" \
--methodName hybridJaccard \
--threshold 0.49 \
--outputFile "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_Filtered_hy_"$TAG".json"


TAG=xai
echo "Running Linking on "$TAG
python filterLshResults.py \
--inputClusters "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_v_"$TAG".json" \
--methodName hybridJaccard \
--threshold 0.49 \
--outputFile "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_Filtered_hy_"$TAG".json"


ELAPSED_TIME=$(($SECONDS - $START_TIME))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' Hybrid jaccard duration = '$timestamp' .' >> /home/aifb-ls3-vm8/gga/runAll_Logs.txt
echo $logPrefix' Hybrid jaccard duration = '$timestamp' .'