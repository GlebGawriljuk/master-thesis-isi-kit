#!/usr/bin/env bash
echo "Starting runAll."

source runMergePipelineConfig.txt

TAG="xae"
LOCAL_HIVE_RESULT_FILE_NAME="LSH_withData_"$TAG"_temp.json"
LOCAL_FILTERED_FILE_NAME="LSH_withData_"$TAG"_Filtered_temp.json"

#./runLSH.sh

#./runHIVE.sh

cd python
python filterLshResults.py \
--inputClusters "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/"$LOCAL_HIVE_RESULT_FILE_NAME \
--methodName hybridJaccard,birthYear \
--threshold 0.49 \
--outputFile "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/"$LOCAL_FILTERED_FILE_NAME

python genTypeArtistsTriplesFromLinking.py \
--i "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/"$LOCAL_FILTERED_FILE_NAME \
--o "/home/aifb-ls3-vm8/gga/00_rawData/3_viaf/artistTriples/artistTriples_"$TAG".nt"


