#!/usr/bin/env bash
echo "Starting runAll."
chmod 777 runLSH_v2.sh
chmod 777 runHIVE_v2.sh

source runMergePipelineConfig.txt



TAG=xad
START_TIME=$SECONDS

#./runLSH_v2.sh $TAG

ELAPSED_TIME=$(($SECONDS - $START_TIME))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' LSH duration = '$timestamp' .' >> /home/aifb-ls3-vm8/gga/runAll_Logs.txt

START_TIME=$SECONDS

./runHIVE_v2.sh $TAG

echo "Hive done with: "$HIVE_RESULT_FILE_NAME

ELAPSED_TIME=$(($SECONDS - $START_TIME))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' Hive Join duration = '$timestamp' .' >> /home/aifb-ls3-vm8/gga/runAll_Logs.txt







TAG=xae
START_TIME=$SECONDS

#./runLSH_v2.sh $TAG

ELAPSED_TIME=$(($SECONDS - $START_TIME))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' LSH duration = '$timestamp' .' >> /home/aifb-ls3-vm8/gga/runAll_Logs.txt

START_TIME=$SECONDS

./runHIVE_v2.sh $TAG

echo "Hive done with: "$HIVE_RESULT_FILE_NAME

ELAPSED_TIME=$(($SECONDS - $START_TIME))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' Hive Join duration = '$timestamp' .' >> /home/aifb-ls3-vm8/gga/runAll_Logs.txt





TAG=xaf
START_TIME=$SECONDS

#./runLSH_v2.sh $TAG

ELAPSED_TIME=$(($SECONDS - $START_TIME))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' LSH duration = '$timestamp' .' >> /home/aifb-ls3-vm8/gga/runAll_Logs.txt

START_TIME=$SECONDS

./runHIVE_v2.sh $TAG

echo "Hive done with: "$HIVE_RESULT_FILE_NAME

ELAPSED_TIME=$(($SECONDS - $START_TIME))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' Hive Join duration = '$timestamp' .' >> /home/aifb-ls3-vm8/gga/runAll_Logs.txt






TAG=xag
START_TIME=$SECONDS

#./runLSH_v2.sh $TAG

ELAPSED_TIME=$(($SECONDS - $START_TIME))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' LSH duration = '$timestamp' .' >> /home/aifb-ls3-vm8/gga/runAll_Logs.txt

START_TIME=$SECONDS

./runHIVE_v2.sh $TAG

echo "Hive done with: "$HIVE_RESULT_FILE_NAME

ELAPSED_TIME=$(($SECONDS - $START_TIME))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' Hive Join duration = '$timestamp' .' >> /home/aifb-ls3-vm8/gga/runAll_Logs.txt




TAG=xah
START_TIME=$SECONDS

#./runLSH_v2.sh $TAG

ELAPSED_TIME=$(($SECONDS - $START_TIME))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' LSH duration = '$timestamp' .' >> /home/aifb-ls3-vm8/gga/runAll_Logs.txt

START_TIME=$SECONDS

./runHIVE_v2.sh $TAG

echo "Hive done with: "$HIVE_RESULT_FILE_NAME

ELAPSED_TIME=$(($SECONDS - $START_TIME))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' Hive Join duration = '$timestamp' .' >> /home/aifb-ls3-vm8/gga/runAll_Logs.txt




TAG=xai
START_TIME=$SECONDS

#./runLSH_v2.sh $TAG

ELAPSED_TIME=$(($SECONDS - $START_TIME))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' LSH duration = '$timestamp' .' >> /home/aifb-ls3-vm8/gga/runAll_Logs.txt

START_TIME=$SECONDS

./runHIVE_v2.sh $TAG

echo "Hive done with: "$HIVE_RESULT_FILE_NAME

ELAPSED_TIME=$(($SECONDS - $START_TIME))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' Hive Join duration = '$timestamp' .' >> /home/aifb-ls3-vm8/gga/runAll_Logs.txt

#cd python
#python filterLshResults.py \
#--inputClusters "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/"$HIVE_RESULT_FILE_NAME \
#--methodName hybridJaccard,birthYear \
#--threshold 0.49 \
#--outputFile "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_Filtered_temp.json"

#python genTypeArtistsTriplesFromLinking.py \
#--i "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_Filtered_temp.json" \
#--o "/home/aifb-ls3-vm8/gga/00_rawData/3_viaf/artistTriples/artistTriples_xad.nt"


