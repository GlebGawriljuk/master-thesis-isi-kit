#!/usr/bin/env bash
echo "Starting runAll."
chmod 777 runLSH.sh
chmod 777 runHIVE.sh

source runMergePipelineConfig.txt

START_TIME_1=$SECONDS

./runLSH.sh

ELAPSED_TIME=$(($SECONDS - $START_TIME_1))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' LSH duration = '$timestamp' .' >> /home/aifb-ls3-vm8/gga/runAll_Logs.txt

START_TIME_2=$SECONDS

./runHIVE.sh

echo "Hive done with: "$HIVE_RESULT_FILE_NAME

ELAPSED_TIME=$(($SECONDS - $START_TIME_2))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' Hive Join duration = '$timestamp' .' >> /home/aifb-ls3-vm8/gga/runAll_Logs.txt

cd $ROOT
cd python
temp_filtered_lsh_file_path="/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_viaf_artists_Filtered_hy_by.json"
python filterLshResults.py \
--inputClusters "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/"$HIVE_RESULT_FILE_NAME \
--methodName hybridJaccard,birthYear \
--threshold 0.49 \
--outputFile $temp_filtered_lsh_file_path

echo $logPrefix"Creating provenance..."
tempProvFileName="prov_viaf_artist_small.json"
START_TIME_CREATE_PROV=$SECONDS
python createProvJson.py \
--lshParameters $LSH_NUM_OF_HASHES"_Hashes,"$LSH_ITEM_IN_BANDS"_Items_in_Bands,tokenizerFile_"$LSH_TOKENIZER_CONFIG_NAME"" \
--filterParameters $FILTER_MTHD",top_1,thrld_"$FILTER_THRESHOLD \
--sourceObj1 $KG_FILE_PATH \
--sourceObj2 $DATASOURCE_FILE_PATH \
--timeStamp 2015 \
--output $tempProvFileName

echo $logPrefix"Creating index..."
tempIndexFileName="tempIndex.json"
#START_TIME_CREATE_PROV=$SECONDS
python generateLinksIndex.py \
--inputLinksFilePath $temp_filtered_lsh_file_path \
--outputFile $tempIndexFileName

echo $logPrefix"Merging Results into Knowledge Graph"
START_TIME_KG_CONSTRUCTION=$SECONDS
python integrateLshLinks_v3.py \
--inputLshClusters $temp_filtered_lsh_file_path \
--linksIndexFilePath $tempIndexFileName \
--inputKnowledgGraph $KG_FILE_PATH \
--inputProvFile $tempProvFileName \
--inputBaseData $DATASOURCE_FILE_PATH \
--outputFile "../../07_KnowledgeGraphs/KG_u_s_d_v_artists.json"

ELAPSED_TIME=$(($SECONDS - $START_TIME_CREATE_PROV))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' Construction KG duration = '$timestamp' .'
echo $logPrefix' Construction KG duration = '$timestamp' .' >> $tempLogFileName

#python genTypeArtistsTriplesFromLinking.py \
#--i "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_Filtered_temp.json" \
#--o "/home/aifb-ls3-vm8/gga/00_rawData/3_viaf/artistTriples/artistTriples_xad.nt"


