#!/usr/bin/env bash
echo "Starting runAll."
chmod -R 777 ../libs/

source runMergePipelineConfig.txt


# remove LSH data
rm  /home/aifb-ls3-vm8/gga/libs/lsh_spark/baseDatasource/00_tokens/*
rm  /home/aifb-ls3-vm8/gga/libs/lsh_spark/knowledgeGraph/00_tokens/*
rm  /home/aifb-ls3-vm8/gga/libs/lsh_spark/baseDatasource/01_lshMinHashes/*
rm  /home/aifb-ls3-vm8/gga/libs/lsh_spark/knowledgeGraph/01_lshMinHashes/*
rm  /home/aifb-ls3-vm8/gga/libs/lsh_spark/cluster/*