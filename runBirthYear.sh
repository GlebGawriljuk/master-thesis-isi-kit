#!/usr/bin/env bash
echo "Starting runBirthYear."

cd python

START_TIME=$SECONDS


TAG=xaa
python filterLshResults.py \
--inputClusters "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_Filtered_hy_"$TAG".json" \
--methodName birthYear \
--threshold 0.49 \
--outputFile "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_Filtered_hy_by_"$TAG".json"


TAG=xab
python filterLshResults.py \
--inputClusters "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_Filtered_hy_"$TAG".json" \
--methodName birthYear \
--threshold 0.49 \
--outputFile "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_Filtered_hy_by_"$TAG".json"


TAG=xac
python filterLshResults.py \
--inputClusters "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_Filtered_hy_"$TAG".json" \
--methodName birthYear \
--threshold 0.49 \
--outputFile "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_Filtered_hy_by_"$TAG".json"


TAG=xad
python filterLshResults.py \
--inputClusters "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_Filtered_hy_"$TAG".json" \
--methodName birthYear \
--threshold 0.49 \
--outputFile "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_Filtered_hy_by_"$TAG".json"



TAG=xae
python filterLshResults.py \
--inputClusters "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_Filtered_hy_"$TAG".json" \
--methodName birthYear \
--threshold 0.49 \
--outputFile "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_Filtered_hy_by_"$TAG".json"




TAG=xaf
python filterLshResults.py \
--inputClusters "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_Filtered_hy_"$TAG".json" \
--methodName birthYear \
--threshold 0.49 \
--outputFile "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_Filtered_hy_by_"$TAG".json"



TAG=xag
python filterLshResults.py \
--inputClusters "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_Filtered_hy_"$TAG".json" \
--methodName birthYear \
--threshold 0.49 \
--outputFile "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_Filtered_hy_by_"$TAG".json"



TAG=xah
python filterLshResults.py \
--inputClusters "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_Filtered_hy_"$TAG".json" \
--methodName birthYear \
--threshold 0.49 \
--outputFile "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_Filtered_hy_by_"$TAG".json"


TAG=xai
python filterLshResults.py \
--inputClusters "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_Filtered_hy_"$TAG".json" \
--methodName birthYear \
--threshold 0.49 \
--outputFile "/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_withData_Filtered_hy_by_"$TAG".json"


ELAPSED_TIME=$(($SECONDS - $START_TIME))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' Birth year duration = '$timestamp' .' >> /home/aifb-ls3-vm8/gga/runAll_Logs.txt
echo $logPrefix' Birth year duration = '$timestamp' .'