#!/bin/bash
echo "Starting runLSH."
echo $$ > myscript.pid
#start script from: cd /Documents/Glebs_Folder/12_peopleClusters/00_pipeline



chmod -R 777 ../master-thesis-isi-kit/
chmod -R 777 ../libs/

logPrefix="runMergePipelineConfig: "
source runMergePipelineConfig.txt

logFileTimeStamp=$(date +'%m-%d-%Y_%H-%M-%S')
homePath=$OUTSIDE_ROOT"/pipelineLogs"
tempLogFileName=$homePath"/logs_pipeline_"$logFileTimeStamp".txt"
echo "Logs will be writen into "$tempLogFileName

START_TIME=$SECONDS

START_TIME_JQ_KG=$SECONDS
temp_kg_fileName=temp_kg.tsv
echo $logPrefix"Running jQ cmd " "'"$JQ_INSTRUCTION_KG"'" "on" $KG_FILE_PATH 
jQ/jq-linux -r "$JQ_INSTRUCTION_KG" $KG_FILE_PATH > ../04_lshInputData/$temp_kg_fileName

ELAPSED_TIME=$(($SECONDS - $START_TIME_JQ_KG))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' jQ duration for knowledge graph = '$timestamp' .'  >> $tempLogFileName

START_TIME_JQ_DATASOURCE=$SECONDS
temp_datasource_fileName=temp_datasource.tsv
echo $logPrefix"Running jQ cmd " "'"$JQ_INSTRUCTION_DATASOURCE"'" "on" $DATASOURCE_FILE_PATH 
jQ/jq-linux -r "$JQ_INSTRUCTION_DATASOURCE" $DATASOURCE_FILE_PATH > ../04_lshInputData/$temp_datasource_fileName

ELAPSED_TIME=$(($SECONDS - $START_TIME_JQ_DATASOURCE))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' jQ duration for datasource = '$timestamp' .' >> $tempLogFileName





cd $LSH_SPARK_HADOOP_PATH
echo $logPrefix"Running LSH Spark Base Tokenization "
START_TIME_TOKENIZE_DS=$SECONDS
temp_datasource_tokens_fileName=../baseDatasource/00_tokens
rm -rf $temp_datasource_tokens_fileName; ./bin/spark-submit \
    --master local[$LSH_MASTER] \
    --executor-memory=$LSH_EX_MEM \
    --driver-memory=$LSH_DRIVER_MEM \
    --py-files ../dig-lsh-clustering/tokenizer/tokenizer.zip \
    ../dig-lsh-clustering/tokenizer/tokenizer.py \
    $OUTSIDE_ROOT"04_lshInputData/"$temp_datasource_fileName \
    $LSH_TOKENIZER_CONFIG_PATH$LSH_TOKENIZER_CONFIG_NAME \
    $temp_datasource_tokens_fileName





echo $logPrefix"Running LSH Spark Knowlede Graph Tokenization "
START_TIME_TOKENIZE_KG=$SECONDS
temp_kg_tokens_fileName=../knowledgeGraph/00_tokens
rm -rf $temp_kg_tokens_fileName; ./bin/spark-submit \
    --master local[$LSH_MASTER] \
    --executor-memory=$LSH_EX_MEM \
    --driver-memory=$LSH_DRIVER_MEM \
    --py-files ../dig-lsh-clustering/tokenizer/tokenizer.zip \
    ../dig-lsh-clustering/tokenizer/tokenizer.py \
    $OUTSIDE_ROOT"04_lshInputData/"$temp_kg_fileName \
    $LSH_TOKENIZER_CONFIG_PATH$LSH_TOKENIZER_CONFIG_NAME \
    $temp_kg_tokens_fileName






echo $logPrefix"Running LSH Spark Base Hasher "
START_TIME_HASHER_DS=$SECONDS
temp_datasource_hashes_fileName=../baseDatasource/01_lshMinHashes
rm -rf $temp_datasource_hashes_fileName; ./bin/spark-submit \
    --master local[$LSH_MASTER] \
    --executor-memory=$LSH_EX_MEM \
    --driver-memory=$LSH_DRIVER_MEM \
    --py-files ../dig-lsh-clustering/hasher/hasher.zip \
    ../dig-lsh-clustering/hasher/hasher.py \
    --saveMinhashes --numHashes $LSH_NUM_OF_HASHES --numItemsInBand $LSH_ITEM_IN_BANDS \
    $temp_datasource_tokens_fileName \
    $temp_datasource_hashes_fileName





echo $logPrefix"Running LSH Spark Knowledge Graph Hasher "
START_TIME_HASHER_KG=$SECONDS
temp_kg_hashes_fileName=../knowledgeGraph/01_lshMinHashes
rm -rf $temp_kg_hashes_fileName; ./bin/spark-submit \
    --master local[$LSH_MASTER] \
    --executor-memory=$LSH_EX_MEM \
    --driver-memory=$LSH_DRIVER_MEM \
    --py-files ../dig-lsh-clustering/hasher/hasher.zip \
    ../dig-lsh-clustering/hasher/hasher.py \
    --saveMinhashes --numHashes $LSH_NUM_OF_HASHES --numItemsInBand $LSH_ITEM_IN_BANDS \
    $temp_kg_tokens_fileName \
    $temp_kg_hashes_fileName






echo $logPrefix"Running LSH Spark Clustering"
temp_cluster_fileName=cluster
START_TIME_CLUSTERING=$SECONDS
rm -rf ../$temp_cluster_fileName; ./bin/spark-submit \
    --master local[$LSH_MASTER] \
    --executor-memory=$LSH_EX_MEM \
    --driver-memory=$LSH_DRIVER_MEM \
    ../dig-lsh-clustering/clusterer/clusterer.py \
    --numPartitions $LSH_NUM_OF_PARTITIONS \
    --base $temp_kg_hashes_fileName \
    --computeSimilarity \
    $temp_datasource_hashes_fileName \
    ../$temp_cluster_fileName \
    --outputtype $LSH_OUTPUT_FORMAT \
    --topk $LSH_TOP_CANDIDATES

ELAPSED_TIME=$(($SECONDS - $START_TIME_TOKENIZE_DS))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' Clustering duration = '$timestamp' .' >> $tempLogFileName
echo $logPrefix' Clustering duration = '$timestamp' .'




cd ..
tempTimeStamp=$(date +'%m-%d-%Y_%H-%M-%S')
temp_lsh_merged_cluster_fileName=$LSH_CLUSTER_FILE_NAME
echo $logPrefix"Concatinating LSH Spark clusters"
START_TIME_CONC_CLUSTERS=$SECONDS
./concantLshPartitions.sh $temp_cluster_fileName/ $temp_lsh_merged_cluster_fileName

ELAPSED_TIME=$(($SECONDS - $START_TIME_CONC_CLUSTERS))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' Concatinating clusters duration = '$timestamp' .'  >> $tempLogFileName



# move LSH results
echo 'Moving LSH results to pipeline dir.'
START_TIME_MOVE_LSH_RESULTS=$SECONDS
cd $ROOT
cd ..
temp_cluster_fileName="libs/lsh_spark/cluster"
lsh_results_path_destination="05_LSHresults/"$temp_lsh_merged_cluster_fileName
mv $temp_cluster_fileName/$temp_lsh_merged_cluster_fileName $lsh_results_path_destination
