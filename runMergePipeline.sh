#!/bin/bash
echo $$ > myscript.pid
#start script from: cd /Documents/Glebs_Folder/12_peopleClusters/00_pipeline

chmod -R 777 ../master-thesis-isi-kit/
chmod -R 777 ../libs/

logPrefix="runMergePipelineConfig: "
source runMergePipelineConfig.txt

logFileTimeStamp=$(date +'%m-%d-%Y_%H-%M-%S')
homePath=$OUTSIDE_ROOT"/pipelineLogs"
tempLogFileName=$homePath"/logs_pipeline_"$logFileTimeStamp".txt"
echo "Logs will be writen into "$tempLogFileName

START_TIME=$SECONDS

START_TIME_JQ_KG=$SECONDS
temp_kg_fileName=temp_kg.tsv
echo $logPrefix"Running jQ cmd " "'"$JQ_INSTRUCTION_KG"'" "on" $KG_FILE_PATH 
jQ/jq-linux -r "$JQ_INSTRUCTION_KG" $KG_FILE_PATH > ../04_lshInputData/$temp_kg_fileName

ELAPSED_TIME=$(($SECONDS - $START_TIME_JQ_KG))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' jQ duration for knowledge graph = '$timestamp' .'  >> $tempLogFileName

START_TIME_JQ_DATASOURCE=$SECONDS
temp_datasource_fileName=temp_datasource.tsv
echo $logPrefix"Running jQ cmd " "'"$JQ_INSTRUCTION_DATASOURCE"'" "on" $DATASOURCE_FILE_PATH 
jQ/jq-linux -r "$JQ_INSTRUCTION_DATASOURCE" $DATASOURCE_FILE_PATH > ../04_lshInputData/$temp_datasource_fileName

ELAPSED_TIME=$(($SECONDS - $START_TIME_JQ_DATASOURCE))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' jQ duration for datasource = '$timestamp' .' >> $tempLogFileName





cd $LSH_SPARK_HADOOP_PATH
echo $logPrefix"Running LSH Spark Base Tokenization "
START_TIME_TOKENIZE_DS=$SECONDS
temp_datasource_tokens_fileName=../baseDatasource/00_tokens
rm -rf $temp_datasource_tokens_fileName; ./bin/spark-submit \
    --master local[$LSH_MASTER] \
    --executor-memory=$LSH_EX_MEM \
    --driver-memory=$LSH_DRIVER_MEM \
    --py-files ../dig-lsh-clustering/tokenizer/tokenizer.zip \
    ../dig-lsh-clustering/tokenizer/tokenizer.py \
    $OUTSIDE_ROOT"04_lshInputData/"$temp_datasource_fileName \
    $LSH_TOKENIZER_CONFIG_PATH$LSH_TOKENIZER_CONFIG_NAME \
    $temp_datasource_tokens_fileName


ELAPSED_TIME=$(($SECONDS - $START_TIME_TOKENIZE_DS))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' Tokenization of datasource duration = '$timestamp' .' >> $tempLogFileName





echo $logPrefix"Running LSH Spark Knowlede Graph Tokenization "
START_TIME_TOKENIZE_KG=$SECONDS
temp_kg_tokens_fileName=../knowledgeGraph/00_tokens
rm -rf $temp_kg_tokens_fileName; ./bin/spark-submit \
    --master local[$LSH_MASTER] \
    --executor-memory=$LSH_EX_MEM \
    --driver-memory=$LSH_DRIVER_MEM \
    --py-files ../dig-lsh-clustering/tokenizer/tokenizer.zip \
    ../dig-lsh-clustering/tokenizer/tokenizer.py \
    $OUTSIDE_ROOT"04_lshInputData/"$temp_kg_fileName \
    $LSH_TOKENIZER_CONFIG_PATH$LSH_TOKENIZER_CONFIG_NAME \
    $temp_kg_tokens_fileName


ELAPSED_TIME=$(($SECONDS - $START_TIME_TOKENIZE_KG))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' Tokenization of datasource duration = '$timestamp' .'  >> $tempLogFileName







echo $logPrefix"Running LSH Spark Base Hasher "
START_TIME_HASHER_DS=$SECONDS
temp_datasource_hashes_fileName=../baseDatasource/01_lshMinHashes
rm -rf $temp_datasource_hashes_fileName; ./bin/spark-submit \
    --master local[$LSH_MASTER] \
    --executor-memory=$LSH_EX_MEM \
    --driver-memory=$LSH_DRIVER_MEM \
    --py-files ../dig-lsh-clustering/hasher/hasher.zip \
    ../dig-lsh-clustering/hasher/hasher.py \
    --saveMinhashes --numHashes $LSH_NUM_OF_HASHES --numItemsInBand $LSH_ITEM_IN_BANDS \
    $temp_datasource_tokens_fileName \
    $temp_datasource_hashes_fileName


ELAPSED_TIME=$(($SECONDS - $START_TIME_HASHER_DS))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' Hashing of datasource duration = '$timestamp' .'  >> $tempLogFileName





echo $logPrefix"Running LSH Spark Knowledge Graph Hasher "
START_TIME_HASHER_KG=$SECONDS
temp_kg_hashes_fileName=../knowledgeGraph/01_lshMinHashes
rm -rf $temp_kg_hashes_fileName; ./bin/spark-submit \
    --master local[$LSH_MASTER] \
    --executor-memory=$LSH_EX_MEM \
    --driver-memory=$LSH_DRIVER_MEM \
    --py-files ../dig-lsh-clustering/hasher/hasher.zip \
    ../dig-lsh-clustering/hasher/hasher.py \
    --saveMinhashes --numHashes $LSH_NUM_OF_HASHES --numItemsInBand $LSH_ITEM_IN_BANDS \
    $temp_kg_tokens_fileName \
    $temp_kg_hashes_fileName

ELAPSED_TIME=$(($SECONDS - $START_TIME_HASHER_KG))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' Hashing of Knowledge Graph duration = '$timestamp' .' >> $tempLogFileName




echo $logPrefix"Running LSH Spark Clustering"
temp_cluster_fileName=cluster
START_TIME_CLUSTERING=$SECONDS
rm -rf ../$temp_cluster_fileName; ./bin/spark-submit \
    --master local[$LSH_MASTER] \
    --executor-memory=$LSH_EX_MEM \
    --driver-memory=$LSH_DRIVER_MEM \
    ../dig-lsh-clustering/clusterer/clusterer.py \
    --numPartitions $LSH_NUM_OF_PARTITIONS \
    --base $temp_kg_hashes_fileName \
    --computeSimilarity \
    $temp_datasource_hashes_fileName \
    ../$temp_cluster_fileName \
    --outputtype $LSH_OUTPUT_FORMAT \
    --topk $LSH_TOP_CANDIDATES

ELAPSED_TIME=$(($SECONDS - $START_TIME_TOKENIZE_DS))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' Clustering duration = '$timestamp' .' >> $tempLogFileName




cd ..
tempTimeStamp=$(date +'%m-%d-%Y_%H-%M-%S')
temp_lsh_merged_cluster_fileName="lsh_clusters_top_"$LSH_TOP_CANDIDATES"_"$tempTimeStamp".json"
echo $logPrefix"Concatinating LSH Spark clusters"
START_TIME_CONC_CLUSTERS=$SECONDS
./concantLshPartitions.sh $temp_cluster_fileName/ $temp_lsh_merged_cluster_fileName

ELAPSED_TIME=$(($SECONDS - $START_TIME_CONC_CLUSTERS))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' Concatinating clusters duration = '$timestamp' .'  >> $tempLogFileName



# move LSH results
echo 'Moving LSH results to pipeline dir.'
START_TIME_MOVE_LSH_RESULTS=$SECONDS
cd $ROOT
cd ..
temp_cluster_fileName="libs/lsh_spark/cluster"
lsh_results_path_destination="05_LSHresults/"$temp_lsh_merged_cluster_fileName
mv $temp_cluster_fileName/$temp_lsh_merged_cluster_fileName $lsh_results_path_destination

# copy LSH results for HIVE Join
echo 'Copying LSH results to HIVE dir.'
hive_target_dir_path="libs/hiveJoin/target/"
hive_target_file_path=$hive_target_dir_path$temp_lsh_merged_cluster_fileName
rm -r libs/hiveJoin/target/*
cp -v $lsh_results_path_destination $hive_target_file_path

# copy kowledge graph for HIVE Join
echo 'Copying Knowledge graph to HIVE dir.'
hive_target_dir_path="libs/hiveJoin/source/"
hive_source_file_path=$hive_target_dir_path"temp_kg_for_hive.json"
rm -r libs/hiveJoin/source/*
#cp -v $KG_FILE_PATH

#trim KG for Hive Join
python /home/aifb-ls3-vm8/gga/master-thesis-isi-kit/python/trimKG4HiveJoin.py \
--inputKnowledgGraph $KG_FILE_PATH \
--propertyNameList $FILTER_PROPS \
--outputFile $hive_source_file_path

ELAPSED_TIME=$(($SECONDS - $START_TIME_MOVE_LSH_RESULTS))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' moving LSH results = '$timestamp' .' >> $tempLogFileName


# remove LSH data
rm -rf $LSH_SPARK_PATHbaseDatasource/00_tokens
rm -rf $LSH_SPARK_PATHknowledgeGraph/00_tokens
rm -rf $LSH_SPARK_PATHbaseDatasource/01_lshMinHashes
rm -rf $LSH_SPARK_PATHknowledgeGraph/01_lshMinHashes
rm -rf $LSH_SPARK_PATHcluster



# run join between knowledge graph and lsh results
echo $logPrefix"Running Hive Join: mergeing Knowledge Graph data into LSH clusters"
cd $HIVE_SCRIPT_FOLDER
START_TIME_HIVE_JOIN=$SECONDS
./join_LSH_and_KG_V2.sh

# concatinate resultes
echo 'Concatinating Hive Join tempt results.'
./concantLshPartitions.sh ../merged/ temp_LSH_with_KgData.json

# delete previous hive target file
echo 'Setting up files for next Hive Join.'
rm -r ../target/*

# move merged temp file to target
mv ../merged/temp_LSH_with_KgData.json ../target/temp_LSH_with_KgData.json

# delete previous hive source file
rm -r ../source/*

# copy datasource file to hive source dir
cp -v $DATASOURCE_FILE_PATH ../source/temp_datasource.json

# run hive join
echo $logPrefix"Running Hive Join: mergeing Datasource data into LSH clusters"
./join_TEMP_and_Datasource_V2.sh

# concatinate hive results
echo 'Concatinating final HIVE results.'
tempTimeStamp=$(date +'%m-%d-%Y_%H-%M-%S')
./concantLshPartitions.sh ../merged/ "LSH_with_Data_"$tempTimeStamp".json"

# move hive merge result to pipeline dir
echo 'Moving HIVE results to pipeline dir.'
mv "../merged/LSH_with_Data_"$tempTimeStamp".json" "../../../06_LshResultsWithData/LSH_with_Data_"$tempTimeStamp".json"

ELAPSED_TIME=$(($SECONDS - $START_TIME_HIVE_JOIN))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' Hive Join duration = '$timestamp' .' >> $tempLogFileName




# go back to root
cd $ROOT

# run filter on ../../../06_LshResultsWithData/LSH_with_Data_$tempTimeStamp.json
cd python/
echo $logPrefix"Running Filter: method = " $FILTER_MTHD
START_TIME_LINKING=$SECONDS
python filterLshResults.py \
--inputClusters "../../06_LshResultsWithData/LSH_with_Data_"$tempTimeStamp".json" \
--methodName $FILTER_MTHD \
--threshold $FILTER_THRESHOLD \
--outputFile "../../06_LshResultsWithData/LSH_with_Data_"$tempTimeStamp"_FILTERED.json"

ELAPSED_TIME=$(($SECONDS - $START_TIME_LINKING))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' Linking duration = '$timestamp' .' >> $tempLogFileName





# create provenance file
echo $logPrefix"Creating provenance..."
tempProvFileName="tempProvFile.json"
START_TIME_CREATE_PROV=$SECONDS
python createProvJson.py \
--lshParameters $LSH_NUM_OF_HASHES"_Hashes,"$LSH_ITEM_IN_BANDS"_Items_in_Bands,tokenizerFile_"$LSH_TOKENIZER_CONFIG_NAME"" \
--filterParameters $FILTER_MTHD",top_1,thrld_"$FILTER_THRESHOLD \
--sourceObj1 $KG_FILE_PATH \
--sourceObj2 $DATASOURCE_FILE_PATH \
--timeStamp $tempTimeStamp \
--output $tempProvFileName

ELAPSED_TIME=$(($SECONDS - $START_TIME_CREATE_PROV))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' Creating Provenance duration = '$timestamp' .' >> $tempLogFileName


# create  index file
echo $logPrefix"Creating index..."
tempIndexFileName="tempIndex.json"
START_TIME_CREATE_PROV=$SECONDS
python generateLinksIndex.py \
--inputLinksFilePath "../../06_LshResultsWithData/LSH_with_Data_"$tempTimeStamp"_FILTERED.json" \
--outputFile $tempIndexFileName

ELAPSED_TIME=$(($SECONDS - $START_TIME_CREATE_PROV))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' Creating Index duration = '$timestamp' .' >> $tempLogFileName

# merge filtered candidates to knowledgegraph
echo $logPrefix"Merging Results into Knowledge Graph"
START_TIME_KG_CONSTRUCTION=$SECONDS
python integrateLshLinks_v3.py \
--inputLshClusters "../../06_LshResultsWithData/LSH_with_Data_"$tempTimeStamp"_FILTERED.json" \
--linksIndexFilePath $tempIndexFileName \
--inputKnowledgGraph $KG_FILE_PATH \
--inputProvFile $tempProvFileName \
--inputBaseData $DATASOURCE_FILE_PATH \
--outputFile "../../07_KnowledgeGraphs/KG_"$tempTimeStamp".json"

ELAPSED_TIME=$(($SECONDS - $START_TIME_KG_CONSTRUCTION))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' Construction KG duration = '$timestamp' .' >> $tempLogFileName


# remove temporary provenance file
rm $tempProvFileName

# go back to root
cd ../../
