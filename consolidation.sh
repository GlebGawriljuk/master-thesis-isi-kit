#!/bin/bash
echo $$ > myscript.pid
#start script from: cd /Documents/Glebs_Folder/12_peopleClusters/00_pipeline
echo "Consolidation started."
chmod -R 777 ../master-thesis-isi-kit/
chmod -R 777 ../libs/

logPrefix="runMergePipelineConfig: "
source runMergePipelineConfig.txt

NEW_KG_FILE_NAME="KG_u_s_d_artist_v2_v_wikidata_46lsh_hy_by.json"

CLUSTER_FILE_PATH="/home/aifb-ls3-vm8/gga/06_LshResultsWithData/LSH_u_s_d_v_wikidata_artists_46t_F_hy_by.json"

logFileTimeStamp=$(date +'%m-%d-%Y_%H-%M-%S')
homePath=$OUTSIDE_ROOT"/pipelineLogs"
tempLogFileName=$homePath"/logs_pipeline_"$logFileTimeStamp".txt"
echo "Logs will be writen into "$tempLogFileName

# go back to root
cd $ROOT

# run filter on ../../../06_LshResultsWithData/LSH_with_Data_$tempTimeStamp.json
cd python/

# create provenance file
echo $logPrefix"Creating provenance..."
tempProvFileName="tempProvFile.json"
START_TIME_CREATE_PROV=$SECONDS
python createProvJson.py \
--lshParameters $LSH_NUM_OF_HASHES"_Hashes,"$LSH_ITEM_IN_BANDS"_Items_in_Bands,tokenizerFile_"$LSH_TOKENIZER_CONFIG_NAME"" \
--filterParameters $FILTER_MTHD",top_1,thrld_"$FILTER_THRESHOLD \
--sourceObj1 $KG_FILE_PATH \
--sourceObj2 $DATASOURCE_FILE_PATH \
--timeStamp $tempTimeStamp \
--output $tempProvFileName

ELAPSED_TIME=$(($SECONDS - $START_TIME_CREATE_PROV))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' Creating Provenance duration = '$timestamp' .' >> $tempLogFileName


# create  index file
echo $logPrefix"Creating index..."
tempIndexFileName="tempIndex.json"
START_TIME_CREATE_INDEX=$SECONDS
python generateLinksIndex.py \
--inputLinksFilePath $CLUSTER_FILE_PATH \
--outputFile $tempIndexFileName

ELAPSED_TIME=$(($SECONDS - $START_TIME_CREATE_INDEX))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' Creating Index duration = '$timestamp' .' >> $tempLogFileName



# merge filtered candidates to knowledgegraph
echo $logPrefix"Merging Results into Knowledge Graph"
START_TIME_KG_CONSTRUCTION=$SECONDS
python integrateLshLinks_v3.py \
--inputLshClusters $CLUSTER_FILE_PATH \
--linksIndexFilePath $tempIndexFileName \
--inputKnowledgGraph $KG_FILE_PATH \
--inputProvFile $tempProvFileName \
--inputBaseData $DATASOURCE_FILE_PATH \
--outputFile "../../07_KnowledgeGraphs/"$NEW_KG_FILE_NAME

ELAPSED_TIME=$(($SECONDS - $START_TIME_CREATE_PROV))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' Construction KG duration = '$timestamp' .' >> $tempLogFileName
echo $logPrefix' Construction KG duration = '$timestamp' .'

# remove temporary provenance file
rm $tempProvFileName

# go back to root
cd ../../
