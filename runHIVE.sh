#!/bin/bash
echo $$ > myscript.pid
#start script from: cd /Documents/Glebs_Folder/12_peopleClusters/00_pipeline

chmod -R 777 ../master-thesis-isi-kit/
chmod -R 777 ../libs/
chmod -R 777 ../pipelineLogs/

source runMergePipelineConfig.txt


logFileTimeStamp=$(date +'%m-%d-%Y_%H-%M-%S')
homePath=$OUTSIDE_ROOT"/pipelineLogs"
tempLogFileName=$homePath"/logs_pipeline_"$logFileTimeStamp".txt"

chmod -R 777 ../master-thesis-isi-kit/
chmod -R 777 ../libs/

# move LSH results
echo 'Moving LSH results to pipeline dir.'
START_TIME_MOVE_LSH_RESULTS=$SECONDS
cd $ROOT
cd ..

# copy LSH results for HIVE Join
echo 'Copying LSH results to HIVE dir.'
hive_target_dir_path="libs/hiveJoin/target/"
hive_target_file_path=$hive_target_dir_path"temp_lsh.json"
rm -r libs/hiveJoin/target/*
cp -v $LSH_RESULTS_FILE_PATH $hive_target_file_path

# copy kowledge graph for HIVE Join
echo 'Copying Knowledge graph to HIVE dir.'
hive_target_dir_path="libs/hiveJoin/source/"
hive_source_file_path=$hive_target_dir_path"temp_kg_for_hive.json"
rm -r libs/hiveJoin/source/*
#cp -v $KG_FILE_PATH

#trim KG for Hive Join
python /home/aifb-ls3-vm8/gga/master-thesis-isi-kit/python/trimKG4HiveJoin.py \
--inputKnowledgGraph $KG_FILE_PATH \
--propertyNameList $FILTER_PROPS \
--outputFile $hive_source_file_path

ELAPSED_TIME=$(($SECONDS - $START_TIME_MOVE_LSH_RESULTS))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' moving LSH results = '$timestamp' .' >> $tempLogFileName



# run join between knowledge graph and lsh results
echo $logPrefix"Running Hive Join: mergeing Knowledge Graph data into LSH clusters"
cd $HIVE_SCRIPT_FOLDER
START_TIME_HIVE_JOIN=$SECONDS
./join_LSH_and_KG_V2.sh

# concatinate resultes
echo 'Concatinating Hive Join tempt results.'
./concantLshPartitions.sh ../merged/ temp_LSH_with_KgData.json

# delete previous hive target file
echo 'Setting up files for next Hive Join.'
rm -r ../target/*

# move merged temp file to target
mv ../merged/temp_LSH_with_KgData.json ../target/temp_LSH_with_KgData.json

# delete previous hive source file
rm -r ../source/*

# copy datasource file to hive source dir
cp -v $DATASOURCE_FILE_PATH ../source/temp_datasource.json

# run hive join
echo $logPrefix"Running Hive Join: mergeing Datasource data into LSH clusters"
./join_TEMP_and_Datasource_V2.sh

# concatinate hive results
echo 'Concatinating final HIVE results.'
tempTimeStamp=$(date +'%m-%d-%Y_%H-%M-%S')
./concantLshPartitions.sh ../merged/ $HIVE_RESULT_FILE_NAME

# move hive merge result to pipeline dir
echo 'Moving HIVE results to pipeline dir.'
mv "../merged/"$HIVE_RESULT_FILE_NAME "../../../06_LshResultsWithData/"$HIVE_RESULT_FILE_NAME

ELAPSED_TIME=$(($SECONDS - $START_TIME_MOVE_LSH_RESULTS))
((sec=ELAPSED_TIME%60, ELAPSED_TIME/=60, min=ELAPSED_TIME%60, hrs=ELAPSED_TIME/60))
timestamp=$(printf "%d:%02d:%02d" $hrs $min $sec)
echo $logPrefix' Hive Join duration = '$timestamp' .' >> $tempLogFileName


