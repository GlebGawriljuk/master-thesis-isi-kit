__author__ = 'Gleb'

import sys
import json

#reload(sys)
#sys.setdefaultencoding('utf8')

def getFileLines(filePath):
    kgFile = open(filePath)
    return  kgFile.readlines()

def parse_args():
    global viafSingePropFilePath
    global listOfIdsFilePath
    global outputFilePath
    for arg_idx, arg in enumerate(sys.argv):
        if arg == "--viafSingePropFilePath":
            viafSingePropFilePath = sys.argv[arg_idx+1]
            continue
        if arg == "--listOfIdsFilePath":
            listOfIdsFilePath = sys.argv[arg_idx+1]
            continue
        if arg == "--outputFilePath":
            outputFilePath = sys.argv[arg_idx+1]
            continue
            
            
viafSingePropFilePath = None
listOfIdsFilePath = None
outputFilePath = None

if __name__ == '__main__':

    parse_args()

    limitedViafFile = open(outputFilePath, 'w')

    print "Reading in files ..."
    viafSingePropFile = getFileLines(viafSingePropFilePath)
    listOfIdsLines = getFileLines(listOfIdsFilePath)

    limitedIDList = []

    for i_id in listOfIdsLines:
        limitedIDList.append(i_id.strip())

    c = 0

    #with open(viafSingePropFilePath) as f:
    #    for line in f:
    for line in viafSingePropFile:
        c = c + 1
        print "Processing line " + str(c) #+ " of  " + str(len(viafDumpLines))
        singlePropLine = line.strip().split('\t')
        #dumpLineSubjectID = singlePropLine[0].replace("http://viaf.org/viaf/","").strip()
        dumpLineSubjectID = singlePropLine[0].strip()
        if c == 1:
            limitedViafFile.write(line)
        if dumpLineSubjectID in limitedIDList:
            limitedViafFile.write(line)