__author__ = 'rajagopal067'
import sys
#import ngram
#import unicodedata
import re
#import logging




def merge_Property_Values(self,prevValues,newValues):
    #print "add " + newValues + " to " + prevValues
    prevValuesList = list(prevValues.strip().split(" "))
    newValuesList = list(newValues.strip().split(" "))
    for newPropValue in newValuesList:
        valueExist = False
        for propValue in prevValuesList:
            propValue = propValue.replace("\"", "").strip()
            newPropValue = newPropValue.replace("\"", "").strip()
            #print "compare EXISTING VALUE -" + propValue + "- to NEW VALUE-" + newPropValue + "-"
            if propValue == newPropValue:
                valueExist = True
                break
        if valueExist == False and newPropValue != "NA":
            #print "Added new value -" + newPropValue + "-"
            prevValues = prevValues + " " + newPropValue
            prevValues = prevValues.replace("NA", "").replace("\"", "").strip()
            prevValues = re.sub('\s+',' ', prevValues)
            #print "Value added: " + prevValues
    return prevValues

def resourceLineToString(resourceLineList):
    resourceLines = []
    resourceLine = ""
    for idx, values in enumerate(resourceLineList):
        if idx == 0 :
            resourceLine = values[0].strip()
            resourceLines.append(values[0])
        else:
            for currentVal in values:
                for currentLine in resourceLines:
                    currentLine = currentLine + currentVal
            resourceLine = resourceLine + "\t" + re.sub('\s+',' ', currentVal.replace("\"", "").strip())
    return resourceLine
def writeToFile(file, data):
    for record in data:
        recordStringsList = []
        for attrValues in record:
            if len(recordStringsList)==0:
                for attrValue in attrValues:
                    recordStringsList.append(attrValue)
            else:
                tempList = []
                if len(attrValues)>0:
                    for attrValue in attrValues:
                        for exitRecordString in recordStringsList:
                            tempList.append(exitRecordString + "\t" + attrValue)
                    recordStringsList = tempList
                else:
                    for exitRecordString in recordStringsList:
                        tempList.append(exitRecordString + "\t" + "")
                    recordStringsList = tempList
        for line in recordStringsList:
            file.write(line + "\n")

def parse_args():
    global inputFilename
    global outputFilename
    global separator

    for arg_idx, arg in enumerate(sys.argv):
        if arg == "--input":
            inputFilename = sys.argv[arg_idx+1]
            continue
        if arg == "--output":
            outputFilename = sys.argv[arg_idx+1]
            continue
        if arg == "--separator":
            separator = sys.argv[arg_idx+1]
            continue


inputFilename = None
outputFilename = None
separator = "\t"

def die():
    print "Please input the required parameters"
    print "Usage: GenerateTokens.py --input <input filename> --output <output filename> [--separator <sep=\\t>] [--computengramcharacters <True or False>] [--computengramwords <True or False>] " \
          "[--ngramsize <size>]"
    exit(1)


# check if this is main because when this this class is called from other module the following code should not get executed

if __name__ == '__main__':

    parse_args()

  #  if len(sys.argv) < 3:
   #     die()

    file = open(inputFilename,'r')
    outputFile = open(outputFilename,'w')
    newResource = False 
    prevResourceID = None
    prevResourceLine = None
    prevResourceList = None
    recordList = []
    for line in file:
        currentRecordLineList = list(line.rstrip('\n').split(separator))

        resourceID = currentRecordLineList[0]
        if len(recordList) > 0:
            latestURI = recordList[len(recordList)-1][0][0]
        else:
            latestURI = None
        if resourceID == latestURI: # new line is about previous resource



            for prop_idx, resourceProperty in enumerate(currentRecordLineList):
                if prop_idx > 0: # validate only values, not resource id

                    if len(resourceProperty)>0:
                        recordList[len(recordList)-1][prop_idx].append(resourceProperty)

        else: # current line is a new resource

            currentRecordLineLOV = list(line.rstrip('\n').split(separator)) # init resource list for next resource
            currentRecordLineList = []
            for currentValue in currentRecordLineLOV:
                tempList = []
                if len(currentValue)>0:
                    tempList.append(currentValue)
                currentRecordLineList.append(tempList)

            recordList.append(currentRecordLineList)

    exit
    #if False:
     #    outputFile.write(resourceLineToString(recordList) + "\n")

    file.close()
    writeToFile(outputFile, recordList)






