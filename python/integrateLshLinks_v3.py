__author__ = 'glebgawriljuk'
import sys
import json
from copy import deepcopy
from datetime import datetime
from knowledgeGraphHelpers import createRoleObjectFromValue, addRoleObjToNewEntityAttr, addProvToRole, prettyPrintJson, addRoleObjToExistEntityAttr, writeJsonLine2File, prettyWriteJson2File,createSameAsRoleObject, createNewClustersFromBaseFile, prettyPrintListOfJsonLines

reload(sys)
sys.setdefaultencoding('utf8')

# TODO obsolete: remove
# merges candidateJson into targetJson and appends it to newKnowledgeGraphLines
def addToEntityCluster(targetJson, candidateJson, inputKnowledgeGraphLines):

    attrNotToBeMerged = ["@context", "uri","score"]

    printLog = False
    addToEntityCluster_logs = None

    startTime = datetime.now()

    if printLog:
        addToEntityCluster_logs = open("../../logs/addToEntityCluster_logs_" + str(startTime) +".txt", 'w')
        addToEntityCluster_logs.write("Candidate " + candidateJson["uri"] + " is merged to cluster " + targetJson["uri"] + "\nThe cluster before the merge:\n")
        addToEntityCluster_logs.write(prettyPrintJson(targetJson) + "\n")

    for candidate_AttrKey, candidate_AttrValue in candidateJson.iteritems(): # for each candidate attribute, check it attribute exists in entity

        if verbose: print str(datetime.now()) + ": Checking candidate attribute " + candidate_AttrKey
        candidate_AttrKey_Found = False

        for target_AttrKey, target_attrRole in targetJson.iteritems(): #iterate over all attribute Roles

            # same attribute key was found, hence must be merged
            if candidate_AttrKey == target_AttrKey and not candidate_AttrKey in attrNotToBeMerged:

                candidate_AttrKey_Found = True

                if verbose: print str(datetime.now()) + ": Attribute " + candidate_AttrKey + " exists in entity."

                targetListOfDict = []
                candidateListOfDict = []
                #  target attribute are either dict or list of dicts
                if isinstance(target_attrRole, dict): # entity has only one attribute Role
                    targetListOfDict.append(target_attrRole)
                elif isinstance(target_attrRole, list):
                    targetListOfDict = target_attrRole
                else:
                    print str(datetime.now()) + ": Unknown format for target role " + str(target_attrRole)

                if isinstance(candidate_AttrValue, basestring) or isinstance(candidate_AttrValue, int) or isinstance(candidate_AttrValue, float) or isinstance(candidate_AttrValue, dict):
                    candidateListOfDict.append(candidate_AttrValue)
                elif isinstance(candidate_AttrValue, list):
                    candidateListOfDict = candidate_AttrValue
                else:
                    print str(datetime.now()) + ": Unknown format for candidate value " + str(candidate_AttrValue)

                mergedCandidateValues = []

                for candidate_value_i in candidateListOfDict:
                    candidate_value_i_Found = False

                    if isinstance(candidate_value_i, basestring):
                        candidate_value_i.strip()

                    for target_attrRole_idx, target_attrRole_i in enumerate(targetListOfDict):

                        if isinstance(target_attrRole_i[target_AttrKey], basestring):
                            target_attrRole_i[target_AttrKey].strip()

                        if not candidate_value_i in mergedCandidateValues:

                            if target_attrRole_i[target_AttrKey] == candidate_value_i:
                                candidate_value_i_Found = True

                                mergedCandidateValues.append(candidate_value_i)

                                addProvToRole(target_attrRole_i, "wasGeneratedBy", provJSON)
                                targetListOfDict[target_attrRole_idx] = target_attrRole_i

                                break


                    if not candidate_value_i_Found: # the current candidate_value_i does not exist in the knowledge graph, add new role object for this value

                        if verbose: print str(datetime.now()) + ": 6---Value [" +  candidate_value_i + "] does not exists. Add new Role object candidate_AttrKey." + "\n" + prettyPrintJson(targetJson) #OK

                        tempRoleObj = createRoleObjectFromValue(candidate_value_i, candidate_AttrKey, provJSON)
                        #addRoleObjToExistEntityAttr(targetJson, tempRoleObj, candidate_AttrKey)
                        targetListOfDict.append(deepcopy(tempRoleObj))

                        if verbose: print str(datetime.now()) + ": New Role added to list of Roles for key: " + candidate_AttrKey + "\n" + prettyPrintJson(targetJson)

                targetJson[target_AttrKey] = targetListOfDict


        # all target attribute tested
        if not candidate_AttrKey_Found and not candidate_AttrKey in attrNotToBeMerged: # current candidate attribute is not in the cluster, add it to the cluster

            candidateValueList = []
            if not isinstance(candidate_AttrValue, list):
                candidateValueList.append(candidate_AttrValue)

            elif isinstance(candidate_AttrValue, list):
                candidateValueList = candidate_AttrValue
            else:
                print str(datetime.now()) + ": Unknown format for candidate value " + str(candidate_AttrValue)

            for candidate_value_idx, candidate_value_i in enumerate(candidateValueList):

                if verbose: print str(datetime.now()) + ": 7.2---Key  [" +  candidate_AttrKey + "] does not exists and is a LIST. Add new Role object\n" + prettyPrintJson(targetJson) #OK
                tempRoleObj = createRoleObjectFromValue(candidate_value_i, candidate_AttrKey, provJSON)

                if candidate_value_idx == 0:
                    addRoleObjToNewEntityAttr(targetJson, tempRoleObj, candidate_AttrKey)

                else:
                    addRoleObjToExistEntityAttr(targetJson, tempRoleObj, candidate_AttrKey)

                if verbose: print str(datetime.now()) + ": Added new Role obj for key " + candidate_AttrKey + "\n" + prettyPrintJson(targetJson)


            if verbose: print str(datetime.now()) + ": SameAs relation added: \n" + prettyPrintJson(targetJson)

            createSameAsRoleObject(targetJson, candidateJson, "uri", "wasGeneratedBy", provJSON)

            # if verbose: print str(datetime.now()) + ": SameAs relation added: \n" + prettyPrintJson(entityJSON)
            if verbose:print str(datetime.now()) + ": Candidate merge into " + targetJson["uri"] + " completed:\n" + prettyPrintJson(targetJson)

            if printLog:
                addToEntityCluster_logs.write("The cluster after the merge:\n")
                addToEntityCluster_logs.write(prettyPrintJson(targetJson) + "\n")


    if verbose: print str(datetime.now()) + ": merged cluster:\n" + prettyPrintJson(targetJson)
    inputKnowledgeGraphLines.append(deepcopy(json.dumps(targetJson)))


    endTime = datetime.now()
    timeDelta = endTime - startTime
    if verbose: print str(datetime.now()) + ": Total duration: " + str(timeDelta)

    if printLog:
        addToEntityCluster_logs.write("Total duration = " + str(timeDelta))
        addToEntityCluster_logs.close()

# This method consolidates a knowledge graph entity with and data source entity into a new knowledge graph entity.
# The consolidation is accomplished according to the knowledge graph data model.
# kgEntity: the knowledge graph entity.
# dataSourceEntity: the data source entity.
def consolidateEntities(kgEntity, dataSourceEntity):

    attrNotToBeMerged = ["@context", "uri","score"]

    for candidate_AttrKey, candidate_AttrValue in dataSourceEntity.iteritems(): # for each candidate attribute, check it attribute exists in entity

        candidate_AttrKey_Found = False

        for target_AttrKey, target_attrRole in kgEntity.iteritems(): #iterate over all attribute Roles

            # same attribute key was found, hence must be merged
            if candidate_AttrKey == target_AttrKey and not candidate_AttrKey in attrNotToBeMerged:

                candidate_AttrKey_Found = True

                targetListOfDict = []
                candidateListOfValues = []
                #  target attribute are either dict or list of dicts
                if isinstance(target_attrRole, dict): # entity has only one attribute Role
                    targetListOfDict.append(target_attrRole)
                elif isinstance(target_attrRole, list):
                    targetListOfDict = target_attrRole
                else:
                    print str(datetime.now()) + ": Unknown format for target role " + str(target_attrRole)

                if isinstance(candidate_AttrValue, basestring) or isinstance(candidate_AttrValue, int) or isinstance(candidate_AttrValue, float) or isinstance(candidate_AttrValue, dict):
                    candidateListOfValues.append(candidate_AttrValue)
                elif isinstance(candidate_AttrValue, list):
                    candidateListOfValues = candidate_AttrValue
                else:
                    print str(datetime.now()) + ": Unknown format for candidate value " + str(candidate_AttrValue)

                mergedCandidateValues = []

                for candidate_value_i in candidateListOfValues:
                    candidate_value_i_Found = False

                    if isinstance(candidate_value_i, basestring):
                        candidate_value_i.strip()

                    for target_attrRole_idx, target_attrRole_i in enumerate(targetListOfDict):

                        if isinstance(target_attrRole_i[target_AttrKey], basestring):
                            target_attrRole_i[target_AttrKey].strip()

                        if not candidate_value_i in mergedCandidateValues:

                            if target_attrRole_i[target_AttrKey] == candidate_value_i: # same value found in kg entity
                                candidate_value_i_Found = True

                                mergedCandidateValues.append(candidate_value_i)

                                addProvToRole(target_attrRole_i, "wasGeneratedBy", provJSON)
                                targetListOfDict[target_attrRole_idx] = target_attrRole_i

                                break


                    if not candidate_value_i_Found: # the current candidate_value_i does not exist in the knowledge graph, add new role object for this value

                        if verbose: print str(datetime.now()) + ": 6---Value [" +  candidate_value_i + "] does not exists. Add new Role object candidate_AttrKey." + "\n" + prettyPrintJson(kgEntity) #OK

                        tempRoleObj = createRoleObjectFromValue(candidate_value_i, candidate_AttrKey, provJSON)
                        #addRoleObjToExistEntityAttr(targetJson, tempRoleObj, candidate_AttrKey)
                        targetListOfDict.append(deepcopy(tempRoleObj))

                        if verbose: print str(datetime.now()) + ": New Role added to list of Roles for key: " + candidate_AttrKey + "\n" + prettyPrintJson(kgEntity)

                kgEntity[target_AttrKey] = targetListOfDict


        # all kg entity attribute tested
        if not candidate_AttrKey_Found and not candidate_AttrKey in attrNotToBeMerged: # current candidate attribute is not in the cluster, add it to the cluster

            candidateValueList = []
            if not isinstance(candidate_AttrValue, list):
                candidateValueList.append(candidate_AttrValue)

            elif isinstance(candidate_AttrValue, list):
                candidateValueList = candidate_AttrValue
            else:
                print str(datetime.now()) + ": Unknown format for candidate value " + str(candidate_AttrValue)

            for candidate_value_idx, candidate_value_i in enumerate(candidateValueList):

                if verbose: print str(datetime.now()) + ": 7.2---Key  [" +  candidate_AttrKey + "] does not exists and is a LIST. Add new Role object\n" + prettyPrintJson(kgEntity) #OK
                tempRoleObj = createRoleObjectFromValue(candidate_value_i, candidate_AttrKey, provJSON)

                if candidate_value_idx == 0:
                    addRoleObjToNewEntityAttr(kgEntity, tempRoleObj, candidate_AttrKey)

                else:
                    addRoleObjToExistEntityAttr(kgEntity, tempRoleObj, candidate_AttrKey)

                if verbose: print str(datetime.now()) + ": Added new Role obj for key " + candidate_AttrKey + "\n" + prettyPrintJson(kgEntity)


    createSameAsRoleObject(kgEntity, dataSourceEntity, "uri", "wasGeneratedBy", provJSON)

    return kgEntity

def parse_args():
    global inputLshClusters
    global linksIndexFilePath
    global inputKnowledgeGraph
    global inputProvFilename
    global inputBaseData
    global outputFile
    for arg_idx, arg in enumerate(sys.argv):
        if arg == "--inputLshClusters":
            inputLshClusters = sys.argv[arg_idx+1]
            continue
        if arg == "--linksIndexFilePath":
            linksIndexFilePath = sys.argv[arg_idx+1]
            continue
        if arg == "--inputKnowledgGraph":
            inputKnowledgeGraph = sys.argv[arg_idx+1]
            continue
        if arg == "--inputProvFile":
            inputProvFilename = sys.argv[arg_idx+1]
            continue
        if arg == "--inputBaseData":
            inputBaseData = sys.argv[arg_idx+1]
            continue
        if arg == "--outputFile":
            outputFile = sys.argv[arg_idx+1]
            continue


inputLshClusters = None
inputKnowledgeGraph = None
linksIndexFilePath = None
inputProvFilename = None
inputBaseData = None
verbose = False
outputFile = None
knowledgeGraphLines = None

timeStart = datetime.now()

# the empty extended knowledge graph
newKnowledgeGraphLines = []

# a list of URIs for data source entities which are consolidated
mergedDataSourceEntityURIs = []
extendedClusterURIs = []
parse_args()


inputBaseDataFile = open(inputBaseData)
baseDataLines = inputBaseDataFile.readlines()


provFile = open(inputProvFilename)
provJSON = json.load(provFile)


# if a knowledge graph and a set of cluster are do consolidation
if inputKnowledgeGraph and inputLshClusters:

    inputKnowledgeGraphFile = open(inputKnowledgeGraph)
    knowledgeGraphLines = inputKnowledgeGraphFile.readlines()

    linksIndexFile = open(linksIndexFilePath)
    linksIndexLines = linksIndexFile.readlines()
    indexInfoObj = json.loads(linksIndexLines[len(linksIndexLines)-1])
    indexList = indexInfoObj["idxs"]


    counter = 0

    # iterate over each knowledge graph entity
    for kgEntityLine in knowledgeGraphLines:
        counter = counter + 1
        print "Kg Entity " + str(counter) + " of " + str(len(knowledgeGraphLines)-1) + " is currently processed."

        kgEntityJson = json.loads(kgEntityLine.strip())

        # if the object does not have a uri-property, it is not an entity
        if "uri" in kgEntityJson:

            kgEntityURI = kgEntityJson["uri"]

            # get target entity for current knowledge graph entity
            for index in indexList:
                indexURI = index[0]

                # current knowledge graph entity is a matched candidate
                # get the target entities and consolidate them with the current knowledge graph entity
                if indexURI == kgEntityURI:
                    linksIndexLinesIdx = index[1]
                    linkJson = json.loads(linksIndexLines[linksIndexLinesIdx].strip())
                    targetsJson = linkJson["targets"]

                    # consolidate each target entity of the current knowledge graph candidate entity
                    for targetJson in targetsJson:
                        kgEntityJson = consolidateEntities(kgEntityJson,targetJson)
                        mergedDataSourceEntityURIs.append(targetJson["uri"])
                    break # go to next knowledge graph entity

            # append the consolidate entity to the extended knowledge graph
            newKnowledgeGraphLines.append(deepcopy(json.dumps(kgEntityJson)))

    # all found links are consolidate in the newKnowledgeGraphLines

# include all data source entities which were not consolidated as target entities in the extended knowledge graph
createNewClustersFromBaseFile(baseDataLines, newKnowledgeGraphLines, mergedDataSourceEntityURIs, provJSON)

# add a JSON object to the knowledge graph holding the number of entities
numOfKgEntities = int(len(newKnowledgeGraphLines))
numOfEntitiesObj = {}
numOfEntitiesObj["numberOfEntities"] = numOfKgEntities
newKnowledgeGraphLines.append(deepcopy(json.dumps(numOfEntitiesObj)))


print "Extended knowledge graph is exported to JSON."
writeJsonLine2File(newKnowledgeGraphLines, outputFile)

timeDelta =  datetime.now() - timeStart
print "Consolidation done."
