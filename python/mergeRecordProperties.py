__author__ = 'gg'
import sys
#import ngram
#import unicodedata
import re
#import logging
import itertools

class TokenPreprocessor:

    def merge_Property_Values(self,prevValues,newValues):
         
        return prevValues
   # def resourceLineToString(self,resourceLineList):
        
        #return resourceLine

    def clearPrefix(self,value):
        return value.replace("http://dbpedia.org/resource/","")

def parse_args():
    global inputFilename
    global outputFilename
    global separator
    global n_gram_words
    global n_gram_characters
    global n_gram_size
    global numOfProps

    for arg_idx, arg in enumerate(sys.argv):
        if arg == "--input":
            inputFilename = sys.argv[arg_idx+1]
            continue
        if arg == "--output":
            outputFilename = sys.argv[arg_idx+1]
            continue
        if arg == "--numOfProps":
            numOfProps = sys.argv[arg_idx+1]
            continue

inputFilename = None
outputFilename = None
numOfProps = None
separator = "\t"

def die():
    print "Please input the required parameters"
    exit(1)


# check if this is main because when this this class is called from other module the following code should not get executed

if __name__ == '__main__':

    parse_args()

  #  if len(sys.argv) < 3:
   #     die()

    file = open(inputFilename,'r')
    fileLines = file.readlines()
    outputFile = open(outputFilename,'w')
    tokenPreprocessor = TokenPreprocessor();
    newResource = False
    prevResourceID = None
    prevColIdx = None
    placeholderString = 'NONE'
    recordList = [[placeholderString] for _ in range(int(numOfProps) + 1)]
    currentColIdx = -1
    printCount = 0
    for line in fileLines:
        printCount = printCount + 1
        print "Processing line " + str(printCount)
        currentRecordList = list(line.rstrip('\n').split(separator))
        resourceID = currentRecordList[0]
        if placeholderString == recordList[0][0]:
            recordList[0].append(resourceID)
            recordList[0].remove(placeholderString)
        currentColIdx = int(currentRecordList[1])
        if resourceID == prevResourceID or prevResourceID == None :
            if placeholderString in recordList[int(currentRecordList[1])]:
                recordList[int(currentRecordList[1])].remove(placeholderString)
            recordList[int(currentRecordList[1])].append(currentRecordList[2])
            prevResourceID = resourceID

        else:# current line is a new resource
            #print recordList
            if not (len(recordList[1]) == 1 and recordList[1][0] == placeholderString):
                for productElement in itertools.product(*recordList):
                    currentProductRow = ""
                    for tempValue in productElement:
                        if currentProductRow == "":
                            currentProductRow = tempValue.strip()
                        else:
                            currentProductRow = currentProductRow + "\t" + tempValue.strip()
                    #print currentProductRow
                    outputFile.write(str(currentProductRow.replace(placeholderString, "")) + "\n")
            recordList = [[placeholderString] for _ in range(int(numOfProps) + 1)]
            #print "NEW RECORD :" + resourceID
            prevResourceID = resourceID
            prevColIdx = int(currentRecordList[1])

            if placeholderString == recordList[0][0]:
                recordList[0].append(resourceID)
                recordList[0].remove(placeholderString)

            if placeholderString in recordList[int(currentRecordList[1])]:
                recordList[int(currentRecordList[1])].remove(placeholderString)
            recordList[int(currentRecordList[1])].append(currentRecordList[2])

            #print recordList
                #outputFile.write(tokenPreprocessor.resourceLineToString(prevResourceList) + "\n")
    #print recordList
    if not (len(recordList[1]) == 1 and recordList[1][0] == placeholderString):
        for productElement in itertools.product(*recordList):
            currentProductRow = ""
            for tempValue in productElement:
                if currentProductRow == "":
                    currentProductRow = tempValue
                else:
                    currentProductRow = currentProductRow + "\t" + tempValue
            #print currentProductRow
            outputFile.write(str(currentProductRow.replace(placeholderString, "")) + "\n")
    file.close()
    outputFile.close()






