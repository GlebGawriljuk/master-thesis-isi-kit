__author__ = 'Gleb'

import sys
from hybrid_jaccard.HybridJaccard import StringMatcher
from ngram import NGram

# python testHybridJaccardScore.py --s1 'test a' --s2 'test b'

def jaccard_similarity(a,b, n):
    n = NGram(key=lambda x:x[1], N=4)
    item = (3,a)
    x = list(n.splititem(item))
    #print x
    item = (3,b)
    y = list(n.splititem(item))
    #print y
    intersection_cardinality = len(set.intersection(*[set(x), set(y)]))
    union_cardinality = len(set.union(*[set(x), set(y)]))
    return intersection_cardinality/float(union_cardinality)

def parse_args():
    global s1
    global s2
    for arg_idx, arg in enumerate(sys.argv):
        if arg == "--s1":
            s1 = sys.argv[arg_idx+1]
            continue
        if arg == "--s2":
            s2 = sys.argv[arg_idx+1]
            continue


s1 = None
s2 = None

parse_args()


s1 = "Ronald Bladen"
#s2 = "William Joshua Blackmon"
s2 = "Robert Bladen"

#hj = HybridJaccard("hybrid_jaccard/config.json")
sm = StringMatcher("smith", 0.7)
queryFields = s1.strip().split(" ")
candidateFields = s2.strip().split(" ")
score = sm.sim_measure(candidateFields, queryFields)

print "Hybrid Jaccard: " + str(score)
print "Jaccard: " + str(jaccard_similarity(s1,s2, 4))