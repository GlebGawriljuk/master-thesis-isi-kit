__author__ = 'Gleb'

import sys
import json

reload(sys)
sys.setdefaultencoding('utf8')

def getFileLines(filePath):
    tempFile = open(filePath)
    return  tempFile.readlines()

def generateSameAs(fromURI,toURI):
    return "<" + str(fromURI) + ">" + "\t" + "<https://schema.org/sameAs>" + "\t" + "<" + str(toURI) +"> ." + "\n"

def parse_args():
    global kgFilePath
    global outputFilePath
    global targetPrefix
    for arg_idx, arg in enumerate(sys.argv):
        if arg == "--kgFilePath":
            kgFilePath = sys.argv[arg_idx+1]
            continue
        if arg == "--outputFilePath":
            outputFilePath = sys.argv[arg_idx+1]
            continue
        if arg == "--targetPrefix":
            targetPrefix = sys.argv[arg_idx+1]
            continue


kgFilePath = None
outputFilePath = None
targetPrefix = None

parse_args()

kgLines = getFileLines(kgFilePath)

outputFile = open(outputFilePath, 'w')
count = 0
kgLength = len(kgLines)
for kgEntity in kgLines:
    count = count + 1
    print "Entity " + str(count) + " from " + str(kgLength) + " is processed."
    kgEntityObj = json.loads(kgEntity.strip())
    if "sameAs" in kgEntityObj:
        sameAsObjs = kgEntityObj["sameAs"]
        targetURIs = []
        destinationURIs = []
        if isinstance(sameAsObjs, list):
            for sameAsObj in sameAsObjs:
                tempURI = sameAsObj["sameAs"]
                if tempURI.startswith(targetPrefix):
                    targetURIs.append(tempURI)
                else:
                    destinationURIs.append(tempURI)

            for targetURI in targetURIs:
                for destinationURI in destinationURIs:
                    outputFile.write(generateSameAs(targetURI, destinationURI))


outputFile.close()