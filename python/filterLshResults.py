__author__ = 'glebgawriljuk'

import sys
import json
from datetime import datetime
from copy import deepcopy
from knowledgeGraphHelpers import prettyPrintJson, writeJsonLine2File
from hybrid_jaccard.HybridJaccard import HybridJaccard

# python filterLshResults.py --inputClusters fileJSON_inline.json --methodName sameAttrValue,hybridJaccard --threshold 0.8 --outputFile testOutput

reload(sys)
sys.setdefaultencoding('utf8')

# A method to generate sameAs-links based on the linked candidates.
# sameAsFile: the file to write the sameAs-links in.
# targetJson: the target entity.
# candidateJson: the matched candidate set.
def generateSameAs(sameAsFile,targetJson, candidateJson):
    if isinstance(targetJson, basestring): # if no data was joined the targetJson will be a
        sameAsFile.write("<" + str(targetJson) + ">" + "\t" + "<https://schema.org/sameAs>" + "\t" + "<" + str(candidateJson["uri"]["uri"]) +">" + "\n")
    elif isinstance(targetJson, dict):
        sameAsFile.write("<" + str(targetJson["uri"]) + ">" + "\t" + "<https://schema.org/sameAs>" + "\t" + "<" + str(candidateJson["uri"]["uri"]) +">" + "\n")

def printToLogs(logFile, targetJson, candidateJson, attrName):
    if attrName in targetJson and attrName in candidateJson:
        if isinstance(targetJson[attrName], basestring):
            if isinstance(candidateJson[attrName], dict):
                #logFile.write(str(targetJson["uri"]) + "," + str(targetJson[attrName][attrName]) + "," + str(candidateJson["uri"]) + "," + str(candidateJson[attrName]) + "\n")
                logFile.write(str(targetJson["uri"]) + "\t" + str(candidateJson["uri"]) + "\n")
            elif isinstance(candidateJson[attrName], list):
                #logFile.write(str(targetJson["uri"]) + "," + str(targetJson[attrName][attrName]) + "," + str(candidateJson["uri"]) + "," + str(candidateJson[attrName][0])+ "\n")
                logFile.write(str(targetJson["uri"]) + "\t" + str(candidateJson["uri"]) + "\n")

        elif isinstance(targetJson[attrName], list):
            if isinstance(candidateJson[attrName], dict):
                #logFile.write(str(targetJson["uri"]) + "," + str(targetJson[attrName][0][attrName]) + "," + str(candidateJson["uri"]) + "," + str(candidateJson[attrName])+ "\n")
                logFile.write(str(targetJson["uri"]) + "\t" + str(candidateJson["uri"]) + "\n")
            elif isinstance(candidateJson[attrName], list):
                #logFile.write(str(targetJson["uri"]) + "," + str(targetJson[attrName][0][attrName]) + "," + str(candidateJson["uri"]) + "," + str(candidateJson[attrName][0])+ "\n")
                logFile.write(str(targetJson["uri"]) + "\t" + str(candidateJson["uri"]) + "\n")
    else:
        if isinstance(targetJson, basestring): # if no data was joined the targetJson will be a string
            logFile.write(str(targetJson) + "\t" + str(candidateJson["uri"])+ "\n")
        elif isinstance(targetJson, dict):
            logFile.write(str(targetJson["uri"]) + "\t" + str(candidateJson["uri"])+ "\n")

# A method to evaluate a candidate in candidateObj with the target entity in targetObj based on equal property values.
# The property is defined by the attrName-parameter.
# The method returns True if the candidate is a match, False otherwise.
# targetObj: the target entity.
# candidateObj: the candidate set.
# attrName: the property name upon which the candidates are evaluated.
def filterSameAttrStrValue(targetObj, candidateObj, attrName):
    # apply rule only if the entities include the property to be evaluated
    if attrName in targetObj and attrName in candidateObj:
        # data source property is a string value
        if isinstance(targetObj[attrName], basestring):
            # knowledge graph property is a single dict
            if isinstance(candidateObj[attrName],dict):
                if verbose: print str(datetime.now()) + ": filterSameAttrStrValue:comparing " + str(targetObj[attrName]) + " with " + str(candidateObj[attrName][attrName])
                if targetObj[attrName] == candidateObj[attrName][attrName]:
                    return True
                else:
                    return False
            # knowledge graph property is a list of dict
            elif isinstance(candidateObj[attrName],list):
                for candidateValue_i in candidateObj[attrName]:
                    if targetObj[attrName] == candidateValue_i[attrName]:
                        return True
                return False
            else:
                if verbose: print str(datetime.now()) + ": filterSameAttrStrValue: values to be compared are not basestring."
                return True

        # data source property is a list of string values
        if isinstance(targetObj[attrName], list):
            for targetDict_i in targetObj[attrName]:
                # knowledge graph property is a single dict
                if isinstance(targetDict_i, basestring) and isinstance(candidateObj[attrName],dict):
                    if verbose: print str(datetime.now()) + ": filterSameAttrStrValue:comparing " + str(targetDict_i[attrName]) + " with " + str(candidateObj[attrName])
                    if targetDict_i == candidateObj[attrName][attrName]:
                        return True

               # knowledge graph property is a list of dict
                elif isinstance(targetDict_i, basestring) and isinstance(candidateObj[attrName],list):
                    for candidateValue_i in candidateObj[attrName]:
                        if targetDict_i == candidateValue_i[attrName]:
                            return True

                else:
                    if verbose: print str(datetime.now()) + ": filterSameAttrStrValue: values to be compared are not basestring."
                    return True

            return False
    else:
       if verbose: print str(datetime.now()) + ": filterSameAttrStrValue: objects do not have the key " + attrName
       return True

# The Hybrid-Jaccard method invoking the Hybrid-Jaccard package to apply the rule.
# The method returns the cluster containing the target entity and only the matched candidates.
# clusterJSON: the cluster holding the target entity and its candidates.
# threshold: the Hybrid-Jaccard-similarity threshold.
# keyName: the property name based upon which the Hybrid-Jaccard-similarity is calculated.
def hybridJaccard(clusterJSON, threshold, keyName):
    hj = HybridJaccard("hybrid_jaccard/config.json")
    return hj.getTopCandidateURI(clusterJSON, threshold, keyName)

def parse_args():
    global inputClusters
    global methods
    global threshold
    global outputFile
    global groundTruthFilepath
    global groundTruthIndex
    global genSameAs
    for arg_idx, arg in enumerate(sys.argv):
        if arg == "--inputClusters":
            inputClusters = sys.argv[arg_idx+1]
            continue
        if arg == "--methodName":
            methods = sys.argv[arg_idx+1].split(",")
            continue
        if arg == "--threshold":
            threshold = float(sys.argv[arg_idx+1])
            continue
        if arg == "--outputFile":
            outputFile = sys.argv[arg_idx+1]
            continue
        if arg == "--groundTruth":
            groundTruthFilepath = sys.argv[arg_idx+1]
            continue
        if arg == "--groundTruthIndex":
            groundTruthIndex = int(sys.argv[arg_idx+1])
            continue
        if arg == "--sameAs":
            genSameAs = True
            continue


inputClusters = None
methods = None
threshold = None
outputFile = None
filterResult = None
groundTruthFilepath = None
groundTruthIndex = None
groundTruthFile = None
groundTruthLines = None

printLinksLog = False
genSameAs = False
addToEntityCluster_logs = None
sameAsFile = None

verbose = False
finalCluster = {}
tempMatchesCluster = {}

print "Python Linking START."

parse_args()

timeStart4Logs = datetime.now().strftime("%d-%B-%Y_%I-%M%p")
timeStart = datetime.now()

inputClustersFile = open(inputClusters)
inputClustersJsonLines = inputClustersFile.readlines()

if groundTruthFilepath:
    groundTruthFile = open(groundTruthFilepath)
    groundTruthLines = groundTruthFile.readlines()



# TODO DEBUG: remove
if printLinksLog: addToEntityCluster_logs = open("sameAsLinks_logs_" + str(timeStart4Logs) +".txt", 'w')
if genSameAs: sameAsFile = open("sameAsTriples_" + str(timeStart4Logs) +".nt", 'w')

# the final cluster after the filtering
filteredClusterLines = []

# iterate over each cluster and evaluate it
for clusterJsonLine in inputClustersJsonLines:

    clusterJSON = json.loads(clusterJsonLine.strip())
    targetObj = clusterJSON["uri"]
    finalCluster["uri"] = targetObj
    finalCluster["candidates"] = clusterJSON["candidates"]

    # iterate over each method
    for method in methods:

        if method in ["birthYear","birthYearDeathYear"]:

            tempMatchesCluster["uri"] = targetObj
            tempMatchesCluster["candidates"] = []

            # iterate over each candidate in the cluster
            for candidateObj in finalCluster["candidates"]:
                candidateURI = candidateObj["uri"]

                # compare targetJsonObj with candidate
                if method == 'birthYear':
                    filterResult = filterSameAttrStrValue(targetObj,candidateObj,"birthYear")
                elif method == 'birthYearDeathYear':
                    filterResult = filterSameAttrStrValue(targetObj,candidateObj,"birthYear")
                    if filterResult: filterResult = filterSameAttrStrValue(targetObj,candidateObj,"deathYear")
                else:
                    filterResult = True

                if filterResult:
                    # add found matched candidate to temporary candidate set
                    tempMatchesCluster["candidates"].append(deepcopy(candidateObj))

            # overwrite candidates with found matched candidates
            finalCluster["candidates"] = tempMatchesCluster["candidates"]

            # TODO DEBUG: remove
            #if len(finalCluster["candidates"]) > 1: # show me cluster with more than one candidate
                #print str(targetObj["uri"]) + " has more than 1 result after birthYear method."

        # apply the Hybrid-Jaccard method
        elif method in ["hybridJaccard"]:

            tempMatchesCluster["uri"] = targetObj
            tempMatchesCluster["candidates"] = []

            if isinstance(targetObj, dict):

                candidateURIs = hybridJaccard(finalCluster, threshold, "name" )

                if candidateURIs is not None and len(candidateURIs) > 0:

                    # find the found matched candidates in the JSON candidateObj
                    for candidateURI in candidateURIs:
                        for candidateObj in clusterJSON["candidates"]:
                            if candidateURI == candidateObj["uri"]:
                                # add found matched candidate to temporary candidate set
                                tempMatchesCluster["candidates"].append(deepcopy(candidateObj))

                # overwrite candidates with found matched candidates
                finalCluster["candidates"] = tempMatchesCluster["candidates"]

            # if the targetJson does not have any data, return all candidates
            else:
                finalCluster["candidates"] = finalCluster["candidates"]


    if len(finalCluster["candidates"]) > 0:
        filteredClusterLines.append(deepcopy(json.dumps(finalCluster)))
        for foundLink in finalCluster["candidates"]:
            # TODO DEBUG: remove
            if printLinksLog: printToLogs(addToEntityCluster_logs,targetObj,foundLink, "name")
            if genSameAs: generateSameAs(sameAsFile,targetObj,foundLink)

    if verbose: print str(datetime.now()) + ": filterLshResults: the cluster after the filter\n " + prettyPrintJson(tempMatchesCluster)

if printLinksLog:addToEntityCluster_logs.close()

writeJsonLine2File(filteredClusterLines, outputFile)

timeDelta =  datetime.now() - timeStart
print "Python Linking done. Duration: " + str(timeDelta)