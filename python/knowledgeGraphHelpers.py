__author__ = 'Gleb'
import json
import sys
from copy import deepcopy
from datetime import datetime

reload(sys)
sys.setdefaultencoding('utf8')
verbose = False

# This method takes a property name, a property value and a provenance object and creates
# a feature role object according to the knowledge graph data model. It returns the feature role object.
# value: the property value.
# featureName: the property name.
# provJSON: the provenance object.
def createRoleObjectFromValue(value, featureName, provJSON):
    role = {}
    if isinstance(value, basestring) or isinstance(value, dict):
        if verbose: print str(datetime.now()) + ": Creating new Role object for " + featureName
        role["a"] = "Role"
        role[featureName] = value
        role["wasGeneratedBy"] = provJSON
        return role
    else:
        if verbose: print str(datetime.now()) + ": createRoleObjectFromValue could not process input value."

# This method takes a property name, a object holding a property value and a provenance object and creates
# a feature role object according to the knowledge graph data model. It returns the feature role object.
# object: the object holding the property value.
# featureName: the property name.
# provJSON: the provenance object.
def createRoleObjectFromObject(object, featureName, provJSON):
    role = {}
    # The object holds only one value as a string.
    if isinstance(object[featureName], basestring) or isinstance(object[featureName], dict):
        if verbose: print str(datetime.now()) + ": Creating new Role object for " + featureName
        role["a"] = "Role"
        role[featureName] = object[featureName]
        role["wasGeneratedBy"] = provJSON
        return role
    # The object holds a list of string values.
    elif isinstance(object[featureName], list):
        if verbose: print str(datetime.now()) + ": Creating new Role object for " + featureName
        role = {}
        tempRoleList = []
        for value_i in object[featureName]:
            role["a"] = "Role"
            role[featureName] = value_i
            role["wasGeneratedBy"] = provJSON
            tempRoleList.append(deepcopy(role))
        return tempRoleList
    else:
        if verbose: print str(datetime.now()) + ": createRoleObjectFromObject could not process input value."


# This method prints a human readable set of JSON object lines.
# jsonLineList: the set of JSON object lines  to be printed.
def prettyPrintListOfJsonLines(jsonLineList):
    output = "["
    for idx, line in enumerate(jsonLineList):
        if idx == 0:
            output = output + "\n" + prettyPrintJson(json.loads(line))
        else:
            output = output + ",\n" + prettyPrintJson(json.loads(line))
    output = output + "\n]"
    return output

# This method prints a human readable JSON object.
# jsonLineList: the JSON object to be printed.
def prettyPrintJson(jsonData):
    return json.dumps(jsonData, indent=4, sort_keys=True)

# This method add provenance data to a feature role object according to the knowledge graph data model.
# role: the feature role object.
# provAttrKey: the property name holding the provenance
# provJSON: a JSON object holding the provenance.
def addProvToRole(role, provAttrKey, provJSON):
    attrObject = role[provAttrKey]
    # the feature role object holds only one provenance object
    if isinstance(attrObject, dict):
        tempProvList = []
        tempProvList.append(deepcopy(role[provAttrKey]))
        tempProvList.append(deepcopy(provJSON))
        role[provAttrKey] = tempProvList
    # the feature role object holds only a set of provenance objects
    elif isinstance(attrObject, list):
        if not provJSON in role[provAttrKey]:
            role[provAttrKey].append(deepcopy(provJSON))

    else:
        if verbose: print provAttrKey + "-object of role could not be processed."

# This method attaches a feature role object to an entity for a specific existing property.
# It returns the entity with the newly attached feature role object.
# entity: the entity object.
# newRoleObj: the feature role object to be attached.
# attrKey: the property name for the feature role object.
def addRoleObjToExistEntityAttr(entity, newRoleObj, attrKey):
    if verbose: print str(datetime.now()) + " addRoleObjToExistEntityAttr: Key = " + attrKey
    # the entity holds already one feature role object for the property.
    if isinstance(entity[attrKey], dict):
        if not entity[attrKey] == newRoleObj: # do not add duplicate dict
            tempRoleList = []
            tempRoleList.append(deepcopy(entity[attrKey]))
            tempRoleList.append(deepcopy(newRoleObj))
            entity[attrKey] = tempRoleList
    # the entity holds already a set of feature role objects for the property.
    elif isinstance(entity[attrKey], list):
        if not newRoleObj in entity[attrKey]: # do not add duplicate dict
            entity[attrKey].append(deepcopy(newRoleObj))

# This method attaches a feature role object to an entity for a specific property.
# The entity does not yet include the property.
# It returns the entity with the newly attached feature role object.
# entity: the entity object.
# newRoleObj: the feature role object to be attached.
# newAttrKey: the property name for the feature role object.
def addRoleObjToNewEntityAttr(entity, newRoleObj, newAttrKey):
    if verbose: print str(datetime.now()) + " addRoleObjToNewEntityAttr: Key = " + newAttrKey
    entity[newAttrKey] = newRoleObj

# This method writes data to a JSON file.
# data: the data object to be written down.
# filename: the file name of the output file.
def prettyWriteJson2File(data, filename):
    try:
        jsondata = json.dumps(data, indent=4, skipkeys=True, sort_keys=True)
        fd = open(filename, 'w')
        fd.write(jsondata)
        fd.close()
    except:
        if verbose: print 'ERROR writing', filename
        pass

# This method writes a set of JSON object lines to a file.
# jsonLines: the set of JSON object lines.
# filename: the file name of the output file.
def writeJsonLine2File(jsonLines, filename):
    fd = open(filename, 'w')
    for jsonObj in jsonLines:
        try:
            #temp = json.dumps(jsonObj)
            fd.write(jsonObj + "\n")

        except:
            if verbose: print 'ERROR writing', filename
            pass
    fd.close()

# TODO check usage
def byteify(input):
    if isinstance(input, dict):
        return {byteify(key):byteify(value) for key,value in input.iteritems()}
    elif isinstance(input, list):
        return [byteify(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input

# This method creates a feature role object for the sameAs property.
# entityObj: the entity object for which the sameAs feature is created.
# valueObj: the entity object whose URI is referenced by the new sameAs feature role object.
# uriAttrKey: the property name used for the URI value.
# provAttrKey: the property name for the provenance feature role objects.
# provJSON: the provenance object.
def createSameAsRoleObject(entityObj, valueObj, uriAttrKey, provAttrKey, provJSON):
        sameAsObj = {}
        sameAsObj["a"] = "Role"
        sameAsObj["sameAs"] = valueObj[uriAttrKey]
        sameAsObj[provAttrKey] = provJSON
        if entityObj.has_key('sameAs'):
            addRoleObjToExistEntityAttr(entityObj, sameAsObj, "sameAs")
        else:
            entityObj["sameAs"] = sameAsObj

# This method receives adds entities from a data source to a knowledge graph. Only entities which are not yet consolidated are added.
# baseResourceLines: a set of JSON object line containing the data source entities.
# knowledgeGraphJsonLines: a set of JSON object lines containing the knowledge graph entities.
# urisAlreadyInKnowledgeGraph: a list of URIs of data source entities which are already consolidated.
# provJSON: the provenance object.
def createNewClustersFromBaseFile(baseResourceLines, knowledgeGraphJsonLines, urisAlreadyInKnowledgeGraph,
                                  provJSON):
    # do not add these properties
    attrBlackList = ["@context","uri",]
    if verbose: print str(datetime.now()) + ": createNewClusters: starting to generate data."

    # the offset used to generate new knowledge graph URIs
    kg_entityURI_idx = int(len(knowledgeGraphJsonLines))

    counter = 0
    # iterate over each entity in the data source
    for baseResourceLine in baseResourceLines:

        counter = counter + 1
        print "Data sources Entity " + str(counter) + " of " + str(len(baseResourceLines)-1) + " is currently processed."

        # create an empty new entity
        newClusterJsonData = {}
        if baseResourceLine.strip() != "":

            # retrieve entity data from data source
            baseResourceJSON = json.loads(baseResourceLine.strip())
            resourceURI = baseResourceJSON["uri"]
            newClusterJsonData["@context"] = "http://isi.edu/context/..."
            newClusterJsonData["a"] = "Person"
            newClusterJsonData["uri"] = "http://isi.edu/knowledgGraph/entity/" + str(kg_entityURI_idx)

            # add new entity only if the entity is not yet consolidated
            if not resourceURI in urisAlreadyInKnowledgeGraph:

                if verbose: print str(datetime.now()) + ": Iterating over object: \n" + prettyPrintJson(baseResourceJSON)

                # iterate over all properties of the new entity
                for dataSourceEntity_AttrKey, dataSourceEntity_AttrValue in baseResourceJSON.iteritems():
                    # do not add blocked properties
                    if not dataSourceEntity_AttrKey in attrBlackList:
                        # create feature role object for current property
                        tempRoleObj = createRoleObjectFromObject(baseResourceJSON, dataSourceEntity_AttrKey, provJSON)
                        # add new feature role object to new entity
                        newClusterJsonData[dataSourceEntity_AttrKey] = tempRoleObj

                # add a sameAs feature role object
                createSameAsRoleObject(newClusterJsonData, baseResourceJSON, "uri", "wasGeneratedBy", provJSON)

                if verbose: print str(datetime.now()) + ": Created new entity:\n" + prettyPrintJson(newClusterJsonData)

                # add new entity to knowledge graph
                knowledgeGraphJsonLines.append(deepcopy(json.dumps(newClusterJsonData)))

                kg_entityURI_idx = kg_entityURI_idx + 1

    if verbose: print str(datetime.now()) + ": Knowledge graph after insertion of new entities:\n" + prettyPrintJson(knowledgeGraphJsonLines)