__author__ = 'glebgawriljuk'
import sys
import json
from copy import deepcopy
from datetime import datetime
from filters import applyJaroWinklerMaxFilterOnCluster
from knowledgeGraphHelpers import createRoleObjectFromValue, addRoleObjToNewEntityAttr, addProvToRole, prettyPrintJson, addRoleObjToExistEntityAttr, writeJsonLine2File, prettyWriteJson2File,createSameAsRoleObject, createNewClustersFromBaseFile, prettyPrintListOfJsonLines

reload(sys)
sys.setdefaultencoding('utf8')

# merges candidateJson inot targetJson and appends it to newKnowledgeGraphLines
def addToEntityCluster(targetJson, candidateJson, inputKnowledgeGraphLines):

    attrNotToBeMerged = ["@context", "uri","score"]

    printLog = False
    addToEntityCluster_logs = None

    startTime = datetime.now()

    if printLog:
        addToEntityCluster_logs = open("../../logs/addToEntityCluster_logs_" + str(startTime) +".txt", 'w')
        addToEntityCluster_logs.write("Candidate " + candidateJson["uri"] + " is merged to cluster " + targetJson["uri"] + "\nThe cluster before the merge:\n")
        addToEntityCluster_logs.write(prettyPrintJson(targetJson) + "\n")

    for candidate_AttrKey, candidate_AttrValue in candidateJson.iteritems(): # for each candidate attribute, check it attribute exists in entity

        if verbose: print str(datetime.now()) + ": Checking candidate attribute " + candidate_AttrKey
        candidate_AttrKey_Found = False

        for target_AttrKey, target_attrRole in targetJson.iteritems(): #iterate over all attribute Roles

            # same attribute key was found, hence must be merged
            if candidate_AttrKey == target_AttrKey and not candidate_AttrKey in attrNotToBeMerged:

                candidate_AttrKey_Found = True

                if verbose: print str(datetime.now()) + ": Attribute " + candidate_AttrKey + " exists in entity."

                targetListOfDict = []
                candidateListOfDict = []
                #  target attribute are either dict or list of dicts
                if isinstance(target_attrRole, dict): # entity has only one attribute Role
                    targetListOfDict.append(target_attrRole)
                elif isinstance(target_attrRole, list):
                    targetListOfDict = target_attrRole
                else:
                    print str(datetime.now()) + ": Unknown format for target role " + str(target_attrRole)

                if isinstance(candidate_AttrValue, basestring) or isinstance(candidate_AttrValue, int) or isinstance(candidate_AttrValue, float) or isinstance(candidate_AttrValue, dict):
                    candidateListOfDict.append(candidate_AttrValue)
                elif isinstance(candidate_AttrValue, list):
                    candidateListOfDict = candidate_AttrValue
                else:
                    print str(datetime.now()) + ": Unknown format for candidate value " + str(candidate_AttrValue)

                mergedCandidateValues = []

                for candidate_value_i in candidateListOfDict:
                    candidate_value_i_Found = False

                    if isinstance(candidate_value_i, basestring):
                        candidate_value_i.strip()

                    for target_attrRole_idx, target_attrRole_i in enumerate(targetListOfDict):

                        if isinstance(target_attrRole_i[target_AttrKey], basestring):
                            target_attrRole_i[target_AttrKey].strip()

                        if not candidate_value_i in mergedCandidateValues:

                            if target_attrRole_i[target_AttrKey] == candidate_value_i:
                                candidate_value_i_Found = True

                                mergedCandidateValues.append(candidate_value_i)

                                addProvToRole(target_attrRole_i, "wasGeneratedBy", provJSON)
                                targetListOfDict[target_attrRole_idx] = target_attrRole_i

                                break


                    if not candidate_value_i_Found: # the current candidate_value_i does not exist in the knowledge graph, add new role object for this value

                        if verbose: print str(datetime.now()) + ": 6---Value [" +  candidate_value_i + "] does not exists. Add new Role object candidate_AttrKey." + "\n" + prettyPrintJson(targetJson) #OK

                        tempRoleObj = createRoleObjectFromValue(candidate_value_i, candidate_AttrKey, provJSON)
                        #addRoleObjToExistEntityAttr(targetJson, tempRoleObj, candidate_AttrKey)
                        targetListOfDict.append(deepcopy(tempRoleObj))

                        if verbose: print str(datetime.now()) + ": New Role added to list of Roles for key: " + candidate_AttrKey + "\n" + prettyPrintJson(targetJson)

                targetJson[target_AttrKey] = targetListOfDict


        # all target attribute tested
        if not candidate_AttrKey_Found and not candidate_AttrKey in attrNotToBeMerged: # current candidate attribute is not in the cluster, add it to the cluster

            candidateValueList = []
            if not isinstance(candidate_AttrValue, list):
                candidateValueList.append(candidate_AttrValue)

            elif isinstance(candidate_AttrValue, list):
                candidateValueList = candidate_AttrValue
            else:
                print str(datetime.now()) + ": Unknown format for candidate value " + str(candidate_AttrValue)

            for candidate_value_idx, candidate_value_i in enumerate(candidateValueList):

                if verbose: print str(datetime.now()) + ": 7.2---Key  [" +  candidate_AttrKey + "] does not exists and is a LIST. Add new Role object\n" + prettyPrintJson(targetJson) #OK
                tempRoleObj = createRoleObjectFromValue(candidate_value_i, candidate_AttrKey, provJSON)

                if candidate_value_idx == 0:
                    addRoleObjToNewEntityAttr(targetJson, tempRoleObj, candidate_AttrKey)

                else:
                    addRoleObjToExistEntityAttr(targetJson, tempRoleObj, candidate_AttrKey)

                if verbose: print str(datetime.now()) + ": Added new Role obj for key " + candidate_AttrKey + "\n" + prettyPrintJson(targetJson)


            if verbose: print str(datetime.now()) + ": SameAs relation added: \n" + prettyPrintJson(targetJson)

            createSameAsRoleObject(targetJson, candidateJson, "uri", "wasGeneratedBy", provJSON)

            # if verbose: print str(datetime.now()) + ": SameAs relation added: \n" + prettyPrintJson(entityJSON)
            if verbose:print str(datetime.now()) + ": Candidate merge into " + targetJson["uri"] + " completed:\n" + prettyPrintJson(targetJson)

            if printLog:
                addToEntityCluster_logs.write("The cluster after the merge:\n")
                addToEntityCluster_logs.write(prettyPrintJson(targetJson) + "\n")


    if verbose: print str(datetime.now()) + ": merged cluster:\n" + prettyPrintJson(targetJson)
    inputKnowledgeGraphLines.append(deepcopy(json.dumps(targetJson)))


    endTime = datetime.now()
    timeDelta = endTime - startTime
    if verbose: print str(datetime.now()) + ": Total duration: " + str(timeDelta)

    if printLog:
        addToEntityCluster_logs.write("Total duration = " + str(timeDelta))
        addToEntityCluster_logs.close()

def createMergedEntity(targetJson, candidateJson):

    attrNotToBeMerged = ["@context", "uri","score"]

    printLog = False
    addToEntityCluster_logs = None

    startTime = datetime.now()

    if printLog:
        addToEntityCluster_logs = open("../../logs/addToEntityCluster_logs_" + str(startTime) +".txt", 'w')
        addToEntityCluster_logs.write("Candidate " + candidateJson["uri"] + " is merged to cluster " + targetJson["uri"] + "\nThe cluster before the merge:\n")
        addToEntityCluster_logs.write(prettyPrintJson(targetJson) + "\n")

    for candidate_AttrKey, candidate_AttrValue in candidateJson.iteritems(): # for each candidate attribute, check it attribute exists in entity

        if verbose: print str(datetime.now()) + ": Checking candidate attribute " + candidate_AttrKey
        candidate_AttrKey_Found = False

        for target_AttrKey, target_attrRole in targetJson.iteritems(): #iterate over all attribute Roles

            # same attribute key was found, hence must be merged
            if candidate_AttrKey == target_AttrKey and not candidate_AttrKey in attrNotToBeMerged:

                candidate_AttrKey_Found = True

                if verbose: print str(datetime.now()) + ": Attribute " + candidate_AttrKey + " exists in entity."

                targetListOfDict = []
                candidateListOfDict = []
                #  target attribute are either dict or list of dicts
                if isinstance(target_attrRole, dict): # entity has only one attribute Role
                    targetListOfDict.append(target_attrRole)
                elif isinstance(target_attrRole, list):
                    targetListOfDict = target_attrRole
                else:
                    print str(datetime.now()) + ": Unknown format for target role " + str(target_attrRole)

                if isinstance(candidate_AttrValue, basestring) or isinstance(candidate_AttrValue, int) or isinstance(candidate_AttrValue, float) or isinstance(candidate_AttrValue, dict):
                    candidateListOfDict.append(candidate_AttrValue)
                elif isinstance(candidate_AttrValue, list):
                    candidateListOfDict = candidate_AttrValue
                else:
                    print str(datetime.now()) + ": Unknown format for candidate value " + str(candidate_AttrValue)

                mergedCandidateValues = []

                for candidate_value_i in candidateListOfDict:
                    candidate_value_i_Found = False

                    if isinstance(candidate_value_i, basestring):
                        candidate_value_i.strip()

                    for target_attrRole_idx, target_attrRole_i in enumerate(targetListOfDict):

                        if isinstance(target_attrRole_i[target_AttrKey], basestring):
                            target_attrRole_i[target_AttrKey].strip()

                        if not candidate_value_i in mergedCandidateValues:

                            if target_attrRole_i[target_AttrKey] == candidate_value_i:
                                candidate_value_i_Found = True

                                mergedCandidateValues.append(candidate_value_i)

                                addProvToRole(target_attrRole_i, "wasGeneratedBy", provJSON)
                                targetListOfDict[target_attrRole_idx] = target_attrRole_i

                                break


                    if not candidate_value_i_Found: # the current candidate_value_i does not exist in the knowledge graph, add new role object for this value

                        if verbose: print str(datetime.now()) + ": 6---Value [" +  candidate_value_i + "] does not exists. Add new Role object candidate_AttrKey." + "\n" + prettyPrintJson(targetJson) #OK

                        tempRoleObj = createRoleObjectFromValue(candidate_value_i, candidate_AttrKey, provJSON)
                        #addRoleObjToExistEntityAttr(targetJson, tempRoleObj, candidate_AttrKey)
                        targetListOfDict.append(deepcopy(tempRoleObj))

                        if verbose: print str(datetime.now()) + ": New Role added to list of Roles for key: " + candidate_AttrKey + "\n" + prettyPrintJson(targetJson)

                targetJson[target_AttrKey] = targetListOfDict


        # all target attribute tested
        if not candidate_AttrKey_Found and not candidate_AttrKey in attrNotToBeMerged: # current candidate attribute is not in the cluster, add it to the cluster

            candidateValueList = []
            if not isinstance(candidate_AttrValue, list):
                candidateValueList.append(candidate_AttrValue)

            elif isinstance(candidate_AttrValue, list):
                candidateValueList = candidate_AttrValue
            else:
                print str(datetime.now()) + ": Unknown format for candidate value " + str(candidate_AttrValue)

            for candidate_value_idx, candidate_value_i in enumerate(candidateValueList):

                if verbose: print str(datetime.now()) + ": 7.2---Key  [" +  candidate_AttrKey + "] does not exists and is a LIST. Add new Role object\n" + prettyPrintJson(targetJson) #OK
                tempRoleObj = createRoleObjectFromValue(candidate_value_i, candidate_AttrKey, provJSON)

                if candidate_value_idx == 0:
                    addRoleObjToNewEntityAttr(targetJson, tempRoleObj, candidate_AttrKey)

                else:
                    addRoleObjToExistEntityAttr(targetJson, tempRoleObj, candidate_AttrKey)

                if verbose: print str(datetime.now()) + ": Added new Role obj for key " + candidate_AttrKey + "\n" + prettyPrintJson(targetJson)


    createSameAsRoleObject(targetJson, candidateJson, "uri", "wasGeneratedBy", provJSON)

    # if verbose: print str(datetime.now()) + ": SameAs relation added: \n" + prettyPrintJson(entityJSON)
    if verbose:print str(datetime.now()) + ": Candidate merge into " + targetJson["uri"] + " completed:\n" + prettyPrintJson(targetJson)

    if printLog:
        addToEntityCluster_logs.write("The cluster after the merge:\n")
        addToEntityCluster_logs.write(prettyPrintJson(targetJson) + "\n")


    if verbose: print str(datetime.now()) + ": merged cluster:\n" + prettyPrintJson(targetJson)

    endTime = datetime.now()
    timeDelta = endTime - startTime
    if verbose: print str(datetime.now()) + ": Total duration: " + str(timeDelta)

    if printLog:
        addToEntityCluster_logs.write("Total duration = " + str(timeDelta))
        addToEntityCluster_logs.close()
    return targetJson

def parse_args():
    global inputLshClusters
    global inputKnowledgeGraph
    global inputProvFilename
    global inputBaseData
    global outputFile
    for arg_idx, arg in enumerate(sys.argv):
        if arg == "--inputLshClusters":
            inputLshClusters = sys.argv[arg_idx+1]
            continue
        if arg == "--inputKnowledgGraph":
            inputKnowledgeGraph = sys.argv[arg_idx+1]
            continue
        if arg == "--inputProvFile":
            inputProvFilename = sys.argv[arg_idx+1]
            continue
        if arg == "--inputBaseData":
            inputBaseData = sys.argv[arg_idx+1]
            continue
        if arg == "--outputFile":
            outputFile = sys.argv[arg_idx+1]
            continue

def die():
    if verbose: print str(datetime.now()) + ": Please input the required parameters"
    if verbose: print str(datetime.now()) + ": Usage: createNewClusters.py --input <input filename> filename> --output <output filename>"
    exit(1)


inputLshClusters = None
inputKnowledgeGraph = None
inputProvFilename = None
inputBaseData = None
verbose = False
outputFile = None
knowledgeGraphLines = None
newKnowledgeGraphLines = []

mergedCandidateURIs = []
extendedClusterURIs = []
parse_args()


inputBaseDataFile = open(inputBaseData)
baseDataLines = inputBaseDataFile.readlines()

provFile = open(inputProvFilename)
provJSON = json.load(provFile)


if inputKnowledgeGraph and inputLshClusters: # Do merging only if a knowledge graph AND Links are present is present

    timeStart = datetime.now()
    inputKnowledgeGraphFile = open(inputKnowledgeGraph)
    knowledgeGraphLines = inputKnowledgeGraphFile.readlines()
    if verbose: print str(datetime.now()) + ": Initial KG:\n" + prettyPrintListOfJsonLines(knowledgeGraphLines)
    timeDelta =  datetime.now() - timeStart
    if True: print str(datetime.now()) + ": Loading knowledge graph duration: " + str(timeDelta)


    timeStart = datetime.now()
    inputLshClustersFile = open(inputLshClusters)
    lshClustersJsonLines = inputLshClustersFile.readlines()

    timeDelta =  datetime.now() - timeStart
    if True: print str(datetime.now()) + ": Loading Links duration: " + str(timeDelta)


    #print str(datetime.now()) + ": KG lines:\n" +  str(knowledgeGraphLines)
    if True: print str(datetime.now()) + ": Merging candidates initiated."
    if verbose: print str(datetime.now()) + ": Initial knowledge graph:\n" + prettyPrintListOfJsonLines(knowledgeGraphLines)
    if True: print str(datetime.now()) + ": Number of clusters to merge: " + str(len(lshClustersJsonLines))
    counter = 1
    timeStart = datetime.now()

    for lshCluster in lshClustersJsonLines:
        clusterJSON = json.loads(lshCluster.strip())
        candidatesJson = clusterJSON["candidates"]
        targetJson = clusterJSON["uri"]
        extendedClusterURIs.append(targetJson["uri"])
        mergedCluster = targetJson

        if verbose: print str(datetime.now()) + ": Cluster " + str(counter) + " out of " + str(len(lshClustersJsonLines)) + " is merged..."
        counter = counter + 1
        if verbose: print str(datetime.now()) + ": Cluster to be processed: \n" + lshCluster

        if candidatesJson and isinstance(candidatesJson, list): # if filterResult is not empty
            #print "list of candidates"
            for candidateJson in candidatesJson:
                mergedCandidateURIs.append(candidateJson["uri"])
                #addToEntityCluster(targetJson,candidateJson,newKnowledgeGraphLines)
                # TODO attrubutes like "a" will get same prov multiple times...
                mergedCluster = createMergedEntity(mergedCluster,candidateJson)
                if verbose: print str(datetime.now()) + ": Integrating Candidate finished. Intermediate knowledge graph:\n" + prettyPrintListOfJsonLines(newKnowledgeGraphLines)

            newKnowledgeGraphLines.append(deepcopy(json.dumps(mergedCluster)))

        elif candidatesJson and isinstance(candidatesJson, dict):
            #print "one dict candidate"
            mergedCandidateURIs.append(candidatesJson["uri"])
            #addToEntityCluster(targetJson,candidatesJson,newKnowledgeGraphLines)

            mergedCluster = createMergedEntity(targetJson,candidatesJson)
            newKnowledgeGraphLines.append(deepcopy(json.dumps(mergedCluster)))

            if verbose : print str(datetime.now()) + ": Integrating Candidate finished. Intermediate knowledge graph:\n" + prettyPrintListOfJsonLines(newKnowledgeGraphLines)
        else:
            print "Candidate not list of dict: \n" + str(candidatesJson)
        if verbose: print str(datetime.now()) + ": Current number of merged clusters: " + str(len(newKnowledgeGraphLines))

    timeDelta =  datetime.now() - timeStart

    if True: print str(datetime.now()) + ": Merging candidates completed. Duration: " + str(timeDelta)

    # remove info data object
    knowledgeGraphLines.pop()
    numberOfClustersNotMerged = len(knowledgeGraphLines) - len(extendedClusterURIs)
    addUnmergedClusterCounter = 0
    timeStart = datetime.now()
    if True: print str(datetime.now()) + ": Number of cluster in total: " + str(len(knowledgeGraphLines))
    if True: print str(datetime.now()) + ": Number of merged clusters: " + str(len(newKnowledgeGraphLines))
    if True: print str(datetime.now()) + ": Number of merged cluster URIs: " + str(len(extendedClusterURIs))
    if True: print str(datetime.now()) + ": Adding unmerged clusters initiated. Number of cluster not merged: " + str(numberOfClustersNotMerged)
    for oldCluster in knowledgeGraphLines:
        oldClusterJson = json.loads(oldCluster)
        if not oldClusterJson["uri"] in extendedClusterURIs:
            addUnmergedClusterCounter = addUnmergedClusterCounter + 1
            if verbose: print str(datetime.now()) + ": " + str(addUnmergedClusterCounter) + " of " + str(numberOfClustersNotMerged) + " unmerged clusters added. "
            newKnowledgeGraphLines.append(deepcopy(json.dumps(oldClusterJson)))
    timeDelta =  datetime.now() - timeStart
    if True: print str(datetime.now()) + ": Adding unmerged clusters completed. Duration: " + str(timeDelta)
    if True: print str(datetime.now()) + ": Number of unmerged clusters added: " + str(addUnmergedClusterCounter)

else:
    knowledgeGraphLines = []
    numOfEntitiesObj = {}
    numOfEntitiesObj["numberOfEntities"] = 0
    knowledgeGraphLines.append(deepcopy(json.dumps(numOfEntitiesObj)))

numOfKgEntities = int(len(newKnowledgeGraphLines))
if True: print str(datetime.now()) + ": Creating new clusters. Current number of entities: " + str(numOfKgEntities)

timeStart = datetime.now()
createNewClustersFromBaseFile(baseDataLines, newKnowledgeGraphLines, mergedCandidateURIs, provJSON)
numOfKgEntities = int(len(newKnowledgeGraphLines))
timeDelta =  datetime.now() - timeStart
if True: print str(datetime.now()) + ": Creating new clusters completed. Current number of entities: " + str(numOfKgEntities) + " Duration: " + str(timeDelta)

if verbose: print str(datetime.now()) + ": Integrating Links finished. Final knowledge graph:\n" + prettyPrintListOfJsonLines(knowledgeGraphLines)

timeStart = datetime.now()
numOfEntitiesObj = {}
numOfEntitiesObj["numberOfEntities"] = numOfKgEntities
newKnowledgeGraphLines.append(deepcopy(json.dumps(numOfEntitiesObj)))
if verbose: print str(datetime.now()) + ": Writing KG:\n" + prettyPrintListOfJsonLines(newKnowledgeGraphLines)
writeJsonLine2File(newKnowledgeGraphLines, outputFile)
timeDelta =  datetime.now() - timeStart
if True: print str(datetime.now()) + ": Writing JSON file completed. Duration: " + str(timeDelta)