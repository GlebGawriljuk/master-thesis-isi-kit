__author__ = 'Gleb'

import sys
import json

reload(sys)
sys.setdefaultencoding('utf8')

def getRecall(trueP, groundTruthSize):
    return trueP / float(groundTruthSize)

def printResults(groundTruthSize,falseExistingCluster, falseClusters, falsePos, recall,outputFilePath):
    outputFile = open(outputFilePath, 'w')
    outputFile.write("GroundTruth size: \t" + str(groundTruthSize) + '\n')
    outputFile.write("Number of wrong sameAs add to existing clusters: \t" + str(falseExistingCluster) + '\n')
    outputFile.write("Number of wrong sameAs add to new clusters: \t" + str(falseClusters) + '\n')
    outputFile.write("Number of wrong sameAs: \t" + str(falsePos) + '\n')
    outputFile.write("Recall: \t" + str(recall) + '\n')

def parse_args():
    global KgFilepath
    global groundTruthFilepath
    global outputFilePath
    for arg_idx, arg in enumerate(sys.argv):
        if arg == "--KgFilepath":
            KgFilepath = sys.argv[arg_idx+1]
            continue
        if arg == "--groundTruth":
            groundTruthFilepath = sys.argv[arg_idx+1]
            continue
        if arg == "--output":
            outputFilePath = sys.argv[arg_idx+1]
            continue

KgFilepath = None
groundTruthFilepath = None
outputFilePath = None

parse_args()

KgFile = open(KgFilepath)
KgLines = KgFile.readlines()

groundTruthFile = open(groundTruthFilepath)
groundTruthLines = groundTruthFile.readlines()
groundTruthSize = len(groundTruthLines)

truePos = 0
falsePos = 0
falseNewClusters = 0
falseExistingCluster = 0
counter = 0
for groundTruthLine in groundTruthLines:
    counter = counter + 1
    print "Ground truth link " + str(counter) + " of " + str(groundTruthSize) + " is processed."
    groundTruthLineList = groundTruthLine.split('\t')
    gtTargetURI = groundTruthLineList[0].strip()
    gtLinkURI = groundTruthLineList[1].replace('\n','').strip()

    for KgkLine in KgLines:
        #print str(KgkLine.strip())
        kgCluster = json.loads(KgkLine.strip())
        if "uri" in kgCluster: # do not evaluate last json doc
            clusterURI = kgCluster["uri"]
            clusterSameAs = kgCluster["sameAs"]
            if isinstance(clusterSameAs, dict):
                if gtTargetURI == clusterURI and gtLinkURI == clusterSameAs["sameAs"]:
                    truePos = truePos + 1
                elif gtTargetURI != clusterURI and gtLinkURI == clusterSameAs["sameAs"]:
                    falsePos = falsePos + 1
                    falseNewClusters = falseNewClusters + 1 # as clusterSameAs is a dict, this means it is a newly created cluster
            elif isinstance(clusterSameAs, list):
                for sameAs in clusterSameAs:
                    if gtTargetURI == clusterURI and gtLinkURI == sameAs["sameAs"]:
                        truePos = truePos + 1
                    elif gtTargetURI != clusterURI and gtLinkURI == sameAs["sameAs"]:
                        falsePos = falsePos + 1
                        falseExistingCluster = falseExistingCluster + 1

printResults(groundTruthSize,falseExistingCluster, falseNewClusters, falsePos,  getRecall(truePos, groundTruthSize),outputFilePath)