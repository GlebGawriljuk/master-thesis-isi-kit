#!/bin/sh

temp=`ls ../output_tfidf/ | grep "json"`
for fname in $temp 
do
	echo $fname
	python HybridJaccard.py $fname 
done
