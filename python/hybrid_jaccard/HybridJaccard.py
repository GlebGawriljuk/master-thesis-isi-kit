import json
import munkres
import jaro
import re
import sys
from knowledgeGraphHelpers import prettyPrintJson

class SmithWaterman:
	def measure(self, strG, strR):
		if(len(strG) < 1 or len(strR) < 1):
			return 0.0

		row = len(strR)
		col = len(strG)
		strR = "^"+strR
		strG = "^"+strG

		matrix = []
		path = []
		for i in range(row+1):
			matrix.append([0]*(col+1))
			path.append(["N"]*(col+1))
	# print_matrix(matrix)
		indelValue = -1
		matchValue = 2
		for i in range(1,row+1):
			for j in range(1,col+1):
				# penalty map
				from_left = matrix[i][j-1] + indelValue
				from_top = matrix[i-1][j] + indelValue
				if strR[i] == strG[j]:
					from_diag = matrix[i-1][j-1] + matchValue
				else:
					from_diag = matrix[i-1][j-1] + indelValue

				matrix[i][j] = max(from_left, from_top, from_diag)
				if matrix[i][j] < 0:
					matrix[i][j] = 0
		max_sim = 0
		for i in range(1, row+1):
			if(max_sim < matrix[i][col]):
				max_sim = matrix[i][col]
		#print(len(strG))
		return float(max_sim)/float(len(strG) - 1)/2.0
class Levenshtein:
    def measure(self, seq1, seq2):
            oneago = None
            thisrow = range(1, len(seq2) + 1) + [0]
            for x in range(len(seq1)):
                twoago, oneago, thisrow = oneago, thisrow, [0] * len(seq2) + [x + 1]
                for y in range(len(seq2)):
                    delcost = oneago[y] + 1
                    addcost = thisrow[y - 1] + 1
                    subcost = oneago[y - 1] + (seq1[x] != seq2[y])
                    thisrow[y] = min(delcost, addcost, subcost)
            max_len = max({len(seq1),len(seq2)})
            min_len = min({len(seq1),len(seq2)})
            return float(max_len - thisrow[len(seq2) - 1])/float(min_len)

class StringMatcher:
    threshold = .5
    method = "smith"

    def __init__(self, method, thr):
        self.threshold = thr
        self.method = method
        #self.setup_config(config_path)

    def sim_metric(self, test_word, keyword):
        if self.method == "smith":
            sw = SmithWaterman()
            return sw.measure(test_word, keyword)
        elif self.method == "jaro":
            return jaro.metric_jaro_winkler(test_word, keyword)
        else:
            return self.levenshtein_sim(keyword, test_word)

    def sim_measure(self, test_words, ref_words):
        #print "-------sim_measure"
        #print "-------test_words " + str(test_words)
        #print "-------ref_word " + str(ref_words)
        outer_arr = []
        for in_word in test_words:
            inner_arr = []
            for ref_word in ref_words:
                sim = self.sim_metric(in_word, ref_word)
                #print "sim_measure: " + str(sim) + " for " + str(in_word) + " and " + str(ref_word)
                if sim < self.threshold:
                    sim = 0.0
                inner_arr.append(1.0 - sim)
            outer_arr.append(inner_arr)
            if len(outer_arr) == 0:
                return 0.0
        #print "sim_measure: outer_arr " + str(outer_arr)
        m = munkres.Munkres()
        indexes = m.compute(outer_arr)
        values = []
        for row, column in indexes:
            values.append(1.0 - outer_arr[row][column]) #go back to similarity
        #print "sim_measure returns :" + str(sum(values)/(len(test_words)+len(ref_words)-len(values)+values.count(0.0)))
        return sum(values)/(len(test_words)+len(ref_words)-len(values)+values.count(0.0))

class HybridJaccard:
    jaccardThr = 0.45
    jaccardMethod = "smith"
    inputPath = ""
    inputTokenizer = ","
    outputFilter = "count"
    numCand = 5
    scoreThr = 0.3
    
    def __init__(self, config_path = "config.json"):
        with open(config_path) as data_file:
            data = json.load(data_file)

        self.jaccardThr = float(data["method_config"]["parameters"]["threshold"])
        self.jaccardMethod = data["method_config"]["partial_method"]
        self.inputPath = data["input_config"]["path"]
        self.inputTokenizer = data["input_config"]["tokenizer"]
        self.outputFilter = data["output_config"]["filter"]
        self.numCand = int(data["output_config"]["num_candidates"])
        self.scoreThr = float(data["output_config"]["score_threshold"])
        
    def generateJson(self, key, matches, candidates_name):
        jsonObj = {"uri": str(key), candidates_name:[]}
        for match in matches:
            # print "Match:", type(match), ", ", match
            candidate = {}
            if type(match) is list or type(match) is dict or type(match) is tuple:
                candidate["uri"] = str(match[0])
                candidate["score"] = match[1]
            else:
                candidate["uri"] = str(match)
            jsonObj[candidates_name].append(candidate)
        return jsonObj

    def generateCandidates(self):
        sm = StringMatcher(self.jaccardMethod, self.jaccardThr)
        candidate_fields = []
        outputFile = open(sys.argv[2],"w")
        decoder = json.JSONDecoder()
        with open(sys.argv[1]) as inputFile:
            for jsonObj in inputFile:
                data = json.loads(jsonObj)
                queryString = data["uri"][keyName][keyName]
                queryFields = queryString.split(self.inputTokenizer)
                candidates = []
                for i in range(len(data["candidates"])):
                    if isinstance(data["candidates"][i][keyName], list):
                        print ""
                        for candValue in data["candidates"][i][keyName]:
                            candidateFields = candValue.split(self.inputTokenizer)
                            score = sm.sim_measure(candidateFields, queryFields)
                            candidates.append([data["candidates"][i]["uri"].encode('ascii', 'ignore'), score])
                            del candidateFields[:]
                    else:
                        candidateFields = data["candidates"][i][keyName].split(self.inputTokenizer)
                        score = sm.sim_measure(candidateFields, queryFields)
                        candidates.append([data["candidates"][i]["uri"].encode('ascii', 'ignore'), score])
                        del candidateFields[:]
                candidates.sort(key=lambda tup: tup[1],reverse=True)

                if self.outputFilter == "count":
                    candidates = candidates[:self.numCand]
                else:
                    candidates = [t for t in candidates if t[1] < self.scoreThr]
                #print >>outputFile, str(json.dumps(self.generateJson(data["uri"]["uri"], candidates, "matches")))
                
                del candidates[:]
                return str(candidates[0][0])
                #print str(data["query_location"]["uri"]) + "\t" + temp_id + "\t" + str(max_score)
                #print temp_name + " " + str(max_score)
                #print "--------------------------------"
                #except:

    def getTopCandidateURI(self,jsonObj, threshold, keyName):
        sm = StringMatcher(self.jaccardMethod, self.jaccardThr)
        candidate_fields = []
        #outputFile = open(sys.argv[2],"w")
        decoder = json.JSONDecoder()
        data = jsonObj
        candidates = []

        evaluationTargetURI = "http://edan.si.edu/saam/id/person-institution/101"

        if isinstance(data["uri"], dict): # if uri is not a dict, it means it is a string and therfore there is no data to compare
            #if isinstance(data["uri"][keyName], dict): # either stirng or list of strings
            if isinstance(data["uri"][keyName], basestring): # either stirng or list of strings
                #queryString = data["uri"][keyName][keyName]
                queryString = data["uri"][keyName]
                queryFields = queryString.strip().split(self.inputTokenizer)
                candidates = []
                for i in range(len(data["candidates"])):
                    #if isinstance(data["candidates"][i][keyName], list): #either dict or list of dicts
                    if keyName in data["candidates"][i] and isinstance(data["candidates"][i][keyName], list): #either dict or list of dicts
                        #print ""
                        for candValue in data["candidates"][i][keyName]:
                            candidateFields = candValue[keyName].strip().split(self.inputTokenizer)
                            score = sm.sim_measure(candidateFields, queryFields)

                            if data["uri"]["uri"] == evaluationTargetURI:
                                    print "Comparing [" + str(candidateFields) + "] with [ " + str(queryFields) + " ]. Score = " + str(score)

                            candidates.append([data["candidates"][i]["uri"].encode('ascii', 'ignore'), score])
                            del candidateFields[:]
                    #else: #either dict or list of dicts
                    elif keyName in data["candidates"][i] and isinstance(data["candidates"][i][keyName], dict): #either dict or list of dicts
                        candidateFields = data["candidates"][i][keyName][keyName].strip().split(self.inputTokenizer)
                        score = sm.sim_measure(candidateFields, queryFields)

                        if data["uri"]["uri"] == evaluationTargetURI:
                                    print "Comparing [" + str(candidateFields) + "] with [ " + str(queryFields) + " ]. Score = " + str(score)

                        candidates.append([data["candidates"][i]["uri"].encode('ascii', 'ignore'), score])
                        del candidateFields[:]

            elif keyName in data["uri"] and isinstance(data["uri"][keyName], list): # either stirng or list of strings
                candidates = []
                #print str(data["uri"][keyName])
                for i in range(len(data["uri"][keyName])):
                    if isinstance(data["uri"][keyName][i], dict):
                        queryString = data["uri"][keyName][i][keyName]
                    elif isinstance(data["uri"][keyName][i], basestring):
                        queryString = data["uri"][keyName][i]
                    else:
                        queryString = ""
                        print "Error value is not dict nor string."
                    queryFields = queryString.strip().split(self.inputTokenizer)

                    for i in range(len(data["candidates"])):
                        #if isinstance(data["candidates"][i][keyName], list): # either a dict or a list of dicts
                        if keyName in data["candidates"][i] and isinstance(data["candidates"][i][keyName], list): # either a dict or a list of dicts
                            #print ""
                            for candValue in data["candidates"][i][keyName]:

                                candidateFields = candValue[keyName].strip().split(self.inputTokenizer)

                                if data["uri"]["uri"] == evaluationTargetURI:
                                    print "Comparing [" + str(candidateFields) + "] with [ " + str(queryFields) + " ]. Score = " + str(score)

                                score = sm.sim_measure(candidateFields, queryFields)
                                candidates.append([data["candidates"][i]["uri"].encode('ascii', 'ignore'), score])
                                del candidateFields[:]
                        #else:# either a dict or a list of dicts
                        elif keyName in data["candidates"][i] and isinstance(data["candidates"][i][keyName], dict): # either a dict or a list of dicts
                            candidateFields = data["candidates"][i][keyName][keyName].strip().split(self.inputTokenizer)
                            score = sm.sim_measure(candidateFields, queryFields)

                            if data["uri"]["uri"] == evaluationTargetURI:
                                    print "Comparing [" + str(candidateFields) + "] with [ " + str(queryFields) + " ]. Score = " + str(score)

                            candidates.append([data["candidates"][i]["uri"].encode('ascii', 'ignore'), score])
                            del candidateFields[:]
            elif isinstance(data["uri"][keyName], basestring):
                queryString = data["uri"][keyName]
                queryFields = queryString.strip().split(self.inputTokenizer)
                candidates = []
                for i in range(len(data["candidates"])):
                        if keyName in data["candidates"][i] and isinstance(data["candidates"][i][keyName], list):
                            #print ""
                            for candValue in data["candidates"][i][keyName]:
                                candidateFields = candValue.strip().split(self.inputTokenizer)
                                score = sm.sim_measure(candidateFields, queryFields)
                                candidates.append([data["candidates"][i]["uri"].encode('ascii', 'ignore'), score])
                                del candidateFields[:]
                        else:
                            candidateFields = data["candidates"][i][keyName].strip().split(self.inputTokenizer)
                            score = sm.sim_measure(candidateFields, queryFields)
                            candidates.append([data["candidates"][i]["uri"].encode('ascii', 'ignore'), score])
                            del candidateFields[:]
            else:
                print "Error: candidate attribute is not dict nor list: " + str(type(data["uri"][keyName]))

        candidates.sort(key=lambda tup: tup[1],reverse=True)

        if self.outputFilter == "count":
            candidates = candidates[:self.numCand]
        else:
            #candidates = [t for t in candidates if t[1] > self.scoreThr]
            candidates = [t for t in candidates if t[1] > float(threshold)]
        #print >>outputFile, str(json.dumps(self.generateJson(data["uri"]["uri"], candidates, "matches")))

        # not test
        #del candidates[:]
        if len(candidates) > 0:
            matchesURIs = []
            topScore = float(candidates[0][1])
            for i in range(len(candidates)):
                if float(candidates[i][1]) == topScore and float(candidates[i][1]) >= float(threshold):
                    matchesURIs.append(candidates[i][0])
            if len(matchesURIs) > 0:

                #if len(matchesURIs) > 1: # show me cluster with more than one candidate
                    #print data["uri"]["uri"] + " was processed. Candidates:\n"
                    #print str(candidates)

                return set(matchesURIs)
            else:
                return None
            #if float(candidates[0][1]) > float(threshold):
            #    return str(candidates[0][0]) # return uri of top candidate
            #else:
            #    return None
        else:
            return None
                    
    def getTopCandidateURIV2(self,jsonObj, threshold, keyName):
        sm = StringMatcher(self.jaccardMethod, self.jaccardThr)
        candidate_fields = []
        #outputFile = open(sys.argv[2],"w")
        decoder = json.JSONDecoder()
        data = jsonObj
        candidates = []
        if isinstance(data["uri"], dict): # if uri is not a dict, it means it is a string and therfore there is no data to compare

            if isinstance(data["uri"][keyName], basestring): # either stirng or list of strings

                queryString = data["uri"][keyName]
                queryFields = queryString.strip().split(self.inputTokenizer)
                candidates = []
                for i in range(len(data["candidates"])):

                    if isinstance(data["candidates"][i][keyName], list): #either string or list of string

                        for candValue in data["candidates"][i][keyName]:
                            candidateFields = candValue.strip().split(self.inputTokenizer)
                            score = sm.sim_measure(candidateFields, queryFields)
                            candidates.append([data["candidates"][i]["uri"].encode('ascii', 'ignore'), score])
                            del candidateFields[:]

                    elif isinstance(data["candidates"][i][keyName], basestring): #either string or list of string
                        candidateFields = data["candidates"][i][keyName].strip().split(self.inputTokenizer)
                        score = sm.sim_measure(candidateFields, queryFields)
                        candidates.append([data["candidates"][i]["uri"].encode('ascii', 'ignore'), score])
                        del candidateFields[:]

            elif isinstance(data["uri"][keyName], list): # either stirng or list of strings
                candidates = []
                #print str(data["uri"][keyName])
                for i in range(len(data["uri"][keyName])):
                    if isinstance(data["uri"][keyName][i], dict):
                        queryString = data["uri"][keyName][i][keyName]
                    elif isinstance(data["uri"][keyName][i], basestring):
                        queryString = data["uri"][keyName][i]
                    else:
                        queryString = ""
                        print "Error value is not dict nor string."
                    queryFields = queryString.strip().split(self.inputTokenizer)

                    for i in range(len(data["candidates"])):
                        #if isinstance(data["candidates"][i][keyName], list): # either a dict or a list of dicts
                        if isinstance(data["candidates"][i][keyName], list): # either a dict or a list of dicts
                            #print ""
                            for candValue in data["candidates"][i][keyName]:
                                candidateFields = candValue.strip().split(self.inputTokenizer)
                                score = sm.sim_measure(candidateFields, queryFields)
                                candidates.append([data["candidates"][i]["uri"].encode('ascii', 'ignore'), score])
                                del candidateFields[:]
                        #else:# either a dict or a list of dicts
                        elif isinstance(data["candidates"][i][keyName], basestring): # either a dict or a list of dicts
                            candidateFields = data["candidates"][i][keyName].strip().split(self.inputTokenizer)
                            score = sm.sim_measure(candidateFields, queryFields)
                            candidates.append([data["candidates"][i]["uri"].encode('ascii', 'ignore'), score])
                            del candidateFields[:]
            elif isinstance(data["uri"][keyName], basestring):
                queryString = data["uri"][keyName]
                queryFields = queryString.strip().split(self.inputTokenizer)
                candidates = []
                for i in range(len(data["candidates"])):
                        if isinstance(data["candidates"][i][keyName], list):
                            #print ""
                            for candValue in data["candidates"][i][keyName]:
                                candidateFields = candValue.strip().split(self.inputTokenizer)
                                score = sm.sim_measure(candidateFields, queryFields)
                                candidates.append([data["candidates"][i]["uri"].encode('ascii', 'ignore'), score])
                                del candidateFields[:]
                        else:
                            candidateFields = data["candidates"][i][keyName].strip().split(self.inputTokenizer)
                            score = sm.sim_measure(candidateFields, queryFields)
                            candidates.append([data["candidates"][i]["uri"].encode('ascii', 'ignore'), score])
                            del candidateFields[:]
            else:
                print "Error: candidate attribute is not dict nor list: " + str(type(data["uri"][keyName]))

        candidates.sort(key=lambda tup: tup[1],reverse=True)

        #if data["uri"]["uri"] == "http://isi.edu/knowledgGraph/entity/31844":
         #   print data["uri"]["uri"] + " was processed. Candidates:\n"
          #  print str(candidates)
        if data["uri"]["uri"] == "http://edan.si.edu/saam/id/person-institution/101":
            print data["uri"]["uri"] + " was processed. Candidates:\n"
            print str(candidates)
        #if data["uri"]["uri"] == "http://isi.edu/knowledgGraph/entity/99455":
         #   print data["uri"]["uri"] + " was processed. Candidates:\n"
          #  print str(candidates)
        if self.outputFilter == "count":
            candidates = candidates[:self.numCand]
        else:
            #candidates = [t for t in candidates if t[1] > self.scoreThr]
            candidates = [t for t in candidates if t[1] > float(threshold)]
        #print >>outputFile, str(json.dumps(self.generateJson(data["uri"]["uri"], candidates, "matches")))

        # not test
        #del candidates[:]
        if len(candidates) > 0:
            matchesURIs = []
            topScore = float(candidates[0][1])
            for i in range(len(candidates)):
                if float(candidates[i][1]) == topScore and float(candidates[i][1]) >= float(threshold):
                    matchesURIs.append(candidates[i][0])
            if len(matchesURIs) > 0:
                return set(matchesURIs)
            else:
                return None
            #if float(candidates[0][1]) > float(threshold):
            #    return str(candidates[0][0]) # return uri of top candidate
            #else:
            #    return None
        else:
            return None


    def getTopCandidateURIwithGT(self,jsonObj, threshold, keyName, groundTruthLines, groundTruthIndex):
        sm = StringMatcher(self.jaccardMethod, self.jaccardThr)
        candidate_fields = []
        #outputFile = open(sys.argv[2],"w")
        decoder = json.JSONDecoder()
        data = jsonObj




        if isinstance(data["uri"][keyName], dict):
            queryString = data["uri"][keyName][keyName]
            queryFields = queryString.split(self.inputTokenizer)
            candidates = []
            for i in range(len(data["candidates"])):
                if isinstance(data["candidates"][i][keyName], list):
                    #print ""
                    for candValue in data["candidates"][i][keyName]:
                        candidateFields = candValue.split(self.inputTokenizer)
                        score = sm.sim_measure(candidateFields, queryFields)
                        candidates.append([data["candidates"][i]["uri"].encode('ascii', 'ignore'), score])
                        del candidateFields[:]

                else:
                    candidateFields = data["candidates"][i][keyName].split(self.inputTokenizer)
                    score = sm.sim_measure(candidateFields, queryFields)
                    candidates.append([data["candidates"][i]["uri"].encode('ascii', 'ignore'), score])
                    del candidateFields[:]


        elif isinstance(data["uri"][keyName], list):
            candidates = []
            for i in range(len(data["uri"][keyName])):
                queryString = data["uri"][keyName][i][keyName]
                queryFields = queryString.split(self.inputTokenizer)

                for i in range(len(data["candidates"])):
                    if isinstance(data["candidates"][i][keyName], list):
                        #print ""
                        for candValue in data["candidates"][i][keyName]:
                            candidateFields = candValue.split(self.inputTokenizer)
                            score = sm.sim_measure(candidateFields, queryFields)
                            candidates.append([data["candidates"][i]["uri"].encode('ascii', 'ignore'), score])
                            del candidateFields[:]

                    else:
                        candidateFields = data["candidates"][i][keyName].split(self.inputTokenizer)
                        score = sm.sim_measure(candidateFields, queryFields)
                        candidates.append([data["candidates"][i]["uri"].encode('ascii', 'ignore'), score])
                        del candidateFields[:]


        candidates.sort(key=lambda tup: tup[1],reverse=True)

        if self.outputFilter == "count":
            candidates = candidates[:self.numCand]
        else:
            candidates = [t for t in candidates if t[1] > self.scoreThr]
        #print >>outputFile, str(json.dumps(self.generateJson(data["uri"]["uri"], candidates, "matches")))

        # not test
        #del candidHybridJaccard.pyates[:]




        currentTargetInGT = False
        gtLinks = []
        curr_targetURI = ""
        gtLinkLost = False
        for groundTruthLine in groundTruthLines:
            groundTruthLineList = groundTruthLine.split("\t")
            targetURI = groundTruthLineList[0]
            if targetURI == data["uri"]["uri"]:
                curr_targetURI = targetURI
                currentTargetInGT = True
                gtLinks.append(groundTruthLineList[groundTruthIndex].replace("\n",""))
            #elif currentTargetInGT:
             #   break


        if float(candidates[0][1]) > float(threshold):
            if currentTargetInGT:
                gtCandidateFound = False
                candidateName = ""
                topCandName = ""
                for i in range(len(data["candidates"])):
                    if data["candidates"][i]["uri"] in gtLinks:
                        gtCandidateFound = True
                        candidateName = data["candidates"][i]["name"]
                    if candidates[0][0] == data["candidates"][i]["uri"]:
                        topCandName = data["candidates"][i]["name"]
                if gtCandidateFound and not (candidates[0][0] in gtLinks)  :
                    print "GT Target " + curr_targetURI + " will not have a GT link. Candidates: " + str(candidates)
                    print "Top candidate URI: " + candidates[0][0] + ", GT link: " + str(gtLinks)
                    print "Top candidate name: " + topCandName + ", GT name: " + candidateName
                    print "GT Link was found but was not top hybrid jaccard.: " + str(candidates[0][0])
                    print "--------------------------"

                if not gtCandidateFound:
                    print "GT Target " + curr_targetURI + " will no have a link."
                    print "GT Link was not in LSH results:" + str(gtLinks)
                    print "--------------------------"


            return str(candidates[0][0]) # return uri of top candidate
        else:

            if currentTargetInGT:
                print "GT Target " + curr_targetURI + " will no have a link. Low score."
                #print "Cluster: \n" + prettyPrintJson(data)

                gtCandidateFound = False
                for i in range(len(data["candidates"])):
                    if data["candidates"][i]["uri"] in gtLinks:
                        gtCandidateFound = True

                if not gtCandidateFound:
                    print "GT Link was not in LSH results."

                elif isinstance(data["uri"][keyName], dict): # target has one value
                    queryString = data["uri"][keyName][keyName]
                    queryFields = queryString.split(self.inputTokenizer)
                    #print "current target data value is string"
                    #print "current num of cand:" + str(len(data["candidates"]))
                    gtCandidateFound = False
                    for i in range(len(data["candidates"])):
                        if isinstance(data["candidates"][i][keyName], list): # candidate has multiple values
                            #print "current cand data value are list"
                            #if data["candidates"][i]["uri"] == candidates[0][0] and data["candidates"][i]["uri"] in gtLinks : # the candidate that would be selected but wasnt
                            if data["candidates"][i]["uri"] in gtLinks : # the candidate that would be selected but wasnt
                                gtLinkLost = True
                                for candValue in data["candidates"][i][keyName]:
                                    #print "Candidate values: " + str(data["candidates"][i][keyName])
                                    candidateFields = candValue.split(self.inputTokenizer)
                                    score = sm.sim_measure(candidateFields, queryFields)
                                    #candidates.append([data["candidates"][i]["uri"].encode('ascii', 'ignore'), score])
                                    print "Current target: " +  data["uri"]["uri"]
                                    print "Filtering out GT Link: " + data["candidates"][i]["uri"]
                                    #print "JSON:\n" +prettyPrintJson(data["candidates"][i])
                                    print "Score: " + str(score)
                                    print "Target values: " + str(queryFields)
                                    print "Candidate values " + str(candidateFields)
                                    gtCandidateFound = True
                                    del candidateFields[:]
                        else: # candidate has one value
                            #print "current cand data value are dic"
                            #if data["candidates"][i]["uri"] == candidates[0][0] and data["candidates"][i]["uri"] in gtLinks : # the candidate that would be selected but wasnt
                            if data["candidates"][i]["uri"] in gtLinks : # the candidate that would be selected but wasnt
                                gtLinkLost = True
                                #print str(data["candidates"][i])
                                candidateFields = data["candidates"][i][keyName].split(self.inputTokenizer)
                                score = sm.sim_measure(candidateFields, queryFields)
                                #candidates.append([data["candidates"][i]["uri"].encode('ascii', 'ignore'), score])

                                print "Current target: " +  data["uri"]["uri"]
                                print "Filtering out GT Link: " + data["candidates"][i]["uri"]
                                #print "JSON:\n" +prettyPrintJson(data["candidates"][i])
                                #print str(data["candidates"][i])
                                #print "Threshold: " + str(threshold)
                                print "Score: " + str(score)
                                print "Target values: " + str(queryFields)
                                print "Candidate values " + str(candidateFields)
                                gtCandidateFound = True
                                del candidateFields[:]
                    if not gtCandidateFound:
                        print "GT Link was not in LSH results."
                elif isinstance(data["uri"][keyName], list): # target has multiple values
                    #print "current target data value is list"
                    #print "current num of cand:" + str(len(data["candidates"]))
                    gtCandidateFound = False
                    for i in range(len(data["uri"][keyName])):
                        queryString = data["uri"][keyName][i][keyName]
                        queryFields = queryString.split(self.inputTokenizer)

                        for i in range(len(data["candidates"])):
                            if isinstance(data["candidates"][i][keyName], list): # candidate has multiple values
                                #print "current cand data value are list"
                                #if data["candidates"][i]["uri"] == candidates[0][0] and data["candidates"][i]["uri"] in gtLinks : # the candidate that would be selected but wasnt
                                if data["candidates"][i]["uri"] in gtLinks : # the candidate that would be selected but wasnt
                                    gtLinkLost = True
                                    for candValue in data["candidates"][i][keyName]:
                                        #print "Candidate values: " + str(data["candidates"][i][keyName])
                                        candidateFields = candValue.split(self.inputTokenizer)
                                        score = sm.sim_measure(candidateFields, queryFields)
                                        #candidates.append([data["candidates"][i]["uri"].encode('ascii', 'ignore'), score])
                                        print "Current target: " +  data["uri"]["uri"]
                                        print "Filtering out GT Link: " + data["candidates"][i]["uri"]
                                        #print "JSON:\n" +prettyPrintJson(data["candidates"][i])
                                        print "Score: " + str(score)
                                        print "Target values: " + str(queryFields)
                                        print "Candidate values " + str(candidateFields)
                                        gtCandidateFound = True
                                        del candidateFields[:]
                            else:
                                #print "current cand data value are dict"
                                #if data["candidates"][i]["uri"] == candidates[0][0] and data["candidates"][i]["uri"] in gtLinks : # the candidate that would be selected but wasnt
                                if data["candidates"][i]["uri"] in gtLinks : # the candidate that would be selected but wasnt
                                    gtLinkLost = True
                                    #print str(data["candidates"][i])
                                    candidateFields = data["candidates"][i][keyName].split(self.inputTokenizer)
                                    score = sm.sim_measure(candidateFields, queryFields)
                                    #candidates.append([data["candidates"][i]["uri"].encode('ascii', 'ignore'), score])
                                    print "Current target: " +  data["uri"]["uri"]
                                    print "Filtering out GT Link: " + data["candidates"][i]["uri"]
                                    #print "JSON:\n" +prettyPrintJson(data["candidates"][i])
                                    #print str(data["candidates"][i])
                                    #print "Threshold: " + str(threshold)
                                    print "Score: " + str(score)
                                    print "Target values: " + str(queryFields)
                                    print "Candidate values " + str(candidateFields)
                                    gtCandidateFound = True
                                    del candidateFields[:]

            if gtLinkLost:
                print "Ground truth link lost for : " + curr_targetURI
                print "-------------------"
            return None
#hj = HybridJaccard("config.json")
#hj.generateCandidates()