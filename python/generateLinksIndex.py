__author__ = 'Gleb'

import sys
import json
from copy import deepcopy
from knowledgeGraphHelpers import writeJsonLine2File

reload(sys)
sys.setdefaultencoding('utf8')

# This python script iterates over a set of clusters holding a target entity and it's candidate entities.
# It builds up a reverse index. The index holds an entry for each candidate. For each candidate it includes all target entities
# where the entity is one of the candidates.

def parse_args():
    global inputLinksFilePath
    global outputFile
    for arg_idx, arg in enumerate(sys.argv):
        if arg == "--inputLinksFilePath":
            inputLinksFilePath = sys.argv[arg_idx+1]
            continue
        if arg == "--outputFile":
            outputFile = sys.argv[arg_idx+1]
            continue

inputLinksFilePath = None
outputFile = None

parse_args()

inputLinksFile = open(inputLinksFilePath)
inputLinksLines = inputLinksFile.readlines()

alreadyFoundKGURIs = []
alreadyFoundKGURIsIdxs = []
indexObjLines = []

counter = 0

# iterate over each cluster
for linkJsonLine in inputLinksLines:

    counter = counter + 1
    print "Link " + str(counter) + " of " + str(len(inputLinksLines)-1) + " is currently processed."

    linkJson = json.loads(linkJsonLine.strip())
    candidatesJson = linkJson["candidates"]
    dataSourceEntityJson = linkJson["uri"]

    # for each candidate in the cluster, create an index holding the candidate and all his target entities
    for kgEntityCandidate in candidatesJson:
        kgEntityCandidateURI = kgEntityCandidate["uri"]

        # this candidate is found for the first time, create new index entry
        if kgEntityCandidateURI not in alreadyFoundKGURIs:

            indexObj = {}
            indexObj["uri"] = kgEntityCandidateURI
            targetObj = []
            targetObj.append(deepcopy(dataSourceEntityJson))
            indexObj["targets"] = targetObj
            indexObjLines.append(deepcopy(json.dumps(indexObj)))
            alreadyFoundKGURIsIdxs.append([kgEntityCandidateURI,len(indexObjLines)-1])
            alreadyFoundKGURIs.append(kgEntityCandidateURI)
        #this candidate was already found once, find index entry and update the index entry
        else:
            idx = -1
            for i, v in enumerate(alreadyFoundKGURIsIdxs):
                if v[0] == kgEntityCandidateURI:
                    idx = v[1]
                    break
            if idx > -1:
                indexObj = json.loads(indexObjLines[idx])
                targetObj = indexObj["targets"]
                targetObj.append(deepcopy(dataSourceEntityJson))
                indexObj["targets"] = targetObj
                indexObjLines[idx] = deepcopy(json.dumps(indexObj))


indexInfoObj = {}
indexInfoObj["idxs"] = alreadyFoundKGURIsIdxs
indexObjLines.append(deepcopy(json.dumps(indexInfoObj)))
writeJsonLine2File(indexObjLines,outputFile)
