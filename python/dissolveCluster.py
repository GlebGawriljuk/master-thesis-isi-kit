__author__ = 'Gleb'

import sys
import json
from copy import deepcopy
from knowledgeGraphHelpers import prettyPrintListOfJsonLines

reload(sys)
sys.setdefaultencoding('utf8')

def parse_args():
    global inputKnowledgeGraphFilePath
    global clusterURI
    global sameAsURI
    global outputKnowledgeGraphFilePath
    for arg_idx, arg in enumerate(sys.argv):
        if arg == "--inputKnowledgeGraphFilePath":
            inputKnowledgeGraphFilePath = sys.argv[arg_idx+1]
            continue
        if arg == "--clusterURI":
            clusterURI = sys.argv[arg_idx+1]
            continue
        if arg == "--sameAsURI":
            sameAsURI = sys.argv[arg_idx+1]
            continue
        if arg == "--outputKnowledgeGraphFilePath":
            outputKnowledgeGraphFilePath = sys.argv[arg_idx+1]
            continue

def isProvEqual(prov1, prov2):
    if prov1 == prov2:
        return True
    else:
        return  False

inputKnowledgeGraphFilePath = None
clusterURI = None
sameAsURI = None
outputKnowledgeGraphFilePath = None

parse_args()


knowledgeGraphFile = open(inputKnowledgeGraphFilePath)
knowledgeGraphJsonLines = knowledgeGraphFile.readlines()

# store and temporarly delete knowledge graph info data
kgInfoObj = json.loads(knowledgeGraphJsonLines[len(knowledgeGraphJsonLines)-1])
del knowledgeGraphJsonLines[len(knowledgeGraphJsonLines)-1]

provForDissolve = []

for clusterIdx, clusterLine in enumerate(knowledgeGraphJsonLines):
    clusterJson = json.loads(clusterLine)
    if clusterJson["uri"] == clusterURI:

        newClusterJson = {}
        provJson2BeRemove = None
        sameAsRoles = clusterJson["sameAs"]

        if isinstance(sameAsRoles, dict):
            # nothing to do because cluster has only one sameAs Role, hence it cannot be a wrong relation
            break

        elif isinstance(sameAsRoles, list):
            for roleIdx, sameAsRole_i in enumerate(sameAsRoles):
                if sameAsRole_i["sameAs"] == sameAsURI:
                    # add sameAs role to new cluster
                    newClusterJson["sameAs"] = sameAsRole_i
                    # save prov of this role object
                    if isinstance(sameAsRole_i["wasGeneratedBy"], list):
                        provForDissolve = sameAsRole_i["wasGeneratedBy"]
                    else:
                        provForDissolve.append(sameAsRole_i["wasGeneratedBy"])

                    # remove this role object and overwrite the sameAs value
                    del sameAsRoles[roleIdx]
                    clusterJson["sameAs"] = sameAsRoles

        # iterate over all cluster attribute and delete every attribute with provJson2BeRemove
        for provForDissolve_i in provForDissolve:
            clusterAttrToDelete = []
            for clusterAttr, clusterValueRoles in clusterJson.iteritems():
                if isinstance(clusterValueRoles, dict):
                    if isinstance(clusterValueRoles["wasGeneratedBy"], dict):
                        if isProvEqual(clusterValueRoles["wasGeneratedBy"], provForDissolve_i):
                            # add attribute role to new cluster
                            newClusterJson[clusterAttr] = clusterValueRoles
                            # delete the attribute role completely from old cluster
                            clusterAttrToDelete.append(clusterAttr)

                    elif isinstance(clusterValueRoles["wasGeneratedBy"], list):
                        for prov_idx, clusterValueRoles_prov in enumerate(clusterValueRoles["wasGeneratedBy"]):
                            if isProvEqual(clusterValueRoles_prov, provForDissolve_i):
                                # delete only this prov dict
                                del clusterValueRoles["wasGeneratedBy"][prov_idx]

                elif isinstance(clusterValueRoles, list):
                    for roleIdx, clusterValueRole in enumerate(clusterValueRoles):
                        if isProvEqual(clusterValueRole["wasGeneratedBy"], provForDissolve_i):
                            # add attribute role to new cluster
                            newClusterJson[clusterAttr] = clusterValueRole
                            # delete attribute role form old cluster
                            del clusterValueRoles[roleIdx]
                            clusterJson[clusterAttr] = clusterValueRoles

            if isinstance(clusterValueRoles, dict):
                for attrToDel in clusterAttrToDelete:
                    del clusterJson[attrToDel]

    # overwrite updated cluster in knowledge graph
    knowledgeGraphJsonLines[clusterIdx] = json.dumps(clusterJson)

newClusterJson["uri"] = "http://isi.edu/knowledgGraph/entity/" + str(kgInfoObj["numberOfEntities"])
newClusterJson["@context"] = "http://isi.edu/context/..."
# append new cluster to knowledge graph
knowledgeGraphJsonLines.append(deepcopy(json.dumps(newClusterJson)))
print "Final knowledge graph:\n" + prettyPrintListOfJsonLines(knowledgeGraphJsonLines)
# append updated knowledge graph info data
kgInfoObj["numberOfEntities"] = int(kgInfoObj["numberOfEntities"]) + 1
knowledgeGraphJsonLines[len(knowledgeGraphJsonLines)-1] = json.dumps(kgInfoObj)