__author__ = 'glebGawriljuk'
import sys
import re


def parse_args():
    global inputFilename
    global outputFilename
    global separator
    global colIdx

    for arg_idx, arg in enumerate(sys.argv):
        if arg == "--input":
            inputFilename = sys.argv[arg_idx+1]
            continue
        if arg == "--output":
            outputFilename = sys.argv[arg_idx+1]
            continue
        if arg == "--colIdx":
            colIdx = sys.argv[arg_idx+1]
            continue

inputFilename = None
outputFilename = None
colIdx = None
separator = "\t"

def die():
    print "Please input the required parameters"
    exit(1)


# check if this is main because when this this class is called from other module the following code should not get executed

if __name__ == '__main__':

    parse_args()

  #  if len(sys.argv) < 3:
   #     die()

    file = open(inputFilename,'r')
    outputFile = open(outputFilename,'w')
    count = 0
    for line in file:
        currentRecordList = list(line.rstrip('\n').split(separator))
        #print currentRecordList[0] + "\t" + colIdx + "\t" + currentRecordList[1] + "\n"

        try:outputFile.write(currentRecordList[0] + "\t" + colIdx + "\t" + currentRecordList[1] + "\n")
        except:
            z = str(line) # representation: "<exceptions.ZeroDivisionError instance at 0x817426c>"
            print z # output: "integer division or modulo by zero"
            print str(line + " line: " + str(count))
            print "Unexpected error:", sys.exc_info()[0]
            raise
        count = count + 1
        #print "Processing row " + str(count)
    file.close()
    outputFile.close()






