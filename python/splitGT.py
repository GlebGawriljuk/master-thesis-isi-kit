__author__ = 'Gleb'

import sys
import json

reload(sys)
sys.setdefaultencoding('utf8')

# python splitGT.py --kgFilePath [] --groundTruthFilepath []

def parse_args():
    global kgFilePath
    global groundTruthFilepath
    for arg_idx, arg in enumerate(sys.argv):
        if arg == "--kgFilePath":
            kgFilePath = sys.argv[arg_idx+1]
            continue
        if arg == "--groundTruthFilepath":
            groundTruthFilepath = sys.argv[arg_idx+1]
            continue

kgFilePath = None
groundTruthFilepath = None

parse_args()

SAAM_COL_IDX = 0
ULAN_COL_IDX = 1
VIAF_COL_IDX = 2
DBP_COL_IDX = 3

saamGT = open("saam_GT.tsv", 'w')
ulanGT = open("ulan_GT.tsv", 'w')
viafGT = open("viaf_GT.tsv", 'w')
dbpGT = open("dbpedia_GT.tsv", 'w')

ulanURIsNotFoundFile = open("ulanURIsNotFound.tsv", "w")
kgFile = open(kgFilePath)
kgJsonLines = kgFile.readlines()

groundTruthFile = open(groundTruthFilepath)
groundTruthFileLines = groundTruthFile.readlines()

counter = 0
size = len(groundTruthFileLines)

for groundTruthLine in groundTruthFileLines:
    groundTruthLineList = groundTruthLine.split('\t')
    saamURIs = groundTruthLineList[SAAM_COL_IDX].split(';')
    ulanURIs = groundTruthLineList[ULAN_COL_IDX].split(';')
    viafURIs = groundTruthLineList[VIAF_COL_IDX].split(';')
    dbpURIs = groundTruthLineList[DBP_COL_IDX].split(';')
    for ulanURI in ulanURIs:
        kgEntiyURI = ""
        # find KG URI for current groundTruthLine
        for kgCluster in kgJsonLines:
            clusterJson = json.loads(kgCluster.strip())
            if 'sameAs' in clusterJson:
                sameAsLinks = clusterJson["sameAs"]
                if isinstance(sameAsLinks, dict):
                    if sameAsLinks["sameAs"] == ulanURI:
                        kgEntiyURI = clusterJson["uri"]
                        break
                elif isinstance(sameAsLinks, list):
                    kgURI_found = False
                    for sameAsLink in sameAsLinks:
                        if sameAsLink["sameAs"] == ulanURI:
                            kgEntiyURI = clusterJson["uri"]
                            kgURI_found = True
                            break

                    if kgURI_found: # break outer loop
                        break
                else:
                    print "Error: KG samesAs links are neither dict nor list."

        if not kgEntiyURI == "":
            counter = counter + 1
            print str(counter) + " of " + str(size) + "GT Links processed."
            ulanGT.write(kgEntiyURI + "\t" + ulanURI + '\n')

            for sameURI in saamURIs:
                saamGT.write(kgEntiyURI + "\t" + sameURI + '\n')
            for viafURI in viafURIs:
                viafGT.write(kgEntiyURI + "\t" + viafURI + '\n')
            for dbpURI in dbpURIs:
                dbpGT.write(kgEntiyURI + "\t" + dbpURI.replace('\n','') + '\n')
        else:
            print "Error: ulan URI not found: " + ulanURI
            ulanURIsNotFoundFile.write(ulanURI + '\n')