__author__ = 'd22admin'

import sys
import json

reload(sys)
sys.setdefaultencoding('utf8')

def printResults(groundTruthSize, numberOfTargets, numberOfCandidates, precision, recall, truePositive, maxNumOfCandidates, falsePositive,
                 totalNumberOfCandidates):
    outputFile = open("evaluateFilteredResults_Output.tsv", 'w')
    outputFile.write("GroundTruth size: " + str(groundTruthSize) + '\n')
    outputFile.write("Number of GT targets in Filter results: " + str(numberOfTargets) + '\n')
    outputFile.write("Number of Filter candidates: " + str(numberOfCandidates) + '\n')
    outputFile.write("Total number of candidates: " + str(totalNumberOfCandidates) + '\n')
    outputFile.write("Max number of Filter candidates: " + str(maxNumOfCandidates) + '\n')
    outputFile.write("Average number of candidates per target: " + str(numberOfCandidates/float(numberOfTargets)) + '\n')
    outputFile.write("True positive: " + str(truePositive) + '\n')
    outputFile.write("False positive: " + str(falsePositive) + '\n')
    outputFile.write("Precision: " + str(precision) + '\n')
    outputFile.write("Recall: " + str(recall) + '\n')


def countCandidates(filteredLinksLines):
    targetNum = 0
    candNum = len(filteredLinksLines)
    tempList = []
    for filteredLink in filteredLinksLines:
        tempList.append(filteredLink.split("\t")[0])
    return  [len(set(tempList)), candNum]

def parse_args():
    global filteredClustersFilepath
    global groundTruthFilepath
    for arg_idx, arg in enumerate(sys.argv):
        if arg == "--filteredClustersFilepath":
            filteredClustersFilepath = sys.argv[arg_idx+1]
            continue
        if arg == "--groundTruth":
            groundTruthFilepath = sys.argv[arg_idx+1]
            continue

filteredClustersFilepath = None
groundTruthFilepath = None

parse_args()

filteredClustersFile = open(filteredClustersFilepath)
filteredClustersLines = filteredClustersFile.readlines()

groundTruthFile = open(groundTruthFilepath)
groundTruthLines = groundTruthFile.readlines()

groundTruthSize = len(groundTruthLines)
truePositive = 0
numberOfCandidates = 0
totalNumberOfCandidates = 0
numberOfTargets = 0
maxNumOfCandidates = 0
count = 1
foundGTTargets = []
totalCandTargetURI = []

linksNotFoundFile = open("evaluateFilteredResults_LinksNotFound.tsv", 'w')
falseLinksFoundFile = open("evaluateFilteredResults_falseLinksFound.tsv", 'w')

indexList = []

for idx, filteredClusterLine in enumerate(filteredClustersLines):
    filteredClusterJson = json.loads(filteredClusterLine.strip())
    clusterURI = filteredClusterJson["uri"]["uri"]
    indexList.append([clusterURI,idx])
    clusterCandidates = filteredClusterJson["candidates"]
    totalNumberOfCandidates = totalNumberOfCandidates + len(clusterCandidates)

for groundTruthLine in groundTruthLines:

    groundTruthLineList = groundTruthLine.split('\t')
    #targetURI = groundTruthLineList[0].strip()
    #gtLinkURI = groundTruthLineList[1].replace('\n','').strip()

    gtLinkURI = groundTruthLineList[0].strip()
    targetURI = groundTruthLineList[1].replace('\n','').strip()

    gtFound = False
    gtTargetFound = False
    currNumOfCand = 0
    currentIndex = -1
    for index in indexList:
        if targetURI == index[0]:
            currentIndex = index[1]

    filteredClusterJson = json.loads(filteredClustersLines[currentIndex].strip())

    #for filteredClusterLine in filteredClustersLines:
        #filteredClusterJson = json.loads(filteredClusterLine.strip())
    clusterURI = filteredClusterJson["uri"]["uri"]
    clusterCandidates = filteredClusterJson["candidates"]

    #if clusterURI not in totalCandTargetURI:
        #totalCandTargetURI.append(clusterURI)
        #totalNumberOfCandidates = totalNumberOfCandidates + len(clusterCandidates)

    if clusterURI == targetURI:
        gtTargetFound = True
        if targetURI not in foundGTTargets:
            foundGTTargets.append(targetURI)

        numberOfCandidates = numberOfCandidates + len(clusterCandidates)
        currNumOfCand = len(clusterCandidates)

        for candidateJson in clusterCandidates:

            candidateURI = candidateJson["uri"]

            if candidateURI == gtLinkURI:
                truePositive = truePositive + 1

                gtFound = True
            else:
                falseLinksFoundFile.write(targetURI + "\t" + candidateURI + "\n")


    if not gtFound:
        linksNotFoundFile.write(targetURI + "\t" + gtLinkURI + "\n")

    if currNumOfCand > maxNumOfCandidates:
        maxNumOfCandidates = currNumOfCand


    print str(count) + " of " + str(groundTruthSize) + " GT links processed."
    count = count + 1

falsePositive = numberOfCandidates - truePositive
precision = truePositive / float(numberOfCandidates)
recall = truePositive / float(groundTruthSize)
numberOfTargets = len(foundGTTargets)
printResults(groundTruthSize, numberOfTargets, numberOfCandidates, precision, recall, truePositive, maxNumOfCandidates, falsePositive, totalNumberOfCandidates )