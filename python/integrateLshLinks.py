__author__ = 'glebgawriljuk'
import sys
import json
from copy import deepcopy
from datetime import datetime
from filters import applyJaroWinklerMaxFilterOnCluster
from knowledgeGraphHelpers import createRoleObjectFromValue, addRoleObjToNewEntityAttr, addProvToRole, prettyPrintJson, addRoleObjToExistEntityAttr, writeJsonLine2File, prettyWriteJson2File,createSameAsRoleObject, createNewClustersFromBaseFile, prettyPrintListOfJsonLines

reload(sys)
sys.setdefaultencoding('utf8')

def addToEntityCluster(candidateTargetURI, candidate, knowledgGraph):
    printLog = False
    addToEntityCluster_logs = None

    startTime = datetime.now()
    #check if entity already exists in knowledgGraph
    candidateEntityExistsInKG = False

    if verbose: print str(datetime.now()) + ": Checking whether " + candidateTargetURI + " is already in KG."

    if printLog: addToEntityCluster_logs = open("../../logs/addToEntityCluster_logs_" + str(startTime) +".txt", 'w')
    numOfClusters = len(knowledgGraph) -1

    for idx, entity in enumerate(knowledgGraph): # iterate over every cluster in knowledge graph
        #print "Trying to load cluster:\n" + entity
        entityJSON = json.loads(entity.strip())
        candidateJSON = candidate
        #yes:
        if idx < numOfClusters and entityJSON["uri"] == candidateTargetURI: # get the candidate for the current knowledge cluster

            if verbose: print candidateTargetURI + " exists in KG:\n" + prettyPrintJson(entityJSON) + "\nIterating over all KG entity attributes."
            candidateEntityExistsInKG = True

            if printLog:
                addToEntityCluster_logs.write("Candidate " + candidateJSON["uri"] + " is merged to cluster " + entityJSON["uri"] + "\nThe cluster before the merge:\n")
                addToEntityCluster_logs.write(prettyPrintJson(entityJSON) + "\n")

            for candidate_AttrKey, candidate_AttrValue in candidate.iteritems(): # for each canidate attribute, check it attribute exists in entity
                #appent data from lshCluster features to knowledgGraph entity
                if verbose: print str(datetime.now()) + ": Checking candidate attribute " + candidate_AttrKey
                currentCandidateAttrFound = False
                for entity_AttrKey, entity_attrRole in entityJSON.iteritems(): #iterate over all attribute Roles
                    # knowledgGraph entity attribute are either dict or list of dicts

                    if candidate_AttrKey == entity_AttrKey:
                        if verbose: print str(datetime.now()) + ": Attribute " + candidate_AttrKey + " exists in entity."
                        #Checking if the attribute value is equal

                        if isinstance(entity_attrRole, dict): # entity has only on attribute Role
                            if verbose: print str(datetime.now()) + ": Entity Key: " + entity_AttrKey + "| value: " + entity_attrRole[entity_AttrKey]

                            if isinstance(candidate_AttrValue, basestring):
                                if verbose: print str(datetime.now()) + ": Candidate Basestring Key: " + candidate_AttrKey + "| value: " + candidate_AttrValue

                                if entity_attrRole[entity_AttrKey].strip() == candidate_AttrValue.strip():
                                    if verbose: print str(datetime.now()) + ": 1---Value [" +  candidate_AttrValue + "] exists. Adding new PROV to entity_attrRole." #OK
                                    if verbose: print str(datetime.now()) + ": 1.1----wasGeneratedBy of " + entity_AttrKey + " is a dict.\n" + prettyPrintJson(entityJSON)
                                    addProvToRole(entity_attrRole, "wasGeneratedBy", provJSON)
                                    if verbose: print str(datetime.now()) + ": New prov added to Role: \n" + prettyPrintJson(entity_attrRole)

                                else:
                                    if verbose: print str(datetime.now()) + ": 1-3---Value [" +  candidate_AttrValue + "] does not exist. Adding new Role object to entity_AttrKey.\n" + prettyPrintJson(entityJSON)
                                    tempRoleObj = createRoleObjectFromValue(candidate_AttrValue, candidate_AttrKey, provJSON)
                                    addRoleObjToExistEntityAttr(entityJSON, tempRoleObj, candidate_AttrKey)
                                    if verbose: print str(datetime.now()) + ": Added new Role obj for key " + candidate_AttrKey + "\n" + prettyPrintJson(entityJSON)

                            elif isinstance(candidate_AttrValue, list):
                                valueExists = False

                                for candidate_value_i in candidate_AttrValue:
                                    if verbose: print str(datetime.now()) + ": Candidate Key: " + candidate_AttrKey + "| value: " + candidate_value_i

                                    if entity_attrRole[entity_AttrKey].strip() == candidate_value_i.strip():
                                        if verbose: print str(datetime.now()) + ": 2---Value [" +  candidate_value_i + "] exists. Adding new PROV to entity_attrRole." #OK
                                        if verbose: print str(datetime.now()) + ": 2.1---wasGeneratedBy of " + entity_AttrKey + " is a dict.\n" + prettyPrintJson(entityJSON)
                                        addProvToRole(entity_attrRole, "wasGeneratedBy", provJSON)
                                        if verbose: print str(datetime.now()) + ": New prov added to Role: \n" + prettyPrintJson(entity_attrRole)
                                        valueExists = True
                                        #break
                                        # TODO: add list of added value to ensure that not duplicate value from candidate_AttrValue is added.

                                    else:
                                        if verbose: print str(datetime.now()) + ": 2.3---Value [" +  candidate_value_i + "] does not exists. Add new Role object\n" + prettyPrintJson(entityJSON) #OK
                                        tempRoleObj = createRoleObjectFromValue(candidate_value_i, candidate_AttrKey, provJSON)
                                        addRoleObjToExistEntityAttr(entityJSON, tempRoleObj, candidate_AttrKey)
                                        if verbose: print str(datetime.now()) + ": Added new Role obj for key " + candidate_AttrKey + "\n" + prettyPrintJson(entityJSON)

                        elif isinstance(entity_attrRole, list): # entity has multiple attribute Roles
                            candidateStringValueFound = False
                            existingCandidateValues = []
                            for entity_attrRole_i in entity_attrRole:
                                if verbose: print str(datetime.now()) + ": Key: " + entity_AttrKey + "| value: " + entity_attrRole_i[entity_AttrKey]
                                if isinstance(candidate_AttrValue, basestring):
                                    if verbose: print str(datetime.now()) + ": Candidate Key: " + candidate_AttrKey + "| value: " + candidate_AttrValue
                                    if entity_attrRole_i[entity_AttrKey].strip() == candidate_AttrValue.strip() and not candidateStringValueFound:
                                        candidateStringValueFound = True
                                        if verbose: print str(datetime.now()) + ": 3---Value [" +  candidate_AttrValue + "] exists. Adding new PROV to entity_attrRole_i." #OK
                                        if verbose: print str(datetime.now()) + ": 3.1---wasGeneratedBy of " + entity_AttrKey + " is a dict.\n" + prettyPrintJson(entityJSON)
                                        addProvToRole(entity_attrRole_i, "wasGeneratedBy", provJSON)
                                        if verbose: print str(datetime.now()) + ": New prov added to Role: \n" + prettyPrintJson(entity_attrRole)

                                elif isinstance(candidate_AttrValue, list):
                                    for candidate_value_i in candidate_AttrValue:
                                        if isinstance(candidate_value_i, basestring) and not candidate_value_i.strip() in existingCandidateValues:
                                            if verbose: print str(datetime.now()) + ": Candidate Key: " + candidate_AttrKey + "| value: " + candidate_value_i
                                            if entity_attrRole_i[entity_AttrKey].strip() == candidate_value_i.strip():
                                                if verbose: print str(datetime.now()) + ": 4---Value [" +  candidate_value_i + "] exists. Add new PROV to entity_attrRole_i." #OK
                                                existingCandidateValues.append(candidate_value_i.strip())
                                                if verbose: print str(datetime.now()) + ": 4.1---wasGeneratedBy of " + entity_AttrKey + " is a dict.\n" + prettyPrintJson(entityJSON)
                                                addProvToRole(entity_attrRole_i, "wasGeneratedBy", provJSON)
                                                if verbose: print str(datetime.now()) + ": New prov added to Role: \n" + prettyPrintJson(entity_attrRole)
                                                break

                            if not candidateStringValueFound and isinstance(candidate_AttrValue, basestring): # if we checked all Roles of current candidate_AttrKey and candidate_AttrValue was not found in any of them
                                if verbose: print str(datetime.now()) + ": 5---Value [" +  candidate_AttrValue + "] does not exist. Add new Role object to candidate_AttrKey." + "\n" + prettyPrintJson(entityJSON) # OK
                                tempRoleObj = createRoleObjectFromValue(candidate_AttrValue, candidate_AttrKey, provJSON)
                                addRoleObjToExistEntityAttr(entityJSON, tempRoleObj, candidate_AttrKey)
                                if verbose: print str(datetime.now()) + ": New Role added to list of Roles for key: " + candidate_AttrKey + "\n" + prettyPrintJson(entityJSON)

                            if isinstance(candidate_AttrValue, list):  # if candidate has a list of values, we need to find all values which are new to the KG
                                # interate over all candidate values and add new Role for the ones which are not yet in the entity
                                for candidate_AttrValue_i in candidate_AttrValue:
                                    if isinstance(candidate_AttrValue_i, basestring) and not candidate_AttrValue_i.strip() in existingCandidateValues:
                                        if verbose: print str(datetime.now()) + ": 6---Value [" +  candidate_AttrValue_i + "] does not exists. Add new Role object candidate_AttrKey." + "\n" + prettyPrintJson(entityJSON) #OK
                                        tempRoleObj = createRoleObjectFromValue(candidate_AttrValue_i, candidate_AttrKey, provJSON)
                                        addRoleObjToExistEntityAttr(entityJSON, tempRoleObj, candidate_AttrKey)
                                        if verbose: print str(datetime.now()) + ": New Role added to list of Roles for key: " + candidate_AttrKey + "\n" + prettyPrintJson(entityJSON)

                        currentCandidateAttrFound = True
                        break

                if not currentCandidateAttrFound: # current candidate attribute is not in the cluster, add it to the cluster
                    if isinstance(candidate_AttrValue, basestring):
                        if verbose: print str(datetime.now()) + ": 7.1---Key [" +  candidate_AttrKey + "] does not exist and is a BASESTRING. Adding new Role object to entity_AttrKey.\n" + prettyPrintJson(entityJSON)
                        tempRoleObj = createRoleObjectFromValue(candidate_AttrValue, candidate_AttrKey, provJSON)
                        addRoleObjToNewEntityAttr(entityJSON, tempRoleObj, candidate_AttrKey)
                        if verbose: print str(datetime.now()) + ": Added new Role obj for key " + candidate_AttrKey + "\n" + prettyPrintJson(entityJSON)

                    elif isinstance(candidate_AttrValue, list):
                        for candidate_value_idx, candidate_value_i in enumerate(candidate_AttrValue):
                            if verbose: print str(datetime.now()) + ": 7.2---Key  [" +  candidate_AttrKey + "] does not exists and is a LIST. Add new Role object\n" + prettyPrintJson(entityJSON) #OK
                            tempRoleObj = createRoleObjectFromValue(candidate_value_i, candidate_AttrKey, provJSON)
                            if candidate_value_idx == 0:
                                addRoleObjToNewEntityAttr(entityJSON, tempRoleObj, candidate_AttrKey)
                            else:
                                addRoleObjToExistEntityAttr(entityJSON, tempRoleObj, candidate_AttrKey)
                            if verbose: print str(datetime.now()) + ": Added new Role obj for key " + candidate_AttrKey + "\n" + prettyPrintJson(entityJSON)

            #mark lshCluster resources
            markedResources.append(candidateJSON["uri"])
            if verbose: print str(datetime.now()) + ": SameAs relation added: \n" + prettyPrintJson(entityJSON)
            createSameAsRoleObject(entityJSON, candidateJSON, "uri", "wasGeneratedBy", provJSON)
            #if verbose: print str(datetime.now()) + ": SameAs relation added: \n" + prettyPrintJson(entityJSON)
            if verbose:print str(datetime.now()) + ": Candidate merge into " + entityJSON["uri"] + " completed:\n" + prettyPrintJson(entityJSON)

            if printLog:
                addToEntityCluster_logs.write("The cluster after the merge:\n")
                addToEntityCluster_logs.write(prettyPrintJson(entityJSON) + "\n")

            knowledgGraph.pop(idx)
            knowledgGraph.insert(idx,json.dumps(entityJSON))
            break
    #no:
    if not candidateEntityExistsInKG:
        if verbose: print str(datetime.now()) + ": Error: candidate target URI was not found in the knowledge graph. Linking should not be able to generate other URIs then in the knowledge graph."
    endTime = datetime.now()
    timeDelta = endTime - startTime
    if verbose: print str(datetime.now()) + ": Total duration: " + str(timeDelta)

    if printLog:
        addToEntityCluster_logs.write("Total duration = " + str(timeDelta))
        addToEntityCluster_logs.close()

def parse_args():
    global inputLshClusters
    global inputKnowledgeGraph
    global inputProvFilename
    global inputBaseData
    global outputFile
    for arg_idx, arg in enumerate(sys.argv):
        if arg == "--inputLshClusters":
            inputLshClusters = sys.argv[arg_idx+1]
            continue
        if arg == "--inputKnowledgGraph":
            inputKnowledgeGraph = sys.argv[arg_idx+1]
            continue
        if arg == "--inputProvFile":
            inputProvFilename = sys.argv[arg_idx+1]
            continue
        if arg == "--inputBaseData":
            inputBaseData = sys.argv[arg_idx+1]
            continue
        if arg == "--outputFile":
            outputFile = sys.argv[arg_idx+1]
            continue

def die():
    if verbose: print str(datetime.now()) + ": Please input the required parameters"
    if verbose: print str(datetime.now()) + ": Usage: createNewClusters.py --input <input filename> filename> --output <output filename>"
    exit(1)


inputLshClusters = None
inputKnowledgeGraph = None
inputProvFilename = None
inputBaseData = None
verbose = False
outputFile = None
knowledgeGraphLines = None

markedResources = []
parse_args()


inputBaseDataFile = open(inputBaseData)
baseDataLines = inputBaseDataFile.readlines()

provFile = open(inputProvFilename)
provJSON = json.load(provFile)



if inputKnowledgeGraph and inputLshClusters: # Do merging only if a knowledge graph AND Links are present is present

    timeStart = datetime.now()
    inputKnowledgeGraphFile = open(inputKnowledgeGraph)
    knowledgeGraphLines = inputKnowledgeGraphFile.readlines()
    timeDelta =  datetime.now() - timeStart
    if True: print str(datetime.now()) + ": Loading knowledge graph duration: " + str(timeDelta)


    timeStart = datetime.now()
    inputLshClustersFile = open(inputLshClusters)
    lshClustersJsonLshClusters = inputLshClustersFile.readlines()

    timeDelta =  datetime.now() - timeStart
    if True: print str(datetime.now()) + ": Loading Links duration: " + str(timeDelta)


    #print str(datetime.now()) + ": KG lines:\n" +  str(knowledgeGraphLines)
    if True: print str(datetime.now()) + ": Merging candidates initiated."
    if verbose: print str(datetime.now()) + ": Initial knowledge graph:\n" + prettyPrintListOfJsonLines(knowledgeGraphLines)
    if True: print str(datetime.now()) + ": Number of clusters to process: " + str(len(lshClustersJsonLshClusters)-1)
    counter = 1
    timeStart = datetime.now()
    for lshCluster in lshClustersJsonLshClusters:
        clusterJSON = json.loads(lshCluster.strip())
        if True: print str(datetime.now()) + ": " + str(counter) + ". cluster is processed..."
        counter = counter + 1
        #candidates = applyJaroWinklerMaxFilterOnCluster(lshCluster , 0.90)
        if verbose: print str(datetime.now()) + ":  cluster to be processed: \n" + lshCluster
        candidates = clusterJSON["candidates"]
        if verbose: print str(datetime.now()) + ": Current candidate object: \n" + prettyPrintListOfJsonLines(candidates)
        candidateTargetURI = clusterJSON["uri"]["uri"]
        if candidates and isinstance(candidates, list): # if filterResult is not empty
            for candidate in candidates:
                addToEntityCluster(candidateTargetURI,candidate,knowledgeGraphLines)
                if verbose: print str(datetime.now()) + ": Integrating Candidate finished. Intermediate knowledge graph:\n" + prettyPrintListOfJsonLines(knowledgeGraphLines)
        elif candidates and isinstance(candidates, dict):
            addToEntityCluster(candidateTargetURI,candidates,knowledgeGraphLines)
            if verbose: print str(datetime.now()) + ": Integrating Candidate finished. Intermediate knowledge graph:\n" + prettyPrintListOfJsonLines(knowledgeGraphLines)

    timeDelta =  datetime.now() - timeStart
    if True: print str(datetime.now()) + ": Merging candidates completed. Duration: " + str(timeDelta)



else:
    knowledgeGraphLines = []
    numOfEntitiesObj = {}
    numOfEntitiesObj["numberOfEntities"] = 0
    knowledgeGraphLines.append(deepcopy(json.dumps(numOfEntitiesObj)))

numOfKgEntities = int(json.loads(knowledgeGraphLines[len(knowledgeGraphLines)-1])["numberOfEntities"])
if True: print str(datetime.now()) + ": Creating new clusters. Current number of entities: " + str(numOfKgEntities)

timeStart = datetime.now()
createNewClustersFromBaseFile(baseDataLines, knowledgeGraphLines, markedResources, provJSON)
numOfKgEntities = int(json.loads(knowledgeGraphLines[len(knowledgeGraphLines)-1])["numberOfEntities"])
timeDelta =  datetime.now() - timeStart
if True: print str(datetime.now()) + ": Creating new clusters completed. Current number of entities: " + str(numOfKgEntities) + " Duration: " + str(timeDelta)

if verbose: print str(datetime.now()) + ": Integrating Links finished. Final knowledge graph:\n" + prettyPrintListOfJsonLines(knowledgeGraphLines)

timeStart = datetime.now()
writeJsonLine2File(knowledgeGraphLines, outputFile)
timeDelta =  datetime.now() - timeStart
if True: print str(datetime.now()) + ": Writing JSON file completed. Duration: " + str(timeDelta)