__author__ = 'Gleb'

import sys
import json

reload(sys)
sys.setdefaultencoding('utf8')

# python splitGT.py --kgFilePath [] --groundTruthFilepath []

def printResults( gtSize, numberOfTargets, numberOfGtCandidates, totalNumberOfCandidates, truePositive,
                  numberOfCorrectCandidates, numberOfIncorrectCandidates, precision, recall, tag):
    outputFile = open("evaluateLinking_Results_" + str(tag) + ".tsv", 'w')
    outputFile.write("GroundTruth size: " + str(gtSize) + '\n')
    outputFile.write("Number of GT targets in Filter results: " + str(numberOfTargets) + '\n')
    outputFile.write("Number of GT candidates: " + str(numberOfGtCandidates) + '\n')
    outputFile.write("Total number of candidates: " + str(totalNumberOfCandidates) + '\n')
    #outputFile.write("Max number of Filter candidates: " + str(maxNumOfCandidates) + '\n')
    #outputFile.write("Average number of candidates per target: " + str(numberOfCandidates/float(numberOfTargets)) + '\n')
    outputFile.write("True positive: " + str(truePositive) + '\n')
    outputFile.write("Number of correct candidates: " + str(numberOfCorrectCandidates) + '\n')
    outputFile.write("Number of incorrect candidates: " + str(numberOfIncorrectCandidates) + '\n')
    outputFile.write("Precision: " + str(precision) + '\n')
    outputFile.write("Recall: " + str(recall) + '\n')

def getFileLines(filePath):
    kgFile = open(filePath)
    return  kgFile.readlines()

def getDataSourceIndex(dataSourecName):
    if dataSourecName == "saam":
        return 0
    elif dataSourecName == "ulan":
        return 1
    elif dataSourecName == "viaf":
        return 2
    elif dataSourecName == "dbp":
        return 3

def getGTLinks(groundTruthLines,sourceIndex):
    gtList = []
    for gtLine in groundTruthLines:
        lineList = []
        gtLineList = gtLine.strip().split('\t')
        lineList.append(gtLineList[sourceIndex].split(';')) # add target URIs
        lineList.append([])
        for x in range(0, 4):
            if x != sourceIndex: # do not add target URIs
                for uri in gtLineList[x].split(';'):
                    lineList[1].append(uri) # add match URIs
        gtList.append(lineList)
    return gtList

def getClusterIndex(inputClusterLines,inputKgIndex, inputGtLinks):
    result = []
    totalNumberOfCandidates = 0
    numberOfGtCandidates = 0
    c = 0
    length = len(inputClusterLines)
    for cluster in inputClusterLines:
        c = c + 1
        print "Generating index line " + str(c) + " of " + str(length)
        clusterList = [] # [taragetURI, [candidateURIs]]
        candidateURIList = [] # [candidateURIs]
        clusterJson = json.loads(cluster.strip())
        clusterURI = clusterJson["uri"]["uri"]
        clusterList.append(clusterURI)
        clusterCandidates =  clusterJson["candidates"]
        totalNumberOfCandidates = totalNumberOfCandidates + len(clusterCandidates)
        for i in inputGtLinks:
            if clusterURI in i[0]:
                numberOfGtCandidates = numberOfGtCandidates + len(clusterCandidates)

        for candidateJson in clusterCandidates:
            candidateKgURI = candidateJson["uri"]
            for kgEntity in inputKgIndex: # exchange the KG URI with its sameAs links
                if kgEntity[0] == candidateKgURI:
                    candidateURIList.append(kgEntity[1])
                    break
            #candidateURIList.append(sameAsLinks)

        clusterList.append(candidateURIList)
        result.append(clusterList)
    return [totalNumberOfCandidates,numberOfGtCandidates, result]

def getKGIndex(inputKgLines):
    result = [] # [[kgURI, [sameAsURIs]]]
    for kgEntity in inputKgLines:
        entityList = [] # [kgURI, [sameAsURIs]]
        entiyJson = json.loads(kgEntity.strip())
        if "uri" in entiyJson: # consider only kg entities and no other JSON object
            entityURI = entiyJson["uri"]
            entityList.append(entityURI)
            sameAsLinks = entiyJson["sameAs"]
            sameAsList = []
            if isinstance(sameAsLinks, list):
                for sameAs in sameAsLinks:
                    sameAsList.append(sameAs["sameAs"])
            elif isinstance(sameAsLinks, dict):
                sameAsList.append(sameAsLinks["sameAs"])
            entityList.append(sameAsList)
            result.append(entityList)
    return result


def parse_args():
    global kgFilePath
    global groundTruthFilepath
    global clusterFilePath
    global dataSourecName
    global outputTag
    for arg_idx, arg in enumerate(sys.argv):
        if arg == "--kgFilePath":
            kgFilePath = sys.argv[arg_idx+1]
            continue
        if arg == "--groundTruthFilepath":
            groundTruthFilepath = sys.argv[arg_idx+1]
            continue
        if arg == "--clusterFilePath":
            clusterFilePath = sys.argv[arg_idx+1]
            continue
        if arg == "--dataSourecName":
            dataSourecName = sys.argv[arg_idx+1]
            continue
        if arg == "--outputTag":
            outputTag = sys.argv[arg_idx+1]
            continue

kgFilePath = None
groundTruthFilepath = None
clusterFilePath = None
dataSourecName = None
outputTag = None

parse_args()

SAAM_COL_IDX = 0
ULAN_COL_IDX = 1
VIAF_COL_IDX = 2
DBP_COL_IDX = 3

kgLines = getFileLines(kgFilePath)
groundTruthLines = getFileLines(groundTruthFilepath)
clusterLines = getFileLines(clusterFilePath)

dataSourceIndex = getDataSourceIndex(dataSourecName)

print "Indexing GT ..."
gtLinks = getGTLinks(groundTruthLines,dataSourceIndex) #[[targetURis],[matched URIs from GT]]

print "Indexing Knowledge graph ..."
kgIndex = getKGIndex(kgLines) # [kgURI, [sameAsURIs]]

print "Indexing Clusters ..."
clusterData = getClusterIndex(clusterLines,kgIndex,gtLinks)
totalNumberOfCandidates = clusterData[0]
numberOfGtCandidates = clusterData[1]
clusterIndex = clusterData[2] # [targetURI,[[sameAsURIs]]

truePositive = 0

numberOfCorrectCandidates = 0
numberOfIncorrectCandidates = 0
targetURIsWithFoundMatches = []
targetURIsWithFoundCandidates = []
numberOfTargets = 0


linksNotFoundFile = open("evaluateLinking_LinksNotFound_" + str(outputTag) + ".tsv", 'w')
falseLinksFoundFile = open("evaluateLinking_FalseLinksFound_" + str(outputTag) + ".tsv", 'w')
print "Beginning evaluation ..."
counter = 0
for gtEntity in gtLinks:
    counter = counter + 1
    print "Evaluating GT entity " + str(counter) + " of " + str(len(gtLinks))
    gtTargetURIs = gtEntity[0]
    gtMatchURIs = gtEntity[1]

    gtEntityFound = False
    for currentCluster in clusterIndex:
        currentClusterTargetURI = currentCluster[0] #

        if currentClusterTargetURI in gtTargetURIs: # we found the cluster of the current GT target
            currentClusterCandidates = currentCluster[1] # [[sameAs links of kG entity ]]
            gtEntityFound = True

            if currentClusterTargetURI not in targetURIsWithFoundCandidates: # count for how many gt entity candidates were found
                numberOfTargets = numberOfTargets +1
                for uri in gtTargetURIs:
                    targetURIsWithFoundCandidates.append(uri)

            correctCandidateFound = False
            for currentClusterCand in currentClusterCandidates: #iterate over each KF entities list of sameAs links
                correctCandidate = False
                for currentSameAsLink in currentClusterCand: #iterate over each sameAs link of current KG candidate

                    if currentSameAsLink in gtMatchURIs: # we found a sameAs link which is a match candidate
                        correctCandidate = True

                        if currentClusterTargetURI not in targetURIsWithFoundMatches: # consider new found match only of no match was found for current target yet
                            correctCandidateFound = True
                            truePositive = truePositive + 1
                            for uri in gtTargetURIs:
                                targetURIsWithFoundMatches.append(uri)

                        break # stop iteration through sameAs links and go to next candidate

                if correctCandidate:
                    numberOfCorrectCandidates = numberOfCorrectCandidates + 1
                else:
                    numberOfIncorrectCandidates = numberOfIncorrectCandidates + 1
                    falseLinksFoundFile.write(str(gtTargetURIs) + "\t"  + str(currentClusterCand) + "\n")
            if not correctCandidateFound: # for current GT entity no correct candidates were found
                linksNotFoundFile.write(str(gtTargetURIs) + "\t"  + str(gtMatchURIs) + "\n")

# find targets which are not in the cluster set at all
for gtEntity in gtLinks:
    gtTargetURIs = gtEntity[0]
    targetFound = False
    for uri in gtTargetURIs:
        if uri in targetURIsWithFoundCandidates:
            targetFound = True
    if not targetFound:
        linksNotFoundFile.write(str(gtTargetURIs) + "\n")

recall = 0
if len(gtLinks)> 0:
    recall = truePositive / float(len(gtLinks))
precision = 0
if numberOfCorrectCandidates > 0 or numberOfIncorrectCandidates > 0:
    precision = numberOfCorrectCandidates / float(numberOfCorrectCandidates + numberOfIncorrectCandidates)
printResults(len(gtLinks), numberOfTargets, numberOfGtCandidates, totalNumberOfCandidates, truePositive,
             numberOfCorrectCandidates, numberOfIncorrectCandidates,precision, recall, outputTag )
print "DONE"