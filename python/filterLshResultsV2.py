__author__ = 'glebgawriljuk'

import sys
import json
from datetime import datetime
#import editdistance
import jaro
from copy import deepcopy
from knowledgeGraphHelpers import prettyPrintJson, writeJsonLine2File
from hybrid_jaccard.HybridJaccard import HybridJaccard

# python filterLshResults.py --inputClusters fileJSON_inline.json --methodName sameAttrValue,hybridJaccard --threshold 0.8 --outputFile testOutput

reload(sys)
sys.setdefaultencoding('utf8')

def generateSameAs(sameAsFile,targetJson, candidateJson):
    if isinstance(targetJson, basestring): # if no data was joined the targetJson will be a
        sameAsFile.write("<" + str(targetJson) + ">" + "\t" + "<https://schema.org/sameAs>" + "\t" + "<" + str(candidateJson["uri"]["uri"]) +">" + "\n")
    elif isinstance(targetJson, dict):
        sameAsFile.write("<" + str(targetJson["uri"]) + ">" + "\t" + "<https://schema.org/sameAs>" + "\t" + "<" + str(candidateJson["uri"]["uri"]) +">" + "\n")


def printToLogs(logFile, targetJson, candidateJson, attrName):
    if attrName in targetJson and attrName in candidateJson:
        if isinstance(targetJson[attrName], basestring):
            if isinstance(candidateJson[attrName], dict):
                #logFile.write(str(targetJson["uri"]) + "," + str(targetJson[attrName][attrName]) + "," + str(candidateJson["uri"]) + "," + str(candidateJson[attrName]) + "\n")
                logFile.write(str(targetJson["uri"]) + "\t" + str(candidateJson["uri"]) + "\n")
            elif isinstance(candidateJson[attrName], list):
                #logFile.write(str(targetJson["uri"]) + "," + str(targetJson[attrName][attrName]) + "," + str(candidateJson["uri"]) + "," + str(candidateJson[attrName][0])+ "\n")
                logFile.write(str(targetJson["uri"]) + "\t" + str(candidateJson["uri"]) + "\n")

        elif isinstance(targetJson[attrName], list):
            if isinstance(candidateJson[attrName], dict):
                #logFile.write(str(targetJson["uri"]) + "," + str(targetJson[attrName][0][attrName]) + "," + str(candidateJson["uri"]) + "," + str(candidateJson[attrName])+ "\n")
                logFile.write(str(targetJson["uri"]) + "\t" + str(candidateJson["uri"]) + "\n")
            elif isinstance(candidateJson[attrName], list):
                #logFile.write(str(targetJson["uri"]) + "," + str(targetJson[attrName][0][attrName]) + "," + str(candidateJson["uri"]) + "," + str(candidateJson[attrName][0])+ "\n")
                logFile.write(str(targetJson["uri"]) + "\t" + str(candidateJson["uri"]) + "\n")
    else:
        if isinstance(targetJson, basestring): # if no data was joined the targetJson will be a string
            logFile.write(str(targetJson) + "\t" + str(candidateJson["uri"])+ "\n")
        elif isinstance(targetJson, dict):
            logFile.write(str(targetJson["uri"]) + "\t" + str(candidateJson["uri"])+ "\n")

def filterSameAttrStrValue(targetObj, candidateObj, attrName):
    if attrName in targetObj and attrName in candidateObj:

        if isinstance(targetObj[attrName], basestring): # data source property is a string value

            if isinstance(candidateObj[attrName], basestring): # knowledge graph property is a base string
                if verbose: print str(datetime.now()) + ": filterSameAttrStrValue:comparing " + str(targetObj[attrName]) + " with " + str(candidateObj[attrName])
                if targetObj[attrName] == candidateObj[attrName]:
                    return True
                else:
                    return False

            elif isinstance(candidateObj[attrName],list): # knowledge graph property is a list of string
                for candidateValue_i in candidateObj[attrName]:
                    if targetObj[attrName] == candidateValue_i:
                        return True
                return False
            else:
                if verbose: print str(datetime.now()) + ": filterSameAttrStrValue: values to be compared are not basestring."
                return True

        if isinstance(targetObj[attrName], list): # data source property is a list of string values
            for targetDict_i in targetObj[attrName]:

                if isinstance(targetDict_i, basestring) and isinstance(candidateObj[attrName],basestring):
                    if verbose: print str(datetime.now()) + ": filterSameAttrStrValue:comparing " + str(targetDict_i) + " with " + str(candidateObj[attrName])
                    if targetDict_i == candidateObj[attrName]:
                        return True

               # elif isinstance(targetDict_i[attrName], basestring) and isinstance(candidateObj[attrName],list):
                elif isinstance(targetDict_i, basestring) and isinstance(candidateObj[attrName],list):
                    for candidateValue_i in candidateObj[attrName]:
                        if targetDict_i == candidateValue_i:
                            return True

                else:
                    if verbose: print str(datetime.now()) + ": filterSameAttrStrValue: values to be compared are not basestring."
                    return True

            return False
    else:
       if verbose: print str(datetime.now()) + ": filterSameAttrStrValue: objects do not have the key " + attrName
       return True

def editDistanceSim(v1, v2):
    lengthV1 = len(v1)
    lengthV2 = len(v2)
    return 1 - editdistance.eval(v1.strip(), v2.strip()) / float(max([lengthV1,lengthV2]))

def filterEditDistance(targetObj, candidateObj, attrName, threshold):

    entity_attrValues = None
    entity_attrRole = targetObj[attrName]

    #get target values from role objects
    if isinstance(entity_attrRole, dict):
        entity_attrValues = entity_attrRole[attrName]
    elif isinstance(entity_attrRole, list):
        entity_attrValues = []
        for entity_Role_i in entity_attrRole:
            entity_attrValues.append(entity_Role_i[attrName])
    else:
        print "Format of entity attribute value not recognized: entity: " +targetObj["uri"] + " attribute key: " + str(attrName)


    candidateValues = None
    candidateAttrObjs = candidateObj[attrName]
    candidateURI = candidateObj["uri"]

    if isinstance(candidateAttrObjs, basestring):
        candidateValues = candidateAttrObjs

        if isinstance(entity_attrValues, basestring):
            print "jaroWinkler( " + entity_attrValues + " , " + candidateValues + " )"
            filterValue = editDistanceSim(entity_attrValues.strip(), candidateValues.strip())
            print "CASE 1: Entity attribute is a String. Jaro-W = " + str(filterValue)
            if threshold < filterValue:
                #filterMaxValue = filterValue
                return True
            else:
                return False
        elif isinstance(entity_attrValues, list):
            for entityValue in entity_attrValues:
                #print "jaroWinkler( " + entity_nameValue + " , " + candidateValues + " )"
                filterValue = editDistanceSim(entityValue.strip(), candidateValues.strip())
                print "CASE 2: Entity attribute is a List. Jaro-W = " + str(filterValue)
                if threshold < filterValue:
                    #filterMaxValue = filterValue
                    return True
            return False

    elif isinstance(candidateAttrObjs, list):
        for candidateValue in candidateAttrObjs:
            #candidateValues.append(candidateValue)
            if isinstance(entity_attrValues, basestring):
                print "jaroWinkler( " + entity_attrValues + " , " + candidateValue + " )"
                filterValue = editDistanceSim(entity_attrValues.strip(), candidateValue.strip())
                print "CASE 3: Entity attribute is a String. Jaro-W = " + str(filterValue)
                if threshold < filterValue:
                    #filterMaxValue = filterValue
                    return True

            elif isinstance(entity_attrValues, list):
                for entityValue in entity_attrValues:
                    filterValue = editDistanceSim(entityValue.strip(), candidateValue.strip())
                    print "CASE 4: Entity attribute is a String. Jaro-W = " + str(filterValue)
                    if threshold < filterValue:
                        #filterMaxValue = filterValue
                        return True
        return False
    else:
        print "Format of candidate attribute value not recognized: candidate: " + candidateURI + " attribute key: " + "name"
    #print "filterMaxValue = " + str(filterMaxValue)

def filterJaroWinkler(targetObj, candidateObj, attrName, threshold):

    targetObjValues = []
    targetObjRoles = targetObj[attrName]

    # get target values from role objects
    if isinstance(targetObjRoles, dict):
        if isinstance(targetObj[attrName], basestring):
            targetObjValues.append(targetObjRoles[attrName])
    elif isinstance(targetObjRoles, list):
        for targetObjectRole in targetObjRoles:
            if isinstance(targetObjectRole[attrName], basestring):
                targetObjValues.append(targetObjectRole[attrName])
    else:
        print "Format of entity attribute value not recognized: entity: " +targetObj["uri"] + " attribute key: " + str(attrName)

    # get candidate values from role objects
    candidateValues = []
    if isinstance(candidateObj[attrName], basestring):
        candidateValues.append(candidateObj[attrName])
    elif isinstance(candidateObj[attrName], list):
        for candidateValue in candidateObj[attrName]:
            if isinstance(candidateValue, basestring):
                candidateValues.append(candidateValue)

    for candidateValue in candidateValues:
        for targetValue in targetObjValues:
            filterValue = jaro.jaro_winkler_metric(targetValue.strip(), candidateValue.strip())
            if threshold < filterValue:
                return True

    return False

def hybridJaccardEvaluation(clusterJSON, threshold, keyName, groundTruthLines, groundTruthIndex):
    hj = HybridJaccard("hybrid_jaccard/config.json")
    return hj.getTopCandidateURIwithGT(clusterJSON, threshold, keyName, groundTruthLines, groundTruthIndex)

def hybridJaccard(clusterJSON, threshold, keyName):
    hj = HybridJaccard("hybrid_jaccard/config.json")
    return hj.getTopCandidateURIV2(clusterJSON, threshold, keyName)

def parse_args():
    global inputClusters
    global methods
    global threshold
    global outputFile
    global groundTruthFilepath
    global groundTruthIndex
    global genSameAs
    for arg_idx, arg in enumerate(sys.argv):
        if arg == "--inputClusters":
            inputClusters = sys.argv[arg_idx+1]
            continue
        if arg == "--methodName":
            methods = sys.argv[arg_idx+1].split(",")
            continue
        if arg == "--threshold":
            threshold = float(sys.argv[arg_idx+1])
            continue
        if arg == "--outputFile":
            outputFile = sys.argv[arg_idx+1]
            continue
        if arg == "--groundTruth":
            groundTruthFilepath = sys.argv[arg_idx+1]
            continue
        if arg == "--groundTruthIndex":
            groundTruthIndex = int(sys.argv[arg_idx+1])
            continue
        if arg == "--sameAs":
            genSameAs = True
            continue


inputClusters = None
methods = None
threshold = None
outputFile = None
filterResult = None
groundTruthFilepath = None
groundTruthIndex = None
groundTruthFile = None
groundTruthLines = None

printLinksLog = True
genSameAs = False
addToEntityCluster_logs = None
sameAsFile = None

verbose = False
finalCluster = {}
tempMatchesCluster = {}

print "filterLshResults START."

parse_args()

inputClustersFile = open(inputClusters)
inputClustersJsonLines = inputClustersFile.readlines()

if groundTruthFilepath:
    groundTruthFile = open(groundTruthFilepath)
    groundTruthLines = groundTruthFile.readlines()

filteredClusterLines = []

timeStart = datetime.now().strftime("%d-%B-%Y_%I-%M%p")
if printLinksLog: addToEntityCluster_logs = open("sameAsLinks_logs_" + str(timeStart) +".txt", 'w')
if genSameAs: sameAsFile = open("sameAsTriples_" + str(timeStart) +".nt", 'w')

for clusterJsonLine in inputClustersJsonLines:
    clusterJSON = json.loads(clusterJsonLine.strip())
    if verbose: print str(datetime.now()) + ": filterLshResults: the cluster\n " + prettyPrintJson(clusterJSON)
    targetObj = clusterJSON["uri"]

    finalCluster["uri"] = targetObj
    finalCluster["candidates"] = clusterJSON["candidates"]
    #print "Current candidates: " + str(finalCluster["candidates"])
    for method in methods:
        #print "Method: " + str(method) + "started."
        #print "Current candidates before " + str(method) + " : " + str(finalCluster["candidates"])
        if method in ["jaroWinkler","editDistance","birthYear","birthYearDeathYear"]:
            tempMatchesCluster["uri"] = targetObj
            tempMatchesCluster["candidates"] = []
            for candidateObj in finalCluster["candidates"]:
                candidateURI = candidateObj["uri"]
                if verbose: print str(datetime.now()) + ": filterLshResults: the candidate JSON\n " + prettyPrintJson(candidateObj)
                #compare targetJsonObj with candidate
                if method == "jaroWinkler":
                    filterResult = filterJaroWinkler(targetObj,candidateObj,"name", threshold)
                elif method == "editDistance":
                    filterResult = filterEditDistance(targetObj,candidateObj,"name", threshold)
                elif method == 'birthYear':
                    filterResult = filterSameAttrStrValue(targetObj,candidateObj,"birthYear")
                elif method == 'birthYearDeathYear':
                    filterResult = filterSameAttrStrValue(targetObj,candidateObj,"birthYear")
                    if filterResult: filterResult = filterSameAttrStrValue(targetObj,candidateObj,"deathYear")
                else:
                    filterResult = True
                if verbose: print str(datetime.now()) + ": filterLshResults: did candidate pass?: " + str(filterResult)

                if filterResult:
                    #printToLogs(addToEntityCluster_logs,targetJson,candidateJson, "name")
                    tempMatchesCluster["candidates"].append(deepcopy(candidateObj))
            finalCluster["candidates"] = tempMatchesCluster["candidates"]
            #print "Current candidates after " + str(method) + " : " + str(finalCluster["candidates"])
        elif method in ["hybridJaccard"]:
            #print "Method: " + str(method) + "started."
            tempMatchesCluster["uri"] = targetObj
            tempMatchesCluster["candidates"] = []

            if isinstance(targetObj, dict):
                if groundTruthLines and groundTruthIndex:
                    #print "Filtering verbose"
                    candidateURIs = hybridJaccardEvaluation(clusterJSON, threshold, "name" , groundTruthLines, groundTruthIndex)
                else:
                    #candidateURIs = hybridJaccard(clusterJSON, threshold, "name" )
                    candidateURIs = hybridJaccard(finalCluster, threshold, "name" )

                if candidateURIs is not None and len(candidateURIs) > 0:
                    for candidateURI in candidateURIs:
                        for candidateObj in clusterJSON["candidates"]:
                            if candidateURI == candidateObj["uri"]:
                                #if printLinksLog: printToLogs(addToEntityCluster_logs,targetJson,candidateJson, "name")
                                #if genSameAs: generateSameAs(sameAsFile,targetJson,candidateJson)
                                tempMatchesCluster["candidates"].append(deepcopy(candidateObj))

                finalCluster["candidates"] = tempMatchesCluster["candidates"]
                #print "Current candidates after " + str(method) + " : " + str(finalCluster["candidates"])
            else: # if the targetJson does not have any data, return all candidates
                finalCluster["candidates"] = finalCluster["candidates"]
                #print "Current candidates after " + str(method) + " : " + str(finalCluster["candidates"])

    if len(finalCluster["candidates"]) > 0:
        filteredClusterLines.append(deepcopy(json.dumps(finalCluster)))
        for foundLink in finalCluster["candidates"]:
            if printLinksLog: printToLogs(addToEntityCluster_logs,targetObj,foundLink, "name")
            if genSameAs: generateSameAs(sameAsFile,targetObj,foundLink)

    if verbose: print str(datetime.now()) + ": filterLshResults: the cluster after the filter\n " + prettyPrintJson(tempMatchesCluster)

if printLinksLog:addToEntityCluster_logs.close()
writeJsonLine2File(filteredClusterLines, outputFile)
print "filterLshResult DONE."