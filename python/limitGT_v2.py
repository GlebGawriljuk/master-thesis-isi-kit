__author__ = 'Gleb'

import sys
import json

reload(sys)
sys.setdefaultencoding('utf8')


def getFileLines(filePath):
    kgFile = open(filePath)
    return  kgFile.readlines()

def getDataSourceIndex(dataSourecName):
    if dataSourecName == "saam":
        return 0
    elif dataSourecName == "ulan":
        return 1
    elif dataSourecName == "viaf":
        return 2
    elif dataSourecName == "dbp":
        return 3

def getGTLinks(groundTruthLines,sourceIndex):
    gtList = []
    for gtLine in groundTruthLines:
        lineList = []
        gtLineList = gtLine.strip().split('\t')
        lineList.append(gtLineList[sourceIndex].split(';')) # add target URIs
        lineList.append([])
        for x in range(0, 4):
            if x != sourceIndex: # do not add target URIs
                for uri in gtLineList[x].split(';'):
                    lineList[1].append(uri) # add match URIs
        gtList.append(lineList)
    return gtList

def glueURI(listOfURIs):
    string = ""
    c = 0
    for i_uri in listOfURIs:
        if c == 0:
            string = i_uri
        else:
            string = string + ";" + i_uri
            c = c + 1
    return  string

def toGtLine(inputGtEntityList):
    string = ""
    c = 0
    for i_uris in inputGtEntityList:
        if c == 0:
            string = i_uris
            c = c + 1
        else:
            string = string + "\t" + i_uris
            c = c + 1
    return  string + "\n"


def parse_args():
    global groundTruthFilepath
    global dataSourceFilePath
    global dataSourceName
    global outputFilePath
    for arg_idx, arg in enumerate(sys.argv):
        if arg == "--groundTruthFilepath":
            groundTruthFilepath = sys.argv[arg_idx+1]
            continue
        if arg == "--dataSourceFilePath":
            dataSourceFilePath = sys.argv[arg_idx+1]
            continue
        if arg == "--dataSourceName":
            dataSourceName = sys.argv[arg_idx+1]
            continue
        if arg == "--outputFilePath":
            outputFilePath = sys.argv[arg_idx+1]
            continue


groundTruthFilepath = None
dataSourceFilePath = None
outputFilePath = None
dataSourceName = None

parse_args()
dataSourceIndex = -1

SAAM_COL_IDX = 0
ULAN_COL_IDX = 1
VIAF_COL_IDX = 2
DBP_COL_IDX = 3

if dataSourceName == "ulan":
    dataSourceIndex = ULAN_COL_IDX
elif dataSourceName == "saam":
    dataSourceIndex = SAAM_COL_IDX
elif dataSourceName == "dbp":
    dataSourceIndex = DBP_COL_IDX
elif dataSourceName == "viaf":
    dataSourceIndex = VIAF_COL_IDX

groundTruthLines = getFileLines(groundTruthFilepath)
dataSourceLines = getFileLines(dataSourceFilePath)
print "Reading data source..."
dataSourceURIs = []
for dataSourceEntity in dataSourceLines:
    dsEntityJson = json.loads(dataSourceEntity.strip())
    clusterURI = dsEntityJson["uri"]
    dataSourceURIs.append(clusterURI)


limitedGTFile = open(outputFilePath, 'w')
print "Beginning limitation with  " + str(len(dataSourceURIs)) + " URIs."
counter = 0
for gtEntity in groundTruthLines:
    counter = counter + 1
    print "Limiting GT entity " + str(counter) + " of " + str(len(groundTruthLines))
    gtEntityList = gtEntity.strip().split('\t')
    gtURIs = gtEntityList[dataSourceIndex].split(';')
    newURIlist = []
    for uri in gtURIs:
        if uri in dataSourceURIs:
            newURIlist.append(uri)
    if len(newURIlist) > 0:
        gtEntityList[dataSourceIndex] = glueURI(newURIlist)
        newGTline = toGtLine(gtEntityList)
        limitedGTFile.write(newGTline)

print "DONE"