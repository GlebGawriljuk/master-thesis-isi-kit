__author__ = 'Gleb'

import sys
import json

reload(sys)
sys.setdefaultencoding('utf8')

def getFileLines(filePath):
    kgFile = open(filePath)
    return  kgFile.readlines()

def parse_args():
    global viafSingePropFilePath
    global listOfIdsFilePath
    global outputFilePath
    for arg_idx, arg in enumerate(sys.argv):
        if arg == "--viafDumpFilePath":
            viafDumpFilePath = sys.argv[arg_idx+1]
            continue
        if arg == "--listOfIdsFilePath":
            listOfIdsFilePath = sys.argv[arg_idx+1]
            continue
        if arg == "--outputFilePath":
            outputFilePath = sys.argv[arg_idx+1]
            continue
            
            
viafSingePropFilePath = None
listOfIdsFilePath = None
outputFilePath = None

parse_args()

limitedViafFile = open(outputFilePath, 'w')

print "Reading in files ..."
#viafDumpLines = getFileLines(viafDumpFilePath)
listOfIdsLines = getFileLines(listOfIdsFilePath)

limitedIDList = []

for i_id in listOfIdsLines:
    limitedIDList.append(i_id.strip())

c = 0
#or dumpLine in viafDumpLines:
with open(viafSingePropFilePath) as f:
    for dumpLine in f:
        c = c + 1
        print "Processing line " + str(c) #+ " of  " + str(len(viafDumpLines))
        dumpLineList = dumpLine.strip().split(' ')
        dumpLineSubjectID = dumpLineList[0].replace("<http://viaf.org/viaf/","").replace(">","").strip()

        if dumpLineSubjectID in limitedIDList:
            limitedViafFile.write(dumpLine)