__author__ = 'd22admin'

import sys
import json

reload(sys)
sys.setdefaultencoding('utf8')

def printResults(groundTruthSize, numberOfTargets, numberOfCandidates, precision, recall, truePositive):
    print "GroundTruth size: " + str(groundTruthSize)
    print "Number of LSH targets: " + str(numberOfTargets)
    print "Number of LSH candidates: " + str(numberOfCandidates)
    print "Average number of candidates per target: " + str(numberOfCandidates/float(numberOfTargets))
    print "True positive: " + str(truePositive)
    print "Precision: " + str(precision)
    print "Recall: " + str(recall)

def countCandidates(lshClustersJsonLines):
    targetNum = 0
    candNum = 0
    for lshClusterJsonLine in lshClustersJsonLines:
        lshClusterJSON = json.loads(lshClusterJsonLine.strip().replace("'","\""))
        lshCandidates = lshClusterJSON["candidates"]
        targetNum = targetNum + 1
        if isinstance(lshCandidates, dict):
            candNum = candNum + 1
        elif isinstance(lshCandidates, list):
            for candidate in lshCandidates:
                candNum = candNum + 1
        else:
            print"evaluateLshLinks > countCandidates: candidates are neither a list or a dict."
    return  [targetNum, candNum]

def parse_args():
    global lshClustersFilepath
    global groundTruthFilepath
    global groundTruthIndex
    for arg_idx, arg in enumerate(sys.argv):
        if arg == "--lshLinks":
            lshClustersFilepath = sys.argv[arg_idx+1]
            continue
        if arg == "--groundTruth":
            groundTruthFilepath = sys.argv[arg_idx+1]
            continue
        if arg == "--groundTruthIndex":
            groundTruthIndex = int(sys.argv[arg_idx+1])
            continue

lshClustersFilepath = None
groundTruthFilepath = None
groundTruthIndex = None

parse_args()

lshClustersFile = open(lshClustersFilepath)
lshClustersJsonLines = lshClustersFile.readlines()

groundTruthFile = open(groundTruthFilepath)
groundTruthLines = groundTruthFile.readlines()

groundTruthSize = 200
truePositive = 0
numberOfCandidates = countCandidates(lshClustersJsonLines)[1]
numberOfTargets = countCandidates(lshClustersJsonLines)[0]
lshIdxToBeDeleted = -1
count = 1
prevClusterURI = ""
gtLinkFound = False

for groundTruthLine in groundTruthLines:

    print str(count) + " of " + str(groundTruthSize) + " ground Truth links processed. LSH size: " + str(len(lshClustersJsonLines))

    groundTruthLineList = groundTruthLine.split('\t')

    if not groundTruthLineList[0] == prevClusterURI:
        gtLinkFound = False
    print "Current gt cluster URI: " + groundTruthLineList[0]

    # delete lsh cluster which already was evaluated
    if lshIdxToBeDeleted > -1:
        del lshClustersJsonLines[lshIdxToBeDeleted]
        lshIdxToBeDeleted= -1

    for lshIdx, lshClusterJsonLine in enumerate(lshClustersJsonLines):
        lshClusterJSON = json.loads(lshClusterJsonLine.strip().replace("'","\""))
        clusterURI = lshClusterJSON["uri"]
        if(clusterURI) == groundTruthLineList[0] :

            lshCandidates = lshClusterJSON["candidates"]
            if isinstance(lshCandidates, dict):
                #print "comparing dict: " + lshCandidates["uri"] + " with " + groundTruthLineList[groundTruthIndex]
                if lshCandidates["uri"] == groundTruthLineList[groundTruthIndex].replace('\n',''):
                    # ground truth link found

                    lshIdxToBeDeleted = lshIdx
                    truePositive = truePositive + 1
                    gtLinkFound = True
                    break

            elif isinstance(lshCandidates, list):
                for candidate in lshCandidates:
                    #print "comparing list: " + candidate["uri"] + " with " + groundTruthLineList[groundTruthIndex]
                    if candidate["uri"] == groundTruthLineList[groundTruthIndex].replace('\n',''):
                        # ground truth link found

                        lshIdxToBeDeleted = lshIdx
                        truePositive = truePositive + 1
                        gtLinkFound = True
                        break
            else:
                print"evaluateLshLinks: candidates are neither a list or a dict."
        if gtLinkFound: break
    count = count + 1
    print "True positive: " + str(truePositive)
    prevClusterURI = groundTruthLineList[0]

precision = truePositive / float(numberOfCandidates)
recall = truePositive / float(groundTruthSize)
printResults(groundTruthSize, numberOfTargets, numberOfCandidates, precision, recall, truePositive )