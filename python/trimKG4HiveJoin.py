__author__ = 'Gleb'

import sys
import json
from copy import deepcopy
from knowledgeGraphHelpers import  writeJsonLine2File

reload(sys)
sys.setdefaultencoding('utf8')

#python /home/aifb-ls3-vm8/gga/master-thesis-isi-kit/python/trimKG4HiveJoin.py \
#--inputKnowledgGraph /home/aifb-ls3-vm8/gga/07_KnowledgeGraphs/KG_u_s_46lsh_hj.json \
#--propertyNameList name,birthYear \
#--outputFile KG_u_s_46lsh_hj_trimmed.json

def parse_args():
    global inputKnowledgeGraph
    global propertyNameList
    global outputFile
    for arg_idx, arg in enumerate(sys.argv):
        if arg == "--inputKnowledgGraph":
            inputKnowledgeGraph = sys.argv[arg_idx+1]
            continue
        if arg == "--propertyNameList":
            propertyNameList = sys.argv[arg_idx+1].split(",")
            continue
        if arg == "--outputFile":
            outputFile = sys.argv[arg_idx+1]
            continue

def readLines(file):
    inputKnowledgeGraphFile = open(inputKnowledgeGraph)
    return inputKnowledgeGraphFile.readlines()

def trimRoleObject(roleObj):
    del roleObj["a"]
    del roleObj["wasGeneratedBy"]
    return roleObj

inputKnowledgeGraph = None
propertyNameList = None
outputFile = None

trimmedKG = []

parse_args()

propertyNameList

knowledgeGraphLines = readLines(inputKnowledgeGraph)

for kgEntityLine in knowledgeGraphLines:
    kgEntityJson = json.loads(kgEntityLine.strip())
    newClusterJson = {}

    for attrKey, attrValue in kgEntityJson.iteritems():

        if attrKey in propertyNameList:

            if isinstance(attrValue, dict):
                newClusterJson[attrKey] = trimRoleObject(attrValue)

            elif isinstance(attrValue, list):
                roleObjList = []

                for v in attrValue:
                    roleObjList.append(trimRoleObject(v))

                newClusterJson[attrKey] = roleObjList

        elif attrKey == "uri":
            if isinstance(attrValue, basestring):
                newClusterJson[attrKey] = attrValue

    trimmedKG.append(deepcopy(json.dumps(newClusterJson)))

writeJsonLine2File(trimmedKG, outputFile)