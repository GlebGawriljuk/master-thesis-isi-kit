__author__ = 'd22admin'

import sys
import json

reload(sys)
sys.setdefaultencoding('utf8')

def printResults(groundTruthSize, numberOfTargets, numberOfCandidates, precision, recall, truePositive, maxNumOfCandidates, falsePositive):
    outputFile = open("evaluateFilteredResults_Output.tsv", 'w')
    outputFile.write("GroundTruth size: " + str(groundTruthSize) + '\n')
    outputFile.write("Number of GT targets in Filter results: " + str(numberOfTargets) + '\n')
    outputFile.write("Number of Filter candidates: " + str(numberOfCandidates) + '\n')
    outputFile.write("Max number of Filter candidates: " + str(maxNumOfCandidates) + '\n')
    outputFile.write("Average number of candidates per target: " + str(numberOfCandidates/float(numberOfTargets)) + '\n')
    outputFile.write("True positive: " + str(truePositive) + '\n')
    outputFile.write("False positive: " + str(falsePositive) + '\n')
    outputFile.write("Precision: " + str(precision) + '\n')
    outputFile.write("Recall: " + str(recall) + '\n')


def countCandidates(filteredLinksLines):
    targetNum = 0
    candNum = len(filteredLinksLines)
    tempList = []
    for filteredLink in filteredLinksLines:
        tempList.append(filteredLink.split("\t")[0])
    return  [len(set(tempList)), candNum]

def parse_args():
    global filteredClustersFilepath
    global groundTruthFilepath
    for arg_idx, arg in enumerate(sys.argv):
        if arg == "--filteredLinksFilepath":
            filteredLinksFilepath = sys.argv[arg_idx+1]
            continue
        if arg == "--groundTruth":
            groundTruthFilepath = sys.argv[arg_idx+1]
            continue

filteredClustersFilepath = None
groundTruthFilepath = None

parse_args()

filteredLinksFile = open(filteredClustersFilepath)
filteredLinksLines = filteredLinksFile.readlines()

groundTruthFile = open(groundTruthFilepath)
groundTruthLines = groundTruthFile.readlines()

groundTruthSize = len(groundTruthLines)
truePositive = 0
numberOfCandidates = 0
numberOfTargets = 0
maxNumOfCandidates = 0
count = 1
foundGTTargets = []

linksNotFoundFile = open("evaluateFilteredResults_LinksNotFound.tsv", 'w')

for groundTruthLine in groundTruthLines:

    groundTruthLineList = groundTruthLine.split('\t')
    #targetURI = groundTruthLineList[0].strip()
    #gtLinkURI = groundTruthLineList[1].replace('\n','').strip()

    gtLinkURI = groundTruthLineList[0].strip()
    targetURI = groundTruthLineList[1].replace('\n','').strip()

    gtFound = False
    gtTargetFound = False
    currNumOfCand = 0
    for filteredLinkLine in filteredLinksLines:
        filteredLinkList = filteredLinkLine.strip().split("\t")
        clusterURI = filteredLinkList[0].strip()
        if clusterURI == targetURI:
            gtTargetFound = True
            if len(foundGTTargets) < 1 or clusterURI == foundGTTargets[-1]: # count gt candidates only if their target URI was not yet considered
                currNumOfCand = currNumOfCand + 1
            else:
                currNumOfCand = 1
            candidateUri = filteredLinkList[1].strip()
            if candidateUri == gtLinkURI:
                # ground truth link found
                gtFound = True
                truePositive = truePositive + 1
                break

    if not gtFound:
        linksNotFoundFile.write(targetURI + "\t" + gtLinkURI + "\n")

    if currNumOfCand > maxNumOfCandidates:
        maxNumOfCandidates = currNumOfCand

    if gtTargetFound and targetURI not in foundGTTargets:
        foundGTTargets.append(targetURI)

    print str(count) + " of " + str(groundTruthSize) + " GT links processed."
    count = count + 1

for filteredLinkLine in filteredLinksLines: #count number of GT candidates.
    filteredLinkList = filteredLinkLine.strip().split("\t")
    clusterURI = filteredLinkList[0].strip()
    if clusterURI in foundGTTargets:
        numberOfCandidates = numberOfCandidates + 1


falsePositive = numberOfCandidates - truePositive
precision = truePositive / float(numberOfCandidates)
recall = truePositive / float(groundTruthSize)
numberOfTargets = len(foundGTTargets)
printResults(groundTruthSize, numberOfTargets, numberOfCandidates, precision, recall, truePositive, maxNumOfCandidates, falsePositive )