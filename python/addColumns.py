__author__ = 'rajagopal067'
import sys
import ngram
import unicodedata
import re
import logging


def merge_Property_Values(self,prevValues,newValues):
    #print "add " + newValues + " to " + prevValues
    prevValuesList = list(prevValues.strip().split(" "))
    newValuesList = list(newValues.strip().split(" "))
    for newPropValue in newValuesList:
        valueExist = False
        for propValue in prevValuesList:
            #print "compare " + propValue + " to " + newPropValue
            if propValue == newPropValue:
                valueExist = True
                break
        if valueExist == False:
            prevValues = prevValues + " " + newPropValue
            #print "Value added: " + prevValues
    return prevValues
def resourceLineToString(resourceLineList):
    resourceLine = ""
    for idx, value in enumerate(resourceLineList):
        if idx == 0 : resourceLine = value
        else: resourceLine = resourceLine + "\t" + value
    return resourceLine

def parse_args():
    global inputFilename
    global outputFilename
    global inputIdxs
    global separator
    global n_gram_words
    global n_gram_characters
    global n_gram_size

    for arg_idx, arg in enumerate(sys.argv):
        if arg == "--input":
            inputFilename = sys.argv[arg_idx+1]
            continue
        if arg == "--output":
            outputFilename = sys.argv[arg_idx+1]
            continue
        if arg == "--inputIdxs":
            inputIdxs = sys.argv[arg_idx+1]
            continue
        if arg == "--separator":
            separator = sys.argv[arg_idx+1]
            continue



inputFilename = None
inputIdxs = None
outputFilename = None
separator = "\t"

def die():
    print "Please input the required parameters"
    print "Usage: GenerateTokens.py --input <input filename> --output <output filename> [--separator <sep=\\t>] [--computengramcharacters <True or False>] [--computengramwords <True or False>] " \
          "[--ngramsize <size>]"
    exit(1)


# check if this is main because when this this class is called from other module the following code should not get executed

if __name__ == '__main__':

    parse_args()

  #  if len(sys.argv) < 3:
   #     die()
    file = open(inputFilename,'r')
    inputIdxs = list(inputIdxs.strip().split(","))
    outputFile = open(outputFilename,'w')
    prevResourceID = None
    prevResourceList = None
    prevResourceListLength = None
    placeHolderValue = ""
    for line in file:
        lineList = list(line.rstrip('\n').strip('\r').split(separator))
        #print "Checking " + lineList[0] + " with length " + str(len(lineList))
        for idx in inputIdxs:
            #print "Checking index: " + idx
            if int(idx) > len(lineList):
                #print str(int(idx) - len(lineList)) + " columns will be added"
                for x in range(len(lineList), int(idx)+1):
                    #print "Index is bigger then record length. Adding column at index: " + str(x)
                    lineList.insert(int(x), placeHolderValue)
            else:
                #print "Index is smaller then record length. Adding column at index: " + idx
                lineList.insert(int(idx), placeHolderValue)
        outputFile.write(resourceLineToString(lineList) + '\n')
    file.close()
    outputFile.close()






