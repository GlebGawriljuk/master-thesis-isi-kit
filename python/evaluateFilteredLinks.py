__author__ = 'd22admin'

import sys
import json

reload(sys)
sys.setdefaultencoding('utf8')

def printResults(groundTruthSize, numberOfTargets, numberOfCandidates, precision, recall, truePositive):
    print "GroundTruth size: " + str(groundTruthSize)
    print "Number of Filter targets: " + str(numberOfTargets)
    print "Number of Filter candidates: " + str(numberOfCandidates)
    print "Average number of candidates per target: " + str(numberOfCandidates/float(numberOfTargets))
    print "True positive: " + str(truePositive)
    print "Precision: " + str(precision)
    print "Recall: " + str(recall)


def countCandidates(filteredLinksLines):
    targetNum = 0
    candNum = len(filteredLinksLines)
    tempList = []
    for filteredLink in filteredLinksLines:
        tempList.append(filteredLink.split("\t")[0])
    return  [len(set(tempList)), candNum]

def parse_args():
    global filteredClustersFilepath
    global groundTruthFilepath
    global groundTruthIndex
    for arg_idx, arg in enumerate(sys.argv):
        if arg == "--filteredLinksFilepath":
            filteredLinksFilepath = sys.argv[arg_idx+1]
            continue
        if arg == "--groundTruth":
            groundTruthFilepath = sys.argv[arg_idx+1]
            continue
        if arg == "--groundTruthIndex":
            groundTruthIndex = int(sys.argv[arg_idx+1])
            continue

filteredClustersFilepath = None
groundTruthFilepath = None
groundTruthIndex = None

parse_args()

filteredLinksFile = open(filteredClustersFilepath)
filteredLinksLines = filteredLinksFile.readlines()

groundTruthFile = open(groundTruthFilepath)
groundTruthLines = groundTruthFile.readlines()

groundTruthSize = 200
truePositive = 0
numberOfCandidates = countCandidates(filteredLinksLines)[1]
numberOfTargets = countCandidates(filteredLinksLines)[0]
lshIdxToBeDeleted = -1
count = 1
prevClusterURI = ""
foundGTTargets = []
for groundTruthLine in groundTruthLines:
    #print str(count) + " of " + str(groundTruthSize) + " ground Truth links processed. LSH size: " + str(len(filteredLinksLines))
    count = count + 1
    groundTruthLineList = groundTruthLine.split('\t')
    #print "current GT URI evaluated: " + groundTruthLineList[0]

    # delete lsh candidate which already was evaluated
    #if lshIdxToBeDeleted > -1:
     #   del filteredLinksLines[lshIdxToBeDeleted]
      #  lshIdxToBeDeleted= -1
    if not groundTruthLineList[0].replace("\n","") in foundGTTargets:
        #print "--------- dup"
        gtFound = False
        for lshIdx, filteredLinkLine in enumerate(filteredLinksLines):
            filteredLinkList = filteredLinkLine.strip().split("\t")
            clusterURI = filteredLinkList[0]
            #print "--- current LSH URI evaluated: " + clusterURI
            if clusterURI == groundTruthLineList[0]:
                lshIdxToBeDeleted = lshIdx
                candidateUri = filteredLinkList[1]
                #print "comparing: " + candidateUri + " with '" + groundTruthLineList[groundTruthIndex].replace('\n','') + "'"
                if candidateUri == groundTruthLineList[groundTruthIndex].replace('\n',''):
                    # ground truth link found
                    gtFound = True
                    foundGTTargets.append(groundTruthLineList[0].replace('\n',''))
                    truePositive = truePositive + 1
                    break
    #prevClusterURI = groundTruthLineList[0].replace('\n','')

        if not gtFound:
            print "GT candidate not found for " + groundTruthLineList[0]
precision = truePositive / float(numberOfCandidates)
recall = truePositive / float(groundTruthSize)
printResults(groundTruthSize, numberOfTargets, numberOfCandidates, precision, recall, truePositive )