__author__ = 'glebgawriljuk'
import sys
import json
import urllib
from copy import deepcopy

reload(sys)
sys.setdefaultencoding('utf8')

def createRoleObject(entityObj, valueObj, featureName):
    tmplist =  [] # alternateName-property to Role
    alternateNameRole = {}
    if not isinstance(valueObj[featureName], basestring):
        for alternateName in valueObj[featureName]: # collect all alternateName
            alternateNameRole["a"] = "Role"
            alternateNameRole[featureName] = alternateName
            alternateNameRole["wasGeneratedBy"] = provJSON
            tmplist.append(deepcopy(alternateNameRole))
        entityObj[featureName] = tmplist
    else:
        alternateNameRole["a"] = "Role"
        alternateNameRole[featureName] = valueObj[featureName]
        alternateNameRole["wasGeneratedBy"] = provJSON
        entityObj[featureName] = alternateNameRole

def createSameAsRoleObject(entityObj, valueObj, featureName):
    alternateNameRole = {}
    alternateNameRole["a"] = "Role"
    alternateNameRole["sameAs"] = valueObj[featureName]
    alternateNameRole["wasGeneratedBy"] = provJSON
    entityObj["sameAs"] = alternateNameRole

def writeJSON2File(data, filename):
	try:
		jsondata = json.dumps(data, indent=4, skipkeys=True, sort_keys=True)
		fd = open(filename, 'w')
		fd.write(jsondata)
		fd.close()
	except:
		print 'ERROR writing', filename
		pass

def parse_args():
    global inputFilename
    global inputProvFilename
    global startURIindex
    global outputFilename
    global separator
    for arg_idx, arg in enumerate(sys.argv):
        if arg == "--input":
            inputFilename = sys.argv[arg_idx+1]
            continue
        if arg == "--inputProvFile":
            inputProvFilename = sys.argv[arg_idx+1]
            continue
        if arg == "--startURIindex":
            startURIindex = int(sys.argv[arg_idx+1])
            continue
        if arg == "--output":
            outputFilename = sys.argv[arg_idx+1]
            continue


def die():
    print "Please input the required parameters"
    print "Usage: createNewClusters.py --input <input filename> filename> --output <output filename>"
    exit(1)


inputFilename = None
inputProvFilename = None
startURIindex = None
outputFilename = None
separator = "\t"

parse_args()

inputBaseDataFile = open(inputFilename)
provFile = open(inputProvFilename)
provJSON = json.load(provFile)

baseResourceLines = inputBaseDataFile.readlines()
JSONdata = []
print "createNewClusters: starting to generate data."
for baseResourceLine in baseResourceLines:
    newClusterJsonData = {}
    if baseResourceLine.strip() != "":
        resource = json.loads(baseResourceLine.strip())
        resourceURI = resource["uri"]
        newClusterJsonData["@context"] = "http://isi.edu/context/..."
        newClusterJsonData["a"] = "Person"
        newClusterJsonData["uri"] = "http://isi.edu/knowledgGraph/entity/" + str(startURIindex)
        if not resource.has_key('inGraph'): # this resource was not integrated into the knoledge graph yet
            if resource.has_key('alternateName'):
                createRoleObject(newClusterJsonData, resource, "alternateName")
            if resource.has_key('description'):
                createRoleObject(newClusterJsonData, resource, "description")
            if resource.has_key('nationality'):
                createRoleObject(newClusterJsonData, resource, "nationality")
            if resource.has_key('gender'):
                createRoleObject(newClusterJsonData, resource, "gender")
            if resource.has_key('birthYear'):
                createRoleObject(newClusterJsonData, resource, "birthYear")
            if resource.has_key('deathYear'):
                createRoleObject(newClusterJsonData, resource, "deathYear")
            if resource.has_key('birthPlace'):
                createRoleObject(newClusterJsonData, resource, "birthPlace")
            if resource.has_key('deathPlace'):
                createRoleObject(newClusterJsonData, resource, "deathPlace")

            createSameAsRoleObject(newClusterJsonData, resource, "uri")
            JSONdata.append(deepcopy(newClusterJsonData))
            startURIindex = startURIindex + 1
            #print "Current number of new entities: " + str(startURIindex)

print "createNewClusters: finished generatimng data."

inputBaseDataFile.close()
numOfEntitiesData = {}
numOfEntitiesData["numberOfEntities"] = startURIindex
JSONdata.append(deepcopy(numOfEntitiesData))
print "createNewClusters: dumping data to JSON-LD."
writeJSON2File(JSONdata, outputFilename)
