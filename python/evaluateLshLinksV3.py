__author__ = 'd22admin'

import sys
import json

reload(sys)
sys.setdefaultencoding('utf8')

def printResults(groundTruthSize, numberOfTargets, numberOfCandidates, precision, recall, truePositive, falsePositive, maxNumOfCandidates, totalNumOfCandidates):
    outputFile = open("evaluateLshResults_Output.tsv", 'w')
    outputFile.write("GroundTruth size: " + str(groundTruthSize) + '\n')
    outputFile.write("Number of GT targets in LSH results: " + str(numberOfTargets) + '\n')
    outputFile.write("Number of LSH candidates: " + str(numberOfCandidates) + '\n')
    outputFile.write("Max number of LSH candidates: " + str(maxNumOfCandidates) + '\n')
    outputFile.write("Average number of candidates per target: " + str(numberOfCandidates/float(numberOfTargets)) + '\n')
    outputFile.write("Total number of candidates: " + str(totalNumOfCandidates) + '\n')
    outputFile.write("True positive: " + str(truePositive) + '\n')
    outputFile.write("False positive: " + str(falsePositive) + '\n')
    outputFile.write("Precision: " + str(precision) + '\n')
    outputFile.write("Recall: " + str(recall) + '\n')


def parse_args():
    global lshClustersFilepath
    global groundTruthFilepath
    for arg_idx, arg in enumerate(sys.argv):
        if arg == "--lshClustersFilepath":
            lshClustersFilepath = sys.argv[arg_idx+1]
            continue
        if arg == "--groundTruthFilepath":
            groundTruthFilepath = sys.argv[arg_idx+1]
            continue

lshClustersFilepath = None
groundTruthFilepath = None

parse_args()

lshClustersFile = open(lshClustersFilepath)
lshClustersJsonLines = lshClustersFile.readlines()

groundTruthFile = open(groundTruthFilepath)
groundTruthLines = groundTruthFile.readlines()

groundTruthSize = len(groundTruthLines)
truePositive = 0
numberOfGtEntities = 0
maxNumOfCandidates = 0
totalNumOfGTCandidates = 0
totalNumOfCandidates = 0
count = 1
targetUriList = []

linksNotFoundFile = open("evaluateLshLins_linksNotFound.tsv", "w")

clusterIndex = []
#create Index
print "Indexing ..."
for idx, lshClusterJsonLine in enumerate(lshClustersJsonLines):
        lshClusterJSON = json.loads(lshClusterJsonLine.strip().replace("'","\""))
        clusterURI = lshClusterJSON["uri"]
        clusterIndex.append([clusterURI,idx])
        totalNumOfCandidates = totalNumOfCandidates + len(lshClusterJSON["candidates"])

print "Indexing finished."
#Iterate through GT
for groundTruthLine in groundTruthLines:

    gtLinkFound = False
    groundTruthLineList = groundTruthLine.split('\t')
    #targetURI = groundTruthLineList[0]
    #gtLinkURI = groundTruthLineList[1].replace('\n','').encode('ascii','ignore').strip()

    gtLinkURI = groundTruthLineList[0]
    targetURI = groundTruthLineList[1].replace('\n','').encode('ascii','ignore').strip()

    kgEntityIndex = -1

    #find offset in clusterIndex
    for currentIdx in clusterIndex:
        #print "Searching for index for " + str(targetURI) + " : " + str(currentIdx[0])
        if currentIdx[0] == targetURI:
            kgEntityIndex = currentIdx[1]
            break


    if kgEntityIndex > -1:
        # get the cluster by offset
        print "Searching for " + targetURI + "   " + gtLinkURI + " with offset " + str(kgEntityIndex) + " of " + str(len(lshClustersJsonLines))
        lshClusterJSON = json.loads(lshClustersJsonLines[kgEntityIndex].strip().replace("'","\""))


        #shClusterJSON = json.loads(lshClusterJsonLine.strip().replace("'","\""))
        clusterURI = lshClusterJSON["uri"]
        lshCandidates = lshClusterJSON["candidates"]

        if isinstance(lshCandidates, dict):
            candidateURI = lshCandidates["uri"].encode('ascii','ignore')
            if targetURI not in targetUriList: # add num of candidate only if not already added
                totalNumOfGTCandidates = totalNumOfGTCandidates + 1
            if maxNumOfCandidates == 0:
                maxNumOfCandidates = 1
            if candidateURI == gtLinkURI:
                truePositive = truePositive + 1
                gtLinkFound = True
                break

        elif isinstance(lshCandidates, list):
            if targetURI not in targetUriList: # add num of candidate only if not already added
                totalNumOfGTCandidates = totalNumOfGTCandidates + len(lshCandidates)
            if len(lshCandidates) > maxNumOfCandidates:
                maxNumOfCandidates = len(lshCandidates)
                #if len(lshCandidates) > 10:
                 #   print "TO MANY CANDIDATES:"
                  #  print str(lshClusterJSON)

            for candidate in lshCandidates:
                candidateURI = candidate["uri"].encode('ascii','ignore')
                #print "Comparing:"
                #print gtLinkURI + " " + str(type(gtLinkURI))
                #print candidateURI + " " + str(type(candidateURI))
                if candidateURI == gtLinkURI:
                    #print "FOUND GT LINK"
                    truePositive = truePositive + 1
                    gtLinkFound = True
                    break
        else:
            print"evaluateLshLinks: candidates are neither a list or a dict."
    #if gtLinkFound: break

    if not gtLinkFound:
        linksNotFoundFile.write(targetURI + "\t" + gtLinkURI + "\n")
    if targetURI not in targetUriList:
        targetUriList.append(targetURI)
    print "True positive: " + str(truePositive)

    print str(count) + " of " + str(groundTruthSize) + " ground Truth links processed."
    count = count + 1

falsePositive = totalNumOfGTCandidates - truePositive
numberOfGtEntities = len(targetUriList)
precision = truePositive / float(totalNumOfGTCandidates)
recall = truePositive / float(groundTruthSize)
printResults(groundTruthSize, numberOfGtEntities, totalNumOfGTCandidates, precision, recall, truePositive, falsePositive, maxNumOfCandidates, totalNumOfCandidates )
