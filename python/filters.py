__author__ = 'Gleb'
import jaro
import json


def applyJaroWinklerMaxFilterOnCluster (cluster, threshold): # return the one candidate json object which fullfills criteria or None
    #print "Filtering cluster: \n" + cluster

    clusterJSON = json.loads(cluster.strip())

    entityURI = clusterJSON["uri"]["uri"]
    #print "applyFilterOnCluster  #####   Filtering cluster of " + entityURI
    entity_alternateName = clusterJSON["uri"]["alternateName"]
    #print "alternateName = "+ str(entity_alternateName)
    entity_alternateNameValue = None
    jaroWinklerMax = threshold
    jaroWinklerMaxCandidates = []

    if isinstance(entity_alternateName, dict):
        #print "Value is a Dict.\nKey: " + "alternateName" + "| value: " + entity_alternateName["alternateName"]
        entity_alternateNameValue = entity_alternateName["alternateName"]
    elif isinstance(entity_alternateName, list):
        entity_alternateNameValue = []
        for entity_key_item in entity_alternateName:
            entity_alternateNameValue.append(entity_key_item["alternateName"])
        #print "Value is a list.\nKey: " + "alternateName" + "| list: " + str(entity_alternateNameValue)
    else:
        print "Format of entity attribute value not recognized: entity: " + entityURI + " attribute key: " + "alternateName"

    #print "Type of entity attribute is " + str(type(entity_alternateNameValue))

    #print "Start iterating over candidates of " + entityURI
    for candidate in clusterJSON["candidates"]: # iterate over all candidates and filter one candidate out
        candidateAlternateNameValue = None
        candidateAlternateName = candidate["alternateName"]
        candidateURI = candidate["uri"]
        #print "CANDIDATE " + candidateURI
        #print "Type of candida attribute is " + str(type(candidateAlternateName)) #+ "\n" + str(candidateAlternateName)
        if isinstance(candidateAlternateName, basestring):
            candidateAlternateNameValue = candidateAlternateName
            #print "Value is a String.\nKey: " + "alternateName" + "| value: " + candidateAlternateNameValue

            if isinstance(entity_alternateNameValue, basestring):
                #print "jaroWinkler( " + entity_alternateNameValue + " , " + candidateAlternateNameValue + " )"
                jwValue = jaro.jaro_winkler_metric(entity_alternateNameValue.strip(), candidateAlternateNameValue.strip())
                #print "CASE 1: Entity attribute is a String. Jaro-W = " + str(jwValue)
                if jaroWinklerMax < jwValue:
                    #jaroWinklerMax = jwValue
                    jaroWinklerMaxCandidates.append(json.dumps(candidate))
            elif isinstance(entity_alternateNameValue, list):
                for entityValue in entity_alternateNameValue:
                    #print "jaroWinkler( " + entity_alternateNameValue + " , " + candidateAlternateNameValue + " )"
                    jwValue = jaro.jaro_winkler_metric(entityValue.strip(), candidateAlternateNameValue.strip())
                    #print "CASE 2: Entity attribute is a List. Jaro-W = " + str(jwValue)
                    if jaroWinklerMax < jwValue:
                        #jaroWinklerMax = jwValue
                        jaroWinklerMaxCandidates.append(json.dumps(candidate))
        elif isinstance(candidateAlternateName, list):
            candidateAlternateNameValue = []
            candidateFound = False
            for value in candidateAlternateName:
                candidateAlternateNameValue.append(value)
                if isinstance(entity_alternateNameValue, basestring):
                    #print "jaroWinkler( " + entity_alternateNameValue + " , " + value + " )"
                    jwValue = jaro.jaro_winkler_metric(entity_alternateNameValue.strip(), value.strip())
                    #print "CASE 3: Entity attribute is a String. Jaro-W = " + str(jwValue)
                    if jaroWinklerMax < jwValue:
                        #jaroWinklerMax = jwValue
                        jaroWinklerMaxCandidates.append(json.dumps(candidate))
                        break
                elif isinstance(entity_alternateNameValue, list) and not candidateFound:
                    for entityValue in entity_alternateNameValue:
                        #print "jaroWinkler( " + entityValue + " , " + value + " )"
                        jwValue = jaro.jaro_winkler_metric(entityValue.strip(), value.strip())
                        #print "CASE 4: Entity attribute is a String. Jaro-W = " + str(jwValue)
                        if jaroWinklerMax < jwValue:
                            #jaroWinklerMax = jwValue
                            jaroWinklerMaxCandidates.append(json.dumps(candidate))
                            candidateFound = True
            #print "Value is a list.\nKey: " + "alternateName" + "| list: " + str(candidateAlternateNameValue)
        else:
            print "Format of candidate attribute value not recognized: candidate: " + candidateURI + " attribute key: " + "alternateName"
        #print "jaroWinklerMax = " + str(jaroWinklerMax)

    return jaroWinklerMaxCandidates
    #print "Applying filter : entityURI " + str(clusterJSON["uri"]["uri"]) + " AND resourceURI: " +  str(clusterJSON["candidates"][0]["uri"])
    #return [str(clusterJSON["uri"]["uri"]),str(clusterJSON["candidates"][0]["uri"])] #[entityURI, dataSourceURI]